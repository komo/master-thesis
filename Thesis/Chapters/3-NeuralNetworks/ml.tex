\chapter{Introduction to Machine Learning}

\begin{itshape}
Machine Learning (ML) is a dynamic field in Artificial Intelligence that 
leverages algorithms to empower computer systems to learn and improve from 
experience without explicit programming. It is broadly classified into three 
types: supervised learning, where algorithms learn from labeled data; 
unsupervised learning, where algorithms discover patterns in unlabelled data; 
and reinforcement learning, which is based on rewarding desired behaviors. 
The quality of training data is pivotal, as it determines the model's capacity 
to generalize effectively. High-quality, diverse, and representative data sets 
lead to reliable and accurate predictive models, illustrating the profound 
significance of good training data in ML.
\end{itshape}\footnote{This short introductory paragraph about machine learning 
was created by GPT-4, a powerful natural language model trained by OpenAI
\cite{GPT4}.
To obtain the text, the following input was provided: \textit{Give me a short 
introductory text of around 100 words to machine learning in a professional and 
scientific way and mention the different types and the importance of good 
training data}}

Opposed to traditional algorithms, machine learning techniques don't use hard-coded 
logic to solve a problem. Machine learning problems can be put into two 
different categories classification and regression. In Classification, the input 
data is sorted into a category. Examples are optical character 
recognition and image recognition. Regression, on the other hand, deals with 
problems that have continuous variables as output. Meaning the output is a 
function of the input variables. A popular example would be the throttle and 
steering control of an autonomous car that is based on the input of various 
cameras and sensors.

The field of artificial intelligence, and especially machine learning, experienced 
a renaissance in the last decade which was driven by two aspects. The first was 
the rise of the Internet which made vast amounts of training data available, 
being it text for language models or image and video data for image recognition 
or generation. Secondly, advances in computer hardware provided the computational 
power needed for processing large amounts of data.

Nowadays, numerous machine learning algorithms exist, ranging from simple linear 
regression over decision trees to support vector machines and neural networks. 
Especially neural networks have seen major improvements over the last decade, and 
several subtypes exist for different types of tasks:

\begin{itemize}
    \item{\textbf{Feedforward Neural Network (FNN)}\\
            These are the most simple and basic forms of Neural Networks that are 
            often used for supervised training tasks. The information moves from 
            the input layer over one or several hidden layers of artificial 
            neurons toward the output layer.}
    \item{\textbf{Recurent Neural Network (RNN)}\\
            These differ from FNNs as the output of the hidden layers is 
            fed back to the inputs. They are often used for sequenced data 
            like text or videos where the context of previous inputs is important.}
    \item{\textbf{Convolutional Neural Network (CNN)}\\
                Convolutional Neural Networks, as the name implies, perform 
                convolutions on the input data. They have been especially 
                successful in the domain of image recognition and other 
                image-based machine-learning tasks.}
    \item{\textbf{Transformers}\\
                The Transformer architecture introduced in 2017 
                \cite{AttentionIsAllYouNeed} has been revolutionary in 
                the field of natural language processing. Models like 
                Chat-GPT or Google Bard are based on it. One advantage
                compared to RNNs is that the inputs do not have to be 
                processed sequentially. The key mechanism enabling this 
                is so-called self-attention. It is a scaled dot product that allows 
                one to compute how strong the relationship between two different positions in 
                an input sequence (e.g. two words in a text) is. This allows
                transformers to also capture context over large distances (e.g. 
                words several sentences apart) which is something that RNNs can not
                accomplish effectively.}
\end{itemize}

\section{Feed Forward Neural Networks}

A classical regression problem would mean that we have inputs \(x\) 
and an unknown function \(f(x)\) and in our case a neural network \(f_{NN}(x, \theta)\) that 
should approximate 
the function, \(\theta\) are the parameters of the model that define its behavior. 
The value of the function is known for some 
inputs \(y^*=f(x^*)\). The set \(\{x^*,y^*\}\) is called training data as it 
is used to adapt ("train") the model by varying the parameters to improve the fit
of the training data.
A neural network consists of several chained functions called layers or hidden layers.

\begin{equation}
    f_{NN}(x, \theta) = f^{n}(f^{n-1}(f^{n-2}(\dots f^1(x, \theta_1),\theta_{n-2}), \theta_{n-1}), \theta_{n})
\end{equation}

Here \(f_1\) is called the first (hidden) layer, \(f_2\) the second layer, and so on.
\(\theta_i\) are the parameters for the i-th layer. The number of layers that a neural network has 
gives the depth of the model. The commonly used term "Deep Learning" stems from this. There is, however, no universal definition defining how many layers a model needs to be considered "deep".

In a neural network, the chained functions \(f\) are vector-valued. Each element of one of 
those hidden layers is multiplied by a weight \(w\) which is different for each connection. 
All multiplied inputs from the preceding layer are 
then summed, and a bias \(b\) individual to each neuron is added. This is the input to the neurons of the next layer.
Figure \ref{chap3_img_fnn_schema} shows a fully connected network with two inputs, two hidden layers of size three, and a
single output. It also shows the weights \(w\) for each connection and the bias for each neuron in the hidden layers 
and the output. As one might assume, the term neural network is loosely connected to neuroscience.
According to neuroscience, the brain consists of connected neurons. Neurons can either be activated or not 
and give a pulse to connected neurons if activated. While it is not the goal of neural networks to mimic the
human brain, an activation function is introduced nevertheless. In this thesis, the 
notation for a general unspecified activation function is \(g(x)\). The main reason behind it is to introduce nonlinearity
into the model. There are various activation functions that one can use for a neural network. Among the most 
common are the sigmoid function, the hyperbolic tangent, or the rectified linear unit (ReLU).

\begin{equation}
        \mathrm{sig}(x) = \frac{1}{1+e^{-x}}, \qquad 
        \tanh(x) = \frac{e^x-e^{-x}}{e^x+e^{-x}}, \qquad
        \mathrm{ReLU}(x) = \mathrm{max}\{x, 0\}
        \label{chap3_eq_activation_functions}
\end{equation}
Equation \ref{chap3_eq_activation_functions} shows the sigmoid, tanh, and ReLU 
functions. In figure \ref{chap3_img_activation_functions}, the sigmoid, tanh, and ReLU 
functions are also plotted. In some cases, it can be beneficial to use the leaky 
ReLU function \(\mathrm{ReLU_{leaky}}(x) = \mathrm{max}\{x, \alpha x\}\) with 
\(0 < \alpha \ll 1\). A commonly chosen value is \(\alpha=0.01\). The reason for 
this is that the weights of the network could result in a negative input which would 
then have an activation of 0, preventing the neuron from contributing to the network. Neurons 
for which this is the case are also called dead neurons.
Nowadays, the defacto standard activation function used for neural networks among most domains is
the ReLU function \cite{deeplearningbook}. Figure \ref{chap3_img_fnn_schema} also 
shows the mathematical expression for computing the activation values \(f\) of the 
individual neurons in the hidden layers and of the output \(y\). If one stops looking 
at individual nodes within the network and considers the neural network again to be 
a concatenation of vector-valued functions, then the hidden layers can also be expressed in 
the following way 
\begin{equation}
        \vec{f}^n = g\left( \mathbf{W}_n\vec{f}^{n-1} + \vec{b}_n\right)
\end{equation}
Here \(\vec{f}^n\) are the activations for the n-th layer \(\mathbf{W}_n\) is 
a matrix containing the weights of each connection between the 
n-th and n-1-th layer and \(\vec{b}_n\) is a vector containing 
the bias for each neuron in the n-th layer.

The weights and biases make up the parameters of a neural network. The model 
has to use the right parameters to make good predictions. How those parameters 
are adjusted ("trained") is the topic of the next section \ref{chap3_training}. Another 
important part is the topology or size of the model. More layers or larger layers mean 
more parameters and hence a more flexible model. How many layers of what size are used is 
specific to each problem, and there is no general rule. A common approach is to gradually 
increase the size of the network until a good performance is accomplished. However, 
increasing the model size also increases the computational cost, and a model that 
is too large also tends to overfit the training data. More about this can be found 
in section \ref{chap3_training}.

\begin{figure}[h!]
    \centering{
    \includegraphics[width=0.8\textwidth]{img/FNN_schema.png}}
    \caption[FNN schema]{Neural Network Schema for a simple network with two inputs, 
    two hidden layers of size three and one output. It also shows how to compute 
    each layer from the previous layer both in the elementwise notation for each individual 
    neuron or node for the specific layer and in vector notation, treating the 
    whole layer as a single vector.}
    \label{chap3_img_fnn_schema}
\end{figure}


\begin{figure}
    \centering{
    \includegraphics[width=\textwidth]{img/activation_functions.png}}
    \caption[Common activation functions]{Different activation functions}
    \label{chap3_img_activation_functions}
\end{figure}



\section{Training and Backpropagation}\label{chap3_training}

In order to adjust the parameters (weights and biases) of a neural network, 
it has to be trained with data. For a regression problem, the training data 
would consist of tuples of function inputs and the desired output of the function. 
The goal is to change the parameters so that the model reproduces the training data 
as well as possible. The first building block for this is the loss function \(L\), 
sometimes also called the cost function. 
During training, samples of the training data are used as input. The output of the 
network is then compared to the desired output of the training sample. The error 
between both values is called loss in machine learning terms. The most commonly used 
loss (error) functions are the mean squared error (MSE) and the mean absolute error (MAE)
\begin{equation}
        L_\mathrm{MSE} = \frac{1}{n}\sum_{i=1}^{n}\left(y_{i} - y_{i}^*\right)^2, \qquad
        L_\mathrm{MAE} = \frac{1}{n}\sum_{i=1}^{n}\left|y_{i} - y_{i}^*\right|
\end{equation}
Here \(y_{i} = f(x_i^*)\) is the prediction of the model for a single input from the 
training data
and \(y_{i}^*\) is the expected output of the model according to the 
training data for the same input. The MSE loss has the disadvantage that it emphasizes outliers 
where the difference between the model prediction and the outlier is large and leads to a large loss. 
MAE, on the other hand, has the disadvantage of not being differentiable at 0. A loss function that combines 
both is the so-called huber loss, where the loss is quadratic for small errors (usually smaller than 1) and increases 
linearly for larger errors. Figure \ref{chap3_img_loss_functions} shows the loss function mentioned above.

\begin{figure}
    \centering{
    \includegraphics[width=0.75\textwidth]{img/loss_functions.png}}
    \caption[Common loss functions]{Mean squared error loss function and mean absolute error 
    loss function for a single training sample \(y_{i}^*\) and 
    model prediction \(y_{i}\).} 
    \label{chap3_img_loss_functions}
\end{figure}

It is common practice to initialize the weights and biases of each layer by either 
a uniform or normal distribution that is proportional to the size of the layer 
or the number of connections \cite{LeCun2012}.
We now have a loss function \(L\), a set of training data 
\(\{(x_i^*, y_i^*)\}\) and a neural network 
\(f(x, \{w_{jk}^l, b_j^l\})\) with randomly initialized weights \(w\) and biases 
\(b\) where \(w_{jk}^l\)
is the weight between the k-th neuron in the l-1-th layer and the j-th neuron 
in the l-th layer, and \(b_j^l\) is the bias of the j-th neuron in the l-th layer.
To get the best possible results with our neural network, we want to tweak our 
weights and biases so that the loss function gets minimized. This is achieved by incrementally 
updating the weights and biases of the networks via the following rule
\begin{equation}\label{chap3_eq_updating_parameters}
\left(w_{jk}^l\right)' = w_{jk}^l - \eta\frac{\partial L}{\partial w_{jk}^l}, \qquad
\left(b_j^l\right)' = b_j^l - \eta\frac{\partial L}{\partial b_j^l}
\end{equation}
Here \(w'\) and \(b'\) are the updated weights and biases. \(\eta\) is a small number 
usually called the learning rate in machine learning terms. Common values for \(\eta\)
are between \(\num{1e-3}\) and \(\num{1e-5}\). By applying this update many times, we
get to the minimum of the loss function. This approach is also called gradient descent.

The remaining question is how the partial derivatives \(\frac{\partial L}{\partial w}\)
and \(\frac{\partial L}{\partial b}\) are calculated. The loss for the i-th 
training sample depends on the weights and biases of the network 
\begin{equation}
L_i = L(y_i^*, f(x_i^*, \{w_{jk}^l, b_j^l\}))
\end{equation}
In the following, we will use \(L\) for the loss of the i-th training sample.
Remember that the activation \(f_i^l\) of the i-th neuron in the l-th layer is 
computed by
\begin{equation}\label{chap3_eq_layer_activation}
    f_j^l = g(z_j^l) = g\left(\sum_k w_{jk}^lf_k^{l-1} + b_j^l\right)
\end{equation}
were we introduced \(z_j^l\), which is the input to the j-th neuron in the l-th layer 
as an intermediate value. \(g\) is the activation function described in the previous 
section. Let us define 
\begin{equation}
    \delta_j^l := \frac{\partial L}{\partial z_j^l}
\end{equation}
which can be interpreted as the error of the neural network in the l-th layer. 
For the final output layer (N-th layer) of the network this is given by applying the chain rule
\begin{equation}
\delta_j^N = \frac{\partial L}{\partial z_j^N} = 
\frac{\partial L}{\partial f_j^N}\frac{\partial f_j^N}{\partial z_j^N} 
\end{equation}
For the MSE loss function, the derivative of the loss function 
\(\frac{\partial L}{\partial f_j^N}\) would for a single training sample simply be 
given by \(2(y_i - y_i^*)\).
By applying the chain rule again, \(\delta_j^{l}\) can then be expressed in terms 
of \(\delta^{l+1}\)
\begin{equation}\label{chap3_eq_layer_error1}
\delta_j^{l} = \frac{\partial L}{\partial z_j^{l}} = 
\sum_k\frac{\partial L}{\partial z_k^{l+1}} \frac{\partial z_k^{l+1}}{\partial z_j^{l}} = 
\sum_k \delta_k^{l+1} \frac{\partial z_k^{l+1}}{\partial z_j^{l}}
\end{equation}
Having a look at equation \ref{chap3_eq_layer_activation}, one can see that 
\(z_j^{l+1}\) can be expressed in the following way
\begin{equation}\label{chap3_eq_neuron_input}
    z_k^{l+1} = \sum_j w_{kj}^{l+1}f_j^{l} + b_j^k
\end{equation}
Therefore
\begin{equation}\label{chap3_eq_partial_diff}
    \frac{\partial z_k^{l+1}}{\partial z_j^{l}} = w_{kj}^{l+1}\frac{\partial f_j^l}{\partial z_j^l}
\end{equation}
By substituting equation \ref{chap3_eq_partial_diff} back into equation 
\ref{chap3_eq_layer_error1} we get 
\begin{equation}
    \delta_j^l = \sum_k w_{kj}^{l+1}\delta_k^{l+1}\frac{\partial f_j^l}{\partial z_j^l}
\end{equation}
It is noteworthy, that \(\frac{\partial f_j^l}{\partial z_j^l}
= \frac{\partial}{\partial z_j^l}g(z_j^l)\) is simply the 
derivative of the activation function \(g\) with respect to \(z_j^l\) which in case of 
the ReLU function (see equation \ref{chap3_eq_activation_functions}) is
the Heaviside step function. Now we have to find out how \(\delta\) is related 
to \(\frac{\partial L}{\partial w}\) and \(\frac{\partial L}{\partial b}\). We
can once more apply the chain rule 
\begin{equation}
    \frac{\partial L}{\partial w_{jk}^l} = \frac{\partial L}{\partial z_j^l}
    \frac{\partial z_j^l}{\partial w_{jk}^l} = \delta_j^l f_k^{l-1}
\end{equation}
in the last step, we simply used the definition of \(\delta_j^l\) and 
\(\frac{\partial z_j^l}{\partial w_{jk}^l} = f_k^{l-1}\) can be seen from 
equation \ref{chap3_eq_neuron_input}. With the same approach, we get 
\begin{equation}
    \frac{\partial L}{\partial b_j^l} = \delta_j^l
\end{equation}
With the equation above, we can now compute the derivatives of the loss function. By 
starting with the final layer of the network, we can gradually work backward through each 
layer and adapt the weights and biases of each layer according to the equation 
\ref{chap3_eq_updating_parameters}.
This method is also called backpropagation since we traverse backward through the network. 

Above, we wrote all the equations for individual vector/tensor elements. We 
can, however, also write them in vector notation. In the following, the 
necessary equations for backpropagation and how they are used for updating 
the weights and biases are summarized on the next page.

The approach outlined there used the gradient for a single pair \(x_i^*, y_i^*\) in the 
training data. But ultimately, we would like to optimize the weights and biases 
of the network not by minimizing the loss \(L_i\) for a single pair, but 
\(L = \frac{1}{n}\sum_{i=1}^{n}L_i\), the loss over the whole data set. 
This can be done by performing steps 1-3 for each pair in the training set 
and then computing the gradient of the loss of the whole data set by averaging 
over the gradient for the individual training samples. 
\begin{equation}\label{chap3_eq_total_loss_gradient}
    \frac{\partial L}{\partial w_{jk}^l} = \frac{1}{n}\sum_{i=1}^{n}\frac{\partial L_i}{\partial w_{jk}^l}, \qquad
    \frac{\partial L}{\partial b_j^l} = \frac{1}{n}\sum_{i=1}^{n}\frac{\partial L_i}{\partial b_j^l}
\end{equation}

\begin{tcolorbox}
\subsection*{Backpropagation}\label{chap_3_backpropagation_box}
\begin{tabularx}{\linewidth}{XX}
    {\begin{center}\textbf{Elementwise notation}\end{center}} & 
    {\begin{center}\textbf{Vector notation}\end{center}}\\
    {\begin{align}
        &\delta_j^N = \frac{\partial L}{\partial f_j^N}\frac{\partial f_j^N}{\partial z_j^N} \\
        &\delta_j^l = \sum_k w_{kj}^{l+1}\delta_k^{l+1}\frac{\partial f_j^l}{\partial z_j^l}\\
        &\frac{\partial L}{\partial w_{jk}^l} = \delta_j^l f_k^{l-1}\\
        &\frac{\partial L}{\partial b_j^l} = \delta_j^l
    \end{align}}
    &
    {\begin{align}
        &\delta^N = \nabla_{f^N} L\: \circ (f^N)' \\[18pt]
        &\delta^l = (\mathbf{W}^{l+1})^T\delta^{l+1} \circ (f^l)'\\[18pt]
        &\nabla_{\mathbf{W^l}}L = \delta^l (f^{l-1})^T\\[18pt]
        &\nabla_{b^l}L = \delta^l
    \end{align}}
\end{tabularx}
The approach for gradient descent with backpropagation is the following:
\begin{enumerate}
    \item Evaluate the network \(y_i = f(x_i^*)\) for an input \(x_i^*\) of the training 
        data and store all the intermediate results, namely the activations \(f^l(z^l)\) and 
        and inputs \(z^l\) for each layer of the network. 
    \item Compute the loss \(L_i = L(y_i, y_i^*)\).
    \item Use the equations above to compute the gradients
        \(\frac{\partial L_i}{\partial w}, \frac{\partial L_i}{\partial b}\) 
        for all the weights and biases of the network, starting with the output 
        layer \(f^N\) and working backward through the layers.
    \item Update the weights and biases with 
    \(\Delta w_{jk}^l = -\eta\frac{\partial L}{\partial w_{jk}^l}\) and 
    \(\Delta b_j^l = -\eta\frac{\partial L}{\partial b_j^l}\) where 
    \(\eta\) is the learning rate.
\end{enumerate}
\end{tcolorbox}

In practice, one differentiates between different approaches:
\begin{enumerate}
    \item \textbf{Batch Gradient Descent:}\\
        This is basically the approach described above, where the gradient 
        is computed with regard to every entry in the training dataset (see equation 
        \ref{chap3_eq_total_loss_gradient}). The training dataset is here referred to as batch. 
        The advantage is that by updating 
        the weights and biases with this gradient, we move directly toward the 
        minimum of the total loss. The disadvantage is that we have to evaluate 
        the model for every training sample before each update to the weights and biases.
        This is computationally expensive and not practical for large datasets.
    \item \textbf{Stochastic Gradient Descent}\\
        This is the complete opposite of batch gradient descent. Here we  
        compute the gradient for a single training sample and then update the 
        weights and biases. In essence, we execute steps 1-4, outlined in the box 
        above. While each update step might not move directly towards the minimum 
        of the total loss, on average, we still get closer over several update steps. Hence, 
        the stochastic nature of this approach.\clearpage
    \item \textbf{Mini Batch Gradient Descent}\\
        This is the middle ground between the two methods mentioned above and also 
        the most commonly used approach to training a neural network. Here the 
        gradients are computed for several entries but not for all of them before 
        performing an update step to the weights and biases. The subset of 
        data points for each update is also called a mini-batch. Modern hardware like 
        GPUs or specialized TPUs (Tensor Processing Units), which are specifically 
        developed for machine learning applications, can perform the computations 
        involved in evaluating and training the network efficiently in parallel. 
        Therefore the whole mini-batch can be evaluated, and the backpropagation can 
        be performed in parallel for a whole mini-batch, given that the size of the 
        mini-batch is not too big for the available hardware. It is common practice 
        to choose the size of the mini-batch as a power of two. Popular choices for 
        mini-batch sizes (meaning the number of training samples used for performing 
        a single update to the model parameters) are 256 or 512. However, the optimal 
        batch size depends on the individual problem and also on the available hardware.
\end{enumerate}
It is noteworthy that in practice, the term batch often actually means a mini-batch, 
meaning a subset of the training data.

The total training of the network is then simply done by iterating several times 
over the training data. Each iteration is called an epoch in machine learning terms. 
Within each epoch, we iterate over each mini-batch or individual training sample.
If we do not use batch gradient descent and perform updates to 
the model parameters as outlined above.

Given a large and complex enough model and enough iterations over the training data, 
the model could reproduce the training dataset arbitrarily well. It is, however, 
not our goal to just make the model "memorize" the training data. The end goal 
is to apply the model to new unseen data. To get a grasp on how well the model 
performs on new data, it is common practice to split the training dataset into an 
actual training dataset and a validation dataset. Usually around 
10-30\% of the dataset is used for validation and the rest for training. To measure the 
performance on new data, one can then evaluate the model for the validation data set 
and compute the loss. This can be done during the training of the model between 
epochs to see if the performance of the model also improves for unseen data that 
has not been used for optimizing the parameters. The model is overfitting if 
the loss over the training dataset is significantly smaller than over the 
validation dataset. If the validation loss is not decreasing anymore with additional 
epochs, one has reached the optimum for the given dataset and given model architecture.
One would then either need more or better training data or, if the model is not 
complex enough for the given dataset, a larger model meaning more layers (deeper model) 
or larger layers (wider model). For specific tasks like image recognition using 
a different approach to the fully connected feed-forward network, like convolutional 
networks, can also improve the performance on the validation data.
\\
Lastly, before concluding this chapter, it is worth mentioning that nowadays, often more sophisticated methods for updating the parameters compared to equation 
\ref{chap3_eq_updating_parameters} where we have the same constant learning rate 
\(\eta\) for each parameter are used. More advanced approaches use individual learning 
rates and or decaying learning rates, starting with large steps towards the minimum 
and decreasing the step size (learning rate) over the number of iterations. One 
such algorithm would be Adagrad \cite{adagrad_paper}. Another method of improving 
vanilla gradient descent is the introduction of momentum. Here a fraction of the previous 
update step is remembered and also used for the next update step. It can be seen 
as analogous to a ball rolling down a slope. Due to its momentum, it would keep rolling 
if the slope would become flat or the slope would become positive. In terms 
of minimizing the loss, this reduces the likelihood of ending up in local minima.
The default algorithm for popular machine learning libraries like PyTorch \cite{pytorch}
is Adam (Adaptive Moment Estimation) \cite{adam_paper}, which combines both concepts,
an adaptive learning rate, and momentum from previous update steps.

This concludes this chapter. Machine learning is a dynamic field of research, and there are many concepts and recent developments beyond the scope of this thesis. 
A more complete overview and also an introduction to different architectures like 
convolutional or recurrent neural networks can be found in \cite{deeplearningbook}.