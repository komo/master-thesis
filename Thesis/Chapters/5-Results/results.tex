\chapter{Results}\label{chap5_results}

\section{Database}\label{chap5_database}

The database created according to section \ref{chap4_database_pipeline} consists of 
2957532 data points taken from 877 different shots. The earliest shot in the 
database is 32214, and the most recent is 41493. Filtering for outliers  
(see section \ref{chap4_post_processing}) yielded 2101301 valid data points that 
could be used for training the model. The database has a size of around 900 MB when stored on disk. 
Figure \ref{appendix_img_histograms} and \ref{appendix_img_histograms_log} show 
the distribution of the model input and output values 
(see table \ref{chap4_tab_inputs} and \ref{chap4_tab_outputs}). From the individual 
histograms, it is already evident that the distribution of many variables is not homogeneous.

Figure \ref{chap5_img_chi_hist} shows the distribution of \(\chi_e\) and \(\chi_i\)
in combination with the respective temperature gradient scale. The distribution 
for the electrons is centered around \(\chi_e = 1.5\) m²/s and 
\(\frac{a}{L_{Te}} = 2.5\). For smaller or larger values, the available database 
quickly gets thinner. From this, we can expect that the models can mainly make 
predictions within the narrow region where most data points for the temperature 
gradient length scale are located. The distribution for the ions is also peaked 
for the temperature gradient length scale around a value between 1.5 and 2. 
Along \(\chi_i\), the distribution is broader compared to the electrons. 
This can be explained with the power balance (equation \ref{chap4-eq-totalpower}), 
which for the ions only depends on 
the electron-ion exchange term (equation \ref{chap4_eq_exchange_term}) 
and the NBI heating.
The NBI is the most powerful heating system at AUG and contributes more 
to the heating of ions than the electrons, which explains the broader range of 
values. Additionally, the marginal distribution of \(\chi_i\) shows a large peak 
close to 0. This is not clearly visible in the joint distribution due to the small bin size.
Those values are not restricted to a narrow range of gradient length scales, 
which can be seen from figure \ref{appendix_img_dbinspector2} in the appendix, 
where the screenshot of a 2d-histogram from the database inspection tool mentioned 
in section \ref{chap4_post_processing} for \(\frac{a}{L_{Ti}}\) and \(\chi_i\)
with a logarithmic scale is shown. 
The large amount of data points with \(\chi_i\) close to 0 can also be explained 
by the power balance. For time points where the NBI heating is not active, 
the only contributing factor is the electron-ion exchange term 
(see equation \ref{chap4_eq_exchange_term}) which is directly 
proportional to the difference between the electron and ion temperature. Since 
the temperature ratio is close to 1 for a large fraction of data points, the 
exchange term is, therefore, close to 0, which in combination with the inactive 
NBI heating explains the existence of those points. 
This shows that our power balance is insufficient for the ions, as it is 
not physically reasonable, that in the absence of NBI heating the ion heat transport 
gets close to 0. Including factors like the time derivation of the plasma energy or 
the ICRH heating in future database versions could potentially improve upon this. 

Overall we can therefore conclude that the model works better for \(\chi_e\) than for 
\(\chi_i\), which is confirmed when taking a look at the loss of the model. 
After 40 epochs of training, the models have an average validation loss 
of around 0.14 (see figure \ref{chap4_img_loss_over_epochs}). 
Since we use the Huber loss for training, which is squared for 
values smaller than 1, this corresponds to a mean absolute error of around 0.37 m²/s, 
which is the average deviation for \(\chi_e\) and \(\chi_i\) combined. 
To see the difference between electrons and ions in terms of validation accuracy, 
the mean absolute error for the electron fits and the ion fits for the validation dataset 
was computed separately. When only looking at \(\chi_e\), the mean absolute error 
was 0.33 m²/s, while for \(\chi_i\), it was 0.41 m²/s, showing that on average the fits for \(\chi_e\)
are better.


\begin{figure}[h!]
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=\textwidth]{img/electron_chi_histogram.png}
        \caption{}
        \label{chap5_img_electron_chi_hist}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=\textwidth]{img/ion_chi_histogram.png}
        \caption{}
        \label{chap5_img_ion_chi_hist}
    \end{subfigure}
    \caption[Distribution of \(\chi\) and gradient scale in database]
        {\textbf{a)} Joint and marginal distribution of 
        \(\chi_e\) and the electron temperature gradient length scale 
        \(\frac{a}{L_{Te}}\). \textbf{b)} Same plot for the ions. The marginal distribution of 
        \(\chi_i\) has a distinctive peak close to 0.}\label{chap5_img_chi_hist}
\end{figure}
    

\section{Transport Model}\label{chap5_transport_model}

In this section, we will take a look at how the model performs at replicating 
\(\chi\)-profiles and how the predictions of the model vary when changing 
parameters like \(q\) or the temperature ratio, which are linked to 
transport as shown in section \ref{chap2_turbulent_transport}.

\subsection{Reproduction of Profiles}

First, we test if the model can replicate profiles for \(\chi_e\) and 
\(\chi_i\). For this, an ensemble of 10 models was trained, where the mean of the fit 
from all 10 models was taken. This also allows us to estimate the uncertainty of the models 
for a given input by computing the standard deviation over the fits from the individual models 
of the ensemble.
Results for this are shown in figure \ref{chap5_img_additional_fits}, 
for three different shots at two different time points for each shot. Except for the profiles for shot 36049 at 1.23 seconds, the ensemble is able 
to fit the profiles for both the ions and electrons within the uncertainty of the prediction. 
The former profiles for 36049 that were not fitted well might be outliers with a specific 
parameter combination that the models did not train well. It was checked if the data points 
for those profiles were part of the training or validation data set, with the result that 
11 out of 12 belonged to the training data set, which is the statistical expectation as 
the database was split randomly into 90\% training and 10\% validation data. Therefore, 
the argument that those data points were in the validation data set, while the data points 
of the other profiles were in the training data set, which would partly explain 
the difference, cannot be made.

This shows that the ensemble can reproduce profiles for data points that it was trained with,
but the more important question is whether the model can also successfully fit unseen data points. 
For this, another ensemble was trained where all data points for shot 35894 were completely excluded 
from the training. Figure \ref{chap5_img_baseline_fit} shows the fits with the new ensemble.
The fits for the profiles at 2.6 seconds are larger when compared to figure 
\ref{chap5_img_additional_fits} and not within the uncertainty of the 
ensemble anymore. Especially when considering the fits for 3.4 seconds, where the deviation 
is even larger, we note that the deviation between the actual profiles and 
the ensemble predictions are larger than the expected 0.37 m²/s found for the 
validation dataset in the previous section.
The reason for this could be that the whole shot is excluded from training. 
In general, the database is randomly split into 90\% training data and 10\% validation 
data. This means that while individual data points are excluded from the training, both 
temporally as well as spatially neighboring data points are likely to be included into the 
training process. 
Those neighboring data points are likely to have similar values for \(\chi\) and 
for the model inputs. Hence the model performs better on the validation dataset 
than on shot 35894, where all neighboring data points are excluded. 
The underestimation in the case of the \(\chi_e\) profile at 3.4 seconds could also be explained 
with the overall distribution of \(\chi_e\) in the database since the value \(\chi_e\) of around 
4 m²/s is already above the majority of data points. The underestimation did not occur 
when the model was trained with neighboring or those exact data points, as in figure 
\ref{chap5_img_additional_fits}. The argument is strengthened by the observations 
that the difference between 
the actual profile and the fit gets smaller towards \(\rho=0.9\), where the profile approaches smaller 
values of around 2 m²/s, which have a much more extensive coverage in the available database.

We can conclude that while the model is able to
replicate the training data, it has problems generalizing to unseen data, especially if the data is outside 
the narrow parameter regime that most training data 
is centered around.

\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{img/additional_fits.png}
    \caption[Profile fits]
    {The plot shows \(\chi_e\) and \(\chi_i\) profiles for three different shots at two different 
    time points. The time points were chosen, 
    so that the first data point is without active ECRH, while the second one is. This 
    was done to ensure that the data points from the two different time points would 
    come from a different parameter regime.
    Additionally, it was ensured that all the data points for all six profiles 
    are valid and not flagged as outliers.}
    \label{chap5_img_additional_fits}
\end{figure}

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{img/baseline_fits.png}
    \caption[Fit for L- and H-mode profiles of shot 35894]
    {Values for \(\chi_e\) and \(\chi_i\) from the database and corresponding 
    predictions with uncertainty from the model ensemble. The last row shows the 
    corresponding temperature profiles. At 3.4 seconds, the electron profile is visible 
    steeper due to the active ECRH.}
    \label{chap5_img_baseline_fit}
\end{figure}

\clearpage
In addition to profiles for single time points, the time evolution between the transition from 
the L-mode to the H-mode for shot 35894 was examined. The H-mode is characterized 
by an elevated temperature and density profile, which is caused by the formation 
of a so-called pedestal, a small region at the plasma edge where temperature 
and density drop off. For these two fixed 
\(\rho\) values of 0.6 and 0.8 were chosen. Because the \(\rho\)-basis changes for 
different time points and profile values for precisely 0.6 and 0.8 are not included, 
all the relevant profiles had to be interpolated. Figure \ref{chap5_img_time_evolution} shows 
the results for this over a time interval from 2.5 to 3.5 seconds. The bottom plot 
shows the total integrated ECRH power over time for the shot. At 3 seconds, the ECRH is activated 
with a total power of around 5 MW. The temperature for both radial points shown in the plot above 
rises with the activation of the ECRH, as expected. The upper two plots show the 
time evolution of \(\chi_e\) for those radial points and the corresponding fit of the model 
ensemble. Together with the temperature, \(\chi_e\) also rises with the activation of 
the ECRH. Similar to the results before, we see that the ensemble, where the shot 
was included in the training data, is better at fitting \(\chi_e\). Especially in the H-mode region, the model that was trained without the data underestimates \(\chi_e\), which is also observed for 
the profile fit at 3.4 seconds shown in figure \ref{chap5_img_baseline_fit} 
and can be explained with the same argument made before.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{img/time_evolution.png}
    \caption[Time evolution of fits]
    {The bottom plot shows the time evolution of the total integrated ECRH power 
    for shot 35894 over time. The plots above depict the time evolution of 
    two radial points (\(\rho=0.6\), \(\rho = 0.8\)), for both the temperature as 
    well as for \(\chi_e\).}
    \label{chap5_img_time_evolution}
\end{figure}


\subsection{Parameter Dependence of the Model}

Next, we test if the model is able to reproduce parameter dependencies for the 
heat transport mentioned in section \ref{chap2_turbulent_transport}. 
We again use an ensemble of ten models trained for 40 epochs. 
First, we test how the model reacts to a variation of the temperature gradient 
scale \(\frac{a}{L_T} = \frac{a}{T}\frac{\partial T}{\partial r}\). For this, 
the median of input variables within the database (see table \ref{appendix_tab_median})
was used as input to the models of the ensemble, and the respective gradient 
length scale 
for the electron and ion temperature was varied between 0 and 8. Figure 
\ref{chap5_img_temp_scale_scan} shows the results of this scan together with the 
uncertainty of the ensemble. In the plot's background, the joint distribution 
of \(\chi\) and the temperature gradient scale is plotted for both the electrons 
and the ions. For the electrons, we see that the uncertainty of the ensemble 
is small in a region of \(a/L_{Te}\) between 1.5 and 3.5. This is where most 
of the training data is concentrated and shows that the model learns better in this
parameter region, as anticipated. From \(a/L_{Te} = 3.5\) to around 5, 
\(\chi_e\) rises slightly, which can be regarded as physically expected behavior.
Beyond gradient scales of 5 to 6, the database gets scarce, resulting in a
broad uncertainty and a flat curve since the model has almost no data 
to learn any parameter dependence in this parameter region. Subsequent plots 
will therefore be cut off at \(a/L_{T} = 6\). Between 0 and 1, the uncertainty of the 
model is also larger due to the lack of available training data in the database. 
Furthermore, \(\chi_e\) drops with increasing \(a/L_{Te}\) in this region. 
The expected behavior would look like figure \ref{chap2_img_critical_gradient}, 
with \(\chi\) starting at 0, rising slowly with increasing temperature gradient scale 
until a critical gradient is reached, which leads to a rapid increase in the 
diffusion coefficient. Except for the aforementioned slight rise in \(\chi_e\)
between 3.5 and 5, the curve shows none of the expected behaviors. 
For the assumption of the model, that small gradient length scales 
correspond to a larger heat transport, which results in the drop of \(\chi\)
for small gradient length scales, no convincing cause was determined 
by the end of the thesis. Among other things, the distribution of the 
various input parameters for data points with \(a/L_T\) between 0 and 1 was compared 
to the overall total distribution within the complete database. However, no deviations 
explaining the behavior of the model were found. The lack of valid training data 
in the parameter region can only be attributed to some degree. While some data points 
with high \(\chi\) and simultaneously low \(a/L_T\) exist, they are too scarce to 
explain the behavior. It is, in general, a problem to reverse engineer the behavior 
of neural networks. The problem is commonly known as the black box problem. 
The behavior and decision-making of a trained neural network is 
inherently difficult to understand and opaque. A detailed dissection of the training data, along 
with a detailed sensitivity analysis of various input parameters to the model, 
might give some insight. There is, however, no universal solution to this problem.

For the ions, we also observe that in stark contrast to the physical expectation, 
\(\chi_i\) drops with increasing \(a/L_{Ti}\). Compared to the electrons, the 
drop is slower and continues up to a gradient length scale of around 5-6. Beyond 
that, as for the electrons, the training data gets scarce, and the model does not seem 
to learn any parameter dependence. Compared to the electrons, there is also no 
smaller uncertainty in the ensemble's prediction around \(a/L_{Ti}=1\) to 3, 
where the majority of the data points is located. This can be explained by the 
marginal distribution of \(\chi_i\). As seen in section \ref{chap5_database}, the 
distribution is more smeared out and has an accumulation of data points close to 
0. This prevents models from learning a uniform parameter dependence 
even in parameter regions where a lot of training data is available.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{img/temp_scale_scan.png}
    \caption[Variation of temperature scale]
    {Variation of the temperature scale as an input parameter of the model. The mean
    fit of the ensemble together with the uncertainty is shown for both electrons 
    and ions. In the background, the joint distribution of the respective \(\chi\)
    gradient scale is plotted.}
    \label{chap5_img_temp_scale_scan}
\end{figure}

The same was done with additionally varying the safety factor \(q\), the 
temperature ratio and the magnetic shear \(s\). To see if this would overall 
decrease or increase \(\chi\) according to section \ref{chap2_cricical_gradient}.

\subsubsection{Variation of Safety Factor}

Figure \ref{chap5_img_q_scan} shows the results of varying the temperature 
gradient scales for four different values of \(q\). As seen in section 
\ref{chap2_cricical_gradient}, increasing \(q\) would lower the critical gradient 
and increase \(\chi\). For the electrons we observe, between the region of 
\(a/L_{Te} = 2\) to 3, that \(\chi\) indeed increases from \(q=1.5\) up to 
\(q=3\). The curve for \(q = 1.1\) is the only one not following the trend. 
Overall the curve behaves atypical, as it is mostly flat even for small gradient 
scales. This could be explained with \(q=1.1\) being at the edge of the 
distribution for \(q\) since values for \(q\) below 1 are not used. Most data points are centered around \(q=1.5\) to 2. The bottom line, considering the uncertainties 
of the individual curves and that for values of \(a/L_{Te}\) outside of this parameter 
region, the dependence is not clearly given is that the model 
shows slight hints of the expected behavior 
for \(\chi_e\), but overall the uncertainties and the behavior of the 
model outside of the aforementioned parameter region make a clear conclusion 
not possible. 
For \(\chi_i\) the uncertainties of the individual curves are larger, making 
a clear distinction between the curves within one standard deviation 
not possible. Overall the model does not show the expected behavior for the ions.


\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{img/q_scan.png}
    \caption[Variation of safety factor]
    {Variation of the gradient length scale for different values of \(q\).}
    \label{chap5_img_q_scan}
\end{figure}


\subsubsection{Variation of Magnetic Shear}

In both cases, the model shows no difference in changing the magnetic shear. 
The physical expectation would be that increasing the shear leads to a reduction 
in the transport. It is possible that using the commonly used definition 
of \(s\) instead of the one used in \cite{Meneghini2017} (see section \ref{chap4_model_inputs})
could give different results, but it is unlikely that they would differ much from 
the results found for \(q\) and the temperature ratio and that the model would 
replicate the expected behavior with this change.

\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{img/s_scan.png}
    \caption[Variation of magnetic shear]
    {Variation of the gradient length scale for different values of the 
    magnetic shear.}
    \label{chap5_img_s_scan}
\end{figure}


\subsubsection{Variation of Temperature Ratio}

The results for different values of the temperature ratio \(T_e/T_i\) is 
shown in figure \ref{chap5_img_t_ratio_scan}. Comparing the result for the ions 
and electrons, we see that again for the ions, the uncertainties are larger, 
making a distinction between the different curves difficult, and overall no uniform 
parameter dependence of \(\chi_i\) on the temperature ratio is visible. 
The expectation would be that \(\chi_i\) rises with an increase 
in the temperature ratio. For \(\chi_e\), the expectation is the opposite. 
While the uncertainties of individual curves are lower for \(\chi_e\), there is, unfortunately, also 
no clear sign of the expected behavior. 


\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{img/t_ratio_scan.png}
    \caption[Variation of temperature ratio]
    {Variation of the gradient length scale for different values of the 
    temperature ratio \(T_e/T_i\).}
    \label{chap5_img_t_ratio_scan}
\end{figure}

\subsection{Problems from Using Experimental Data}\label{chap5_results_problems}

As we have seen in the previous two sections, the uncertainty of the models on 
unseen data is large, and it is not possible to derive many meaningful results 
from them, except for replicating training data. 
Considering the experimental source of the database, this has most 
likely two reasons, especially when comparing the approach of this thesis 
to other work, where models for simulated data were trained.
\begin{itemize}
    \item Using simulated data has the advantage that one can cover 
        the input parameter space arbitrarily well by simply computing the results 
        for the desired input parameters and adding them to the training database. 
        Experimental data is limited by the capability of the experiment, which 
        only covers a fraction of the parameter space. This is evident by 
        the highly peaked distribution of most input parameters in the database 
        (see figure \ref{appendix_img_histograms} or \ref{chap5_img_chi_hist}).
        Since the data points are mostly centered around common values. This makes 
        it difficult for the model to properly learn the different parameter dependencies 
        as there is not enough variation in the training data.
    \item The results from simulations like TGLF are self-consistent, meaning 
        that given the same input parameters, the simulation returns the exact same 
        results. Experimental data, on the other hand, is inherently noisy and can 
        contain systematic measurement errors etc. All this reduces the 
        correlation between the different parameters. In the most extreme case, 
        one could imagine a model trained with completely random white noise. 
        Given a large enough model, the network can learn the data in the training 
        dataset arbitrarily well. It would, however, not be able to generalize on 
        new unseen data, and the loss for the validation dataset would remain large 
        no matter how complex the model and how large the training dataset is. 
        The data in our database is clearly not completely uncorrelated, as the model 
        can approximate data points in the validation dataset to some degree. However, the 
        noise and uncertainty in the database seem too big for the model 
        to generalize well enough to make meaningful predictions. 
\end{itemize}