\chapter{Plasma Equilibrium and Transport}

\section{Plasma Equilibrium and Important Plasma Parameters}

Figure \ref{chap2_img_toroidal_coordinates} shows the different coordinates used to describe a tokamak or a torus in general. A torus is axisymmetric (\(\frac{\partial}{\partial \Phi} = 0\)), and the plasma within a Tokamak is in equilibrium. Theoretically, the plasma within a Tokamak can be described via magnetohydrodynamics (MHD). There the plasma is treated as a conductive fluid instead of treating particles individually like it is done in kinetic theory. In equilibrium, the thermal pressure of the plasma is balanced by the magnetic pressure. So the following force balance is given
\begin{equation}\label{chap2_eq_force_equililbrium}
    \vec{j} \times \vec{B} = \nabla p
\end{equation}
Here \(\vec{j}\) is the current density, \(\vec{B}\) is the magnetic field and \(p\) is the pressure. Due to the axisymmetric nature of a tokamak, one can express the equilibrium with the poloidal cross-section and within the R-Z-plane (see figure \ref{chap2_img_toroidal_coordinates}). This leads to the Grad-Shafranov equation, describing the equilibrium
\begin{equation}
    R\frac{\partial}{\partial R}\left(\frac{1}{R}
    \frac{\partial \psi}{\partial R} \right) + 
    \frac{\partial^2 \psi}{\partial z^2} = 
    -\mu_0(2\pi R)^2\frac{\partial p}{\partial \psi}
    -\mu_0^2 I_\mathrm{pol}\frac{\partial I_\mathrm{pol}}{\partial \psi}
\end{equation}
Here, \(\psi\) is the poloidal magnetic flux, \(p\) is the pressure and \(I_\mathrm{pol}\) is the poloidal current. A derivation of the Grad-Shafranov equation from equation \ref{chap2_eq_force_equililbrium} can be found in \cite{PlasmaphysikBuch}.

The solution of the Grad-Shafranov equation is given as an (infinite) set of flux surfaces with constant magnetic flux, constant pressure and constant poloidal current. It is common to define a normalized plasma radius based on the poloidal magnetic flux. The normalized (poloidal) magnetic flux is given by
\begin{equation}
    \psi_N = \frac{\psi - \psi_\mathrm{axis}}{\psi_\mathrm{lcfs}-\psi_\mathrm{axis}}
\end{equation}
\(\psi_\mathrm{axis}\) is the magnetic flux at the magnetic axis, \(\psi_\mathrm{lcfs}\) is the magnetic flux at the last closed flux surface. The normalized radius is then defined as
\begin{equation}
    \rho_\mathrm{pol} = \sqrt{\psi_N}
\end{equation}
\(\rho_\mathrm{pol} = 0\) is equal to the magnetic axis, while \(\rho_\mathrm{pol} = 1\) would be the normalized radius of the separatrix (unless the plasma is in a limiter configuration without separatrix). Figure \ref{chap2_img_equililbrium_cross_section} shows the magnetic equilibrium for both a divertor configuration with separatrix and a limiter configuration. Notably, one can also define \(\rho_\mathrm{tor}\) where the normalized toroidal flux is used. For this work, we will use \(\rho_\mathrm{pol}\) since profiles for the IDA/IDE code (see section \ref{chap4_diagnostics}) are computed for \(\rho_\mathrm{pol}\).

\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{img/toroidal_coordinates.png}
    \caption[Toroidal coordinates]
    {Illustration of toroidal coordinates. The major radius \(R\)
    is defined from the Torus center, while the minor radius is the radius of the tube or the plasma cross-section in the context of Tokamaks.
    \tiny{(Source: \url{http://fusionwiki.ciemat.es/wiki/File:Toroidal_coordinates.png})}}
    \label{chap2_img_toroidal_coordinates}
\end{figure}


\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{img/equilibrium_cross_section.png}
    \caption[Equilibrium for divertor and limiter configuration]
    {\textbf{a)} Magnetic equilibrium for a typical divertor configuration, 
    where the flux surfaces are closed within the separatrix (bold black line) and
    open outside of it. The equilibrium is taken from shot 41570 at t = 2s.
    \textbf{b)} Magnetic equilibrium for a limiter configuration. Here 
    the last closed flux surface (bold black line) is given by the 
    flux surface that touches the vessel wall (limiter). The equilibrium is taken 
    from shot 29722 at t = 2s. In both plots, flux surfaces with different 
    values for \(\rho_\mathrm{pol}\) are plotted as black dashed lines.}
    \label{chap2_img_equililbrium_cross_section}
\end{figure}

\subsection{Safety Factor \(q\)}\label{chap2_safety_factor}

The safety factor \(q\) is given by the number of toroidal rotations of the magnetic field lines for one poloidal rotation. So \(q=1\) would mean that after one revolution in the toroidal direction, the flux line ends up at the exact same position. \(q\) is given by
\begin{equation}
    q = \frac{d\phi}{d\psi}
\end{equation}
Here \(\phi\) is the magnetic flux in the toroidal direction, and \(\psi\) is the flux in the poloidal direction. Like the poloidal flux or the pressure, the safety factor is also a constant along flux surfaces. In the case of a large aspect ratio (\(\frac{R}{r}\)) and a circular cross section \(q\) can be approximated \cite{wesson2004tokamaks}.
\begin{equation}
    q\approx \frac{rB_\phi}{RB_\psi}
\end{equation}

The term safety factor stems from the fact that it plays a vital role in the stability of the plasma. Especially rational \(q\) values are prone to instabilities \cite{wesson2004tokamaks}. The case \(q=1\) is relevant for this thesis since it leads to sawtooth crashes within the plasma, characterized by slow buildups of temperature in the core followed by sudden drops (crashes). This can lead to an increase in heat transport and lowers the confinement and performance of the plasma. In general, the safety factor grows continuously with increasing minor radius. The change in the safety factor is given by the (normalized) magnetic shear \(s\). 
\begin{equation}
    s = \frac{r}{q}\frac{dq}{dr}
\end{equation}
Both \(q\) and \(s\) are not only crucial for the stability of the plasma, but they also influence the transport within the plasma, as shown in the following sections.

\subsection{Plasma Beta \(\beta\)}

Another important plasma parameter is the plasma beta \(\beta\) given by the ratio of the plasma pressure to the magnetic pressure
\begin{equation}
    \beta = \frac{\langle p\rangle}{B_0^2/2\mu_0}
\end{equation}
\(\beta\) indicates the efficiency of the plasma confinement. Larger values of \(\beta\) are desirable, as a higher pressure results in a higher energy density in the plasma and more fusion reactions, and higher power output in a future power plant and lower magnetic fields are easier and more economical to realize. The plasma beta is usually a global parameter with the averaged pressure and the magnetic field at the magnetic axis. One can, however, also define a local beta with the pressure of the flux surface and the average magnetic field of the flux surface. For current tokamak devices the achievable \(\beta\) is around 
a few percent \cite{neilsonBook}.

\section{Transport Equations}

The change in energy over time of a certain plasma volume is given by 
\begin{equation}
    \frac{\partial E}{\partial t} = -\oint\vec{q}d\vec{A} + \int S dV
\end{equation}
The first term on the right side integrates the heat flux \(\vec{q}\) over 
the plasma surface. The second term integrates the source term \(S\), 
which contains all the energy sources and sinks over the plasma volume.
For a fusion plasma, the sources and sinks are given by external heating, 
heating from the fusion reactions and energy losses due to radiation or particle loss.
Introducing the energy density \(w\) and rewriting 
the surface integral into a volume integral over \(\nabla\vec{q}\) one can rewrite the 
integral equation into a differential equation
\begin{equation}
    \frac{\partial w}{\partial t} = -\nabla\vec{q} + S
\end{equation}
Temperature gradients drive the heat flux within the plasma.
By introducing the heat conductivity \(\kappa = n\chi\), where \(n\) is the density 
and \(\chi\) has the form of a diffusion coefficient, the heat flux can be expressed as 
\begin{equation}
    \vec{q} = -n\chi\nabla T
\end{equation}
Parallel to the magnetic field, the particles in the plasma can move freely, and 
the heat conductivity is so large that, in practice, no temperature gradients can build up.
Hence the plasma is in thermal equilibrium toroidally, and the temperature is a flux 
surface constant \cite{PlasmaphysikBuch}. The poloidal density and temperature profile can be measured directly 
(see section \ref{chap4_diagnostics}). The remaining unknown for the radial 
heat flux is hence \(\chi\). Assuming a steady state with 
\(\frac{\partial w}{\partial t} = 0\)
we can compute \(\chi\) with a power balance over all the sinks and sources as described 
in section \ref{chap4_powerbalance_and_outputs}. We will now first look into the 
theory behind radial heat transport within a tokamak.


\section{Collisional Transport}

The particles are confined by the magnetic field and cannot move freely perpendicular to 
it. Hence it is safe to assume that radial transport is mainly given by collisions 
between them. The process of heat and particle transport via collisions 
is called diffusion.

\subsection{Diffusion as Random Walk}\label{chap2_random_walk}

 
From the viewpoint of a single particle, diffusion can be modeled as a random walk, 
where for every time step \(\delta t\), the particle experiences an offset \(\delta x\)
through collisions. In the 1-dimensional case, the offset is given by either \(+\delta x\)
or \(-\delta x\), with equal likelihood. This leads to a stochastic process  
by concatenating several of those random moves. 
The number of total steps is given by \(N = \frac{t}{\delta t}\). 
Additionally, we can write \(N = N_++N_-\) where \(N_\pm\) indicates the total number 
of steps in the two possible directions. The amount of steps that the particle is away from the origin
after \(N\) total steps is given by \(d = N_+-N_-\).
The total distance from the origin after N steps is then expressed as 
\begin{equation}
    x = d\delta x
\end{equation}
The likelihood of ending up \(d\) steps away from the origin after \(N\) of those 
random moves is given by a binominal distribution.
\begin{equation}
    P(d, N) = \frac{1}{2^N}\binom{N}{\frac{N+d}{2}} = \frac{1}{2^N}\frac{N!}{N_+!N_-!}=
    \frac{1}{2^N}\frac{N!}{(\frac{1}{2}(N+d))!(\frac{1}{2}(N-d))!}
\end{equation}
With Stirling's approximation, this can be approximated as \cite{PlasmaphysikBuch}
\begin{equation}
    P(d, N) = \sqrt{\frac{2}{\pi N}}\exp\left({-\frac{d^2}{2N}}\right)
\end{equation}
We see that this takes the form of a normal distribution with mean 0 and 
the standard deviation \(\sigma \propto \sqrt{N}\). If we look at an ensemble of 
particles, this would mean that there is no transport. But the distance that individual 
particles move from the origin, which is given by \(\sigma\), 
grows with time since \(N\propto t\). 
This changes in the presence of gradients. 
We can write the particle flux at position \(x_0\) over the time 
\(\delta t\) as a difference over the flux in positive and negative 
\(x\) direction
\begin{equation}
    \Gamma_{x_0} = \Gamma_{+} - \Gamma_{-} = 
    \frac{1}{2\delta t}\int_{x_0-\delta x}^{x_0}n(x)dx -
    \frac{1}{2\delta t}\int_{x_0}^{x_0 + \delta x}n(x)dx
\end{equation}
Here \(n\) is the particle density. 
The \(\frac{1}{2}\) in front of the integrals comes from the fact that (without
gradient) half of the particles flow to the left and half to the right.
With a small enough \(\delta x\), the gradient can be assumed to be constant and we 
can write 
\begin{align*}
    \Gamma_{x_0} &= \frac{1}{2\delta t}\left(n(x_0)\delta x - 
        \frac{(\delta x)^2}{2}\left.\frac{dn}{dx}\right|_{x=x_0}\right)
        - \frac{1}{2\delta t}\left(n(x_0)\delta x + 
        \frac{(\delta x)^2}{2}\left.\frac{dn}{dx}\right|_{x=x_0}\right)\\ 
        &= -\frac{(\delta x)^2}{2\delta t}\left.\frac{dn}{dx}\right|_{x=x_0}
\end{align*}
By introducing the diffusion constant \(D\)
\begin{equation}
    D = \frac{(\delta x)}{2\delta t}
\end{equation}
we arrive at the general result for diffusive transport
\begin{equation}
    \Gamma_{x} = -D\frac{dn}{dx}
\end{equation}
Usually, the diffusion constant for particle transport is denoted with \(D\) and
the diffusion constant for heat transport with \(\chi\).

\subsection{Classical Transport}\label{chap2_classical_transport}

For the plasma we can assume that \(\delta t\) is given by the collision time 
\(\tau_\perp = \frac{1}{\nu_\perp}\) or by the inverse of the collision frequency for 
collisions perpendicular to the magnetic field. We have to differentiate between 
ion collisions, electron collisions and electron-ion collisions.
The collision frequency for both electrons and ions is dependent on 
the density and the temperature \cite{PlasmaphysikBuch,PlasmaFormulary}
\begin{equation}
    \nu \propto nT^{-3/2}
\end{equation}
The average particle displacement per collision \(\delta x\) is just given by 
the larmor radius, which is the radius of the gyration movement of the particle 
around the magnetic field line. For a specific particle species \(s\) the 
larmor radius is given by 
\begin{equation}
    r_{L,s} = \frac{m_sv_{s,\perp}}{q_sB} = \frac{\sqrt{2m_sT_s}}{q_sB}
\end{equation}
Here \(v_{s,\perp}\) the velocity perpendicular 
to the magnetic field \(B\), which can be approximated by the thermal velocity.
For particle transport, collisions between the same particle species don't contribute 
to overall particle transport since the particles merely swap places. For heat 
transport, on the other hand, both collisions between different particle species and 
collisions among the same species contribute since a more energetic particle can 
transfer energy to a less energetic one. 
With this knowledge, we can express the heat diffusion coefficients as 
\begin{equation}
    \chi_e = \frac{1}{2}r_{L,e}^2(\nu_{ee} + \nu_{ei}), \qquad 
    \chi_i = \frac{1}{2}r_{L,i}^2\nu_{ii}
\end{equation}
Since collision between electrons and ions mostly transfer energy from 
ions to electrons due to the large mass difference, we have to include them 
for the electron heat transport but not for ions. Because of the mass dependence 
of both \(r_L\) and \(\nu\) we have the following relationship between \(\chi_e\)
and \(\chi_i\)
\begin{equation}
    \chi_i \approx \sqrt{\frac{m_i}{m_e}}\chi_e
\end{equation}
One would therefore assume that ions dominate the heat transport within the plasma.
Because of the temperature dependence of \(r_L\) and \(\nu\) the transport coefficients
would also get smaller with \(T^{-1/2}\). Comparing this to experimental observations, 
one finds not only increasing heat transport with larger temperatures
and that the contribution from electrons is of the same order of magnitude as that 
of ions. The transport 
coefficients are also much larger than the expected values from classical transport. 
Using typical values (\(T = 10\) keV, \(n = 10^{20}\) \(\mathrm{m^{-3}}\) and 
\(B = 2\) T) one would assume diffusion coefficients in the range of 
\(D\approx\chi\approx10^{-5}-10^{-4}\, \mathrm{\frac{m^2}{s}}\). In experiments 
the values are typically \(D \approx 0.1\, \mathrm{\frac{m^2}{s}}\) and 
\(\chi \approx 1\, \mathrm{\frac{m^2}{s}}\) \cite{wesson2004tokamaks}. 
This shows that other effects dominate perpendicular transport within the plasma. 

\subsection{Plasma Drifts}\label{chap2_drifts}

Particles in the plasma exert a drift motion under the influence of a force 
perpendicular to the magnetic field. The drift velocity is given by 
\begin{equation}
v_D = \frac{1}{q}\frac{\vec{F}\times\vec{B}}{B^2}
\end{equation}
In astrophysics, the gravitational force, therefore, leads to drift effects. For 
experiments on earth, the effect is negligible, though. There is, however, a range 
of different drift effects that are relevant for fusion experiments:
\begin{itemize}
    \item \textbf{\(\mathrm{\mathbf{E\times B}}\)-Drift}\\
        The resulting drift under the presence of of an electric field (\(\vec{F} = q\vec{E}\)) 
        is given by
        \begin{equation}
            v_D = \frac{\vec{E}\times\vec{B}}{B^2}
        \end{equation}
        In this specific case, the drift is not dependent on the charge, and both 
        electrons and ions move in the same direction.
    \item \textbf{Diamagnetic-Drift}\\
        In the presence of a density gradient, each particle experiences a force 
        \(F = -\nabla p/n\). Hence, the resulting drift velocity is 
        \begin{equation}
            v_D = -\frac{\nabla p\times \vec{B}}{qnB^2}
        \end{equation}
    \item \textbf{\(\mathrm{\mathbf{\nabla B}}\)-Drift}\\
        An inhomogeneous magnetic field causes particle drifts in two different ways. 
        First, particles experience a force due to the gradient of the magnetic field 
        itself \(\vec{F} = \mu\nabla B\). Here \(\mu\) is the magnetic moment of the 
        particle. The resulting drift is given by 
        \begin{equation}
            v_D = - \frac{mv_\perp^2}{2qB^3}\nabla B \times \vec{B}
        \end{equation}
        Here \(v_\perp\) is the velocity perpendicular to the magnetic field.
    \item \textbf{Curvature-Drift}\\
        The second reason for drift due to an inhomogeneous magnetic field is 
        given by the curvature of the field lines. Particles moving along the bend 
        lines experience a centrifugal force. The resulting drift is
        \begin{equation}
            v_D = - \frac{mv_\parallel^2}{qB^3}\nabla B \times \vec{B}
        \end{equation}
        Here \(v_\parallel\) is the velocity parallel to the magnetic field.
\end{itemize}
 
The last two drift velocities can be summarized into one equation 
\begin{equation}\label{chap2_eq_B_drift}
    v_D = - \frac{m}{qB^3}\left(\frac{1}{2}v_\perp^2 + v_\parallel^2\right)\nabla B \times \vec{B}
\end{equation}
This drift not only plays an important role in neoclassical transport, 
as we will see in the next section. Together with 
the \(\mathrm{E\times B}\)-drift it is also the reason why a toroidal field alone 
is not sufficient for plasma confinement. In a toroidal field alone, the drift 
from the inhomogenous magnetic field would lead to charge separation and the resulting 
\(\mathrm{E\times B}\)-drift would move both the electrons and ions radially outwards.

\subsection{Neoclassical Transport}\label{chap2_neoclassic}
The magnetic moment \(\mu\) of a particle is a conserved quantity. 
\begin{equation}
    \mu = \frac{mv_\perp^2}{2B} = \mathrm{const}
\end{equation}
This means that a changing field \(B\) causes a change in the velocity perpendicular 
to the magnetic field 
\(v_\perp\) of the particle. On the other hand the, kinetic energy 
\(\frac{1}{2}m(v_\perp^2+v_\parallel^2)\) is also a conserved quantity, which leads 
to a change in parallel velocity \(v_\parallel\) of the particle. Under certain 
conditions, this can lead to a flip of sign for the parallel velocity. Because the 
particle is "reflected". This is called a magnetic mirror. 
The resulting mirror 
condition is given by 
\begin{equation}
    \frac{v_\parallel^2}{v_\perp^2} < \frac{B_\mathrm{max}}{B_\mathrm{min}}-1
\end{equation}
Particles moving in an inhomogeneous magnetic field whose velocity fulfills this 
condition, cannot move freely and are trapped as they are reflected back and forth.
The magnetic field in a toroidal geometry is inhomogenous, and particles experience 
a higher toroidal field when they are closer to the torus center than those 
further away. This is due to the \(1/R\) dependency of the toroidal field. For 
a small inverse aspect ratio \(\epsilon = \frac{r}{R} \ll 1\) we can therefore rewrite 
the condition into
\begin{equation}
    \frac{v_\parallel^2}{v_\perp^2} < \frac{B_0(R_0 + r)}{B_0(R_0-r)} - 1 
    = \frac{2r/R_0}{1-r/R_0} \approx 2\epsilon
\end{equation}
From the Maxwellian velocity distribution of the particles, one can estimate that 
the relative amount of trapped particles is given by 
\begin{equation}
    \frac{n_t}{n} \approx \sqrt{2\epsilon}
\end{equation}
Trapped particles cannot complete a poloidal trajectory within the toroidal geometry. Since they move through the inhomogeneous field, they additionally 
experience an up- or downward displacement due to the \(\nabla B\)- and 
curvature drift. Which leads to an oscillating change in the poloidal 
radius of the particle. Looking at the poloidal cross-section, the particle 
trajectory looks banana-shaped, hence, the name banana-trajectory. 
Figure \ref{chap2_img_banana_trajectory} shows the poloidal cross-section of 
a banana trajectory. 
\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{img/banana-orbit.png}
    \caption[Banana-trajectory]
    {This figure shows the poloidal cross-section of a banana trajectory. The 
    particle (ion) starts moving clockwise from the upper reflection point. 
    The drift velocity is upwards 
    , moving the particle radially outwards until 
    the particle passes the horizontal center line. Now the particle is moved radially 
    inwards until it reaches its initial flux surface again at the second (lower)
    reflection point. From now on, the particle moves counterclockwise while 
    still being moved radially inwards until it passes the horizontal center again.
    Due to the opposite charge, an electron would experience the whole sequence in the opposite 
    direction.
    The width of the banana trajectory \(w_B\) is given by the total radial displacement 
    during one full banana orbit.
    \tiny{(Source: Adapted from \cite[p.~419]{PlasmaphysikBuch}})}
    \label{chap2_img_banana_trajectory}
\end{figure}
The width \(w_B\) of the banana trajectory can be approximated by \(w_B = v_D\delta t\), 
where \(v_D\) is the drift velocity and \(\delta t\) the time for one full banana-trajectory. 
For this, one can obtain the following approximation
\begin{equation}
    w_B \approx r_L\frac{q}{\sqrt{\epsilon}}
\end{equation}
Here \(r_L\) is the lamor radius and \(q\) the safety factor 
(see section \ref{chap2_safety_factor}).
For the collision time \(\tau\), one classically assumes the time it takes for a 
particle deflection of 90° (which usually occurs as a summation of several collisions 
with smaller deflection angles). However, for particles on a banana trajectory, 
smaller angles are sufficient to free the particle from its trapped state. In detail 
one gets an effective collision frequency
\begin{equation}
    \nu_\mathrm{eff} \approx \frac{\nu}{2\epsilon}
\end{equation}
Following our random walk argument (section \ref{chap2_random_walk}) and 
using \(w_B\) as displacement length and 
\(v_\mathrm{eff}\) we get the following for the neoclassical diffusion coefficients
\begin{equation}
    D_\mathrm{neo} = \frac{1}{2}w_B^2\nu_\mathrm{eff}\frac{n_t}{n}
    \approx \frac{q^2}{\epsilon^{3/2}}D_\mathrm{class}
\end{equation}
\(D_\mathrm{class}\) is the diffusion coefficient from classical transport obtained 
in section \ref{chap2_classical_transport}. For common values of \(\epsilon = 0.1\) 
and \(q = 2\), one would get 
\begin{equation}
    D_\mathrm{neo} \approx 100D_\mathrm{class}
\end{equation}
The neoclassical results for particle and electron heat transport are 
still around 100 times below the observed values. However, for ion heat transport, observed values 
of \(\chi_i\) are only 1-10 times 
larger than the neoclassical result, meaning that neoclassical effects have a 
non-negligible role \cite{wesson2004tokamaks}. A more detailed derivation and 
treatment of neoclassical transport is given in 
\cite{wesson2004tokamaks,PlasmaphysikBuch}.

\section{Turbulent Transport}\label{chap2_turbulent_transport}

We have seen that neoclassical transport is still insufficient for describing 
radial transport within a tokamak. Historically all transport effects not belonging 
to classical or neoclassical transport were summarized under the 
term anomalous transport. 
While details about anomalous transport are an ongoing research topic, it is 
undisputed that turbulences and disturbances in the plasma cause the higher observed transport \cite{PlasmaphysikBuch}. 
Turbulences lead to the emergence of eddies in the plasma. If we assume an 
eddy size of 1 cm and a lifetime of 100 \(\mathrm{\mu s}\), which are commonly 
observed values and use our random walk argument again, we arrive at a 
diffusion coefficient of around 1 \(\mathrm{m^2s^{-1}}\) which matches 
experimental values \cite{scriptPlasma}.

Turbulence cannot be handled analytically and requires computationally expensive 
numerical simulations. In the following sections, we will introduce some instabilities 
that lead to the emergence of turbulences and introduce the critical gradient. 
The following sections are roughly based on \cite{scriptPlasma, PlasmaphysikBuch,AlexPHD}.


\subsection{Electrostatic Turbulence}

First, we will look at instabilities arising from fluctuating electrical fields.
Fluctuations in density or temperature can cause these. 

\subsubsection{Drift Wave Instability}

Drift waves can occur in an arbitrary magnetic field configuration, with a 
density gradient perpendicular to the magnetic field. 
A periodic perturbation of the density along the magnetic field lines leads to a flow 
of electrons, which are more mobile than ions, due to their lower mass, to less dense regions. 
Under idealized conditions, one can assume that the electrons behave adiabatically, meaning 
they react instantaneously to the pressure perturbation. This leads to accumulations 
of electrons in regions with lower density and an excess of ions in denser parts. This 
in turn causes a periodic perturbation of the electric potential. Figure 
\ref{chap2_img_driftwave_instability} shows the perturbation of the density 
and the potential in the poloidal plane. The plot is zoomed in so that the flux surface lines 
look straight. The accumulations of positive and negative 
charges shown in the plot are caused by the periodical density perturbation along the 
magnetic field, which points into the plane. The periodic potential fluctuation 
causes electric fields. Therefore, the \(E\times B\)-drift leads to alternating drift 
directions, which are also shown in the figure. In the case of adiabatic electrons, 
both the perturbation of the density and the potential are in phase. From the plot, 
it is evident that, in this case, perturbations are simply shifted along the poloidal 
plane. Hence, the name drift wave. In this configuration no transport occurs, and the 
wave simply moves in the poloidal plane. What happens if both perturbations 
are out of phase is shown in figure \ref{chap2_img_interchange_instability}. There the 
drift velocities lead to an amplification of density perturbations, which can then 
become unstable and lead to turbulent transport. The shift between both perturbations 
can be caused by effects that hinder the adiabatic response of the electrons to the 
density perturbation. The adiabatic response is hindered by the resistivity of the 
plasma and also by trapped electrons, which were introduced in the previous section about 
neoclassical transport. 


\subsubsection{Interchange Instability}

Interchange instabilities can be treated as purely 2-dimensional and only occur 
in the existence of a curved field configuration.
Looking at equation \ref{chap2_eq_B_drift}, we see that 
the total drift from curvature and \(\nabla B\) is proportional to the 
kinetic energy of the particles, which is just given by their thermal velocity.
A periodic perturbation in the temperature, therefore, means that the drift is 
weaker for regions
with lower temperatures and stronger for regions with higher temperatures. The 
difference in drift velocity then leads to a perturbation of the particle density. 
Due to their much lower mass, we can assume that the electrons behave adiabatically 
and follow the ions immediately to preserve the plasma's quasineutrality. 
The movement is caused by electric fields arising from the perturbation in the 
potential due to the density perturbation. Compared to the drift wave above, both 
perturbations are now phase shifted by 90 degrees. 
Figure \ref{chap2_img_interchange_instability}
shows the case where the density gradient and the magnetic field 
point in the same direction, as is the case for the outer half of a tokamak or 
its low field side. In this case, the resulting drifts have an amplifying effect on 
the perturbation. Once the perturbation gets large enough, turbulences can emerge. 
For the inner part or the high field side of the tokamak, \(\nabla B\) would 
point in the opposite direction, also resulting in an opposite drift movement, 
dampening the perturbation.
 
For electrostatic turbulence, one distinguishes between ion temperature gradient (ITG) 
modes, electron temperature gradient (ETG) modes and trapped electron modes (TEM), depending 
on the underlying mechanism causing the instability. 

Perturbations in the density lead to compensating currents, which cause 
micro fluctuations in the magnetic field. Including such effects in the 
description of instabilities leads to electromagnetic turbulence. Treatment of 
such effects is beyond the scope of this thesis, though.



\begin{figure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=\textwidth]{img/driftwave_instability.png}
        \caption[Drift-wave instability]
        {Drift-wave instability}
        \label{chap2_img_driftwave_instability}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=\textwidth]{img/interchange_instability2.png}
        \caption[Interchange instability]
        {Interchange instability}
        \label{chap2_img_interchange_instability}
    \end{subfigure}
    \caption[Interchange and drift-wave instability]
        {Interchange and drift-wave instability, \(v_D\) is the resulting 
        drift velocity from the \(E\times B\)-drift. The explanation of 
        the instabilities are given in the text.}
\end{figure}


\subsection{MHD Instability}

Disturbances of the magnetic 
field give another reason for anomalous transport. Such disturbances can lead 
to the formation of so-called magnetic islands. 
An extensive treatment of magnetohydrodynamic (MHD) instabilities and magnetic islands 
and their roles in tokamaks is 
given in \cite{MagneticIslandsHabilitation,MagneticIslandsPHD}, but is beyond 
the scope of this thesis. Figure \ref{chap2_img_magnetic_islands} shows 
the poloidal cross section for an equilibrium with several magnetic islands. 
The heat transport across the radial extent of those islands is significantly increased, 
since particles on field lines within the island move along the closed flux lines 
of the island.
The magnetic islands basically act as a thermal short circuit. 

\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{img/magnetic_islands.png}
    \caption[Magnetic islands]
    {Magnetic equilibrium with several magnetic islands shown in green.
    \tiny{(Source \cite[p.~16]{MagneticIslandsPHD}})}
    \label{chap2_img_magnetic_islands}
\end{figure}

\subsection{Critical Gradients}\label{chap2_cricical_gradient}

As we have seen in the previous sections about anomalous transport gradients 
are responsible for the emergence of instabilities that lead to turbulent 
transport. However, these turbulent effects only occur above critical gradients. 
Introducing the temperature gradient scale \(\frac{1}{L_T} = \frac{\nabla T}{T}\),
a model including the critical gradient would take the following form 
\cite{AUG_heat_transport}
\begin{equation}\label{chap2_chi_with_crit_gradient}
    \chi = \chi_0 + C\left(\frac{1}{L_T} - \frac{1}{L_T, \mathrm{crit}}\right)
            H\left(\frac{1}{L_T} - \frac{1}{L_T,\mathrm{crit}}\right)
\end{equation}
Here \(\chi_0\) is the non-turbulent (neoclassical) diffusion coefficient, \(H\)
is the Heaviside step function and \(C\gg\chi_0\) is a constant. Below 
the critical gradient scale \(\frac{1}{L_T,\mathrm{crit}}\), transport 
would be dominated by neoclassical effects. Once the critical gradient is surpassed, heat transport increases rapidly due to the emergence of turbulent effects.
According to \cite{ITG_critical_gradient, ETG_critical_gradient}, the critical 
gradient for both electrons (ETG mode) and ions (ITG mode) have the following 
dependencies
\begin{equation}
    \left(\frac{R}{L_{Ti}}\right)_\mathrm{crit} \propto \left(1+\frac{T_i}{T_e}\right)
    \left(1+\alpha\frac{s}{q}\right), \qquad
    \left(\frac{R}{L_{Te}}\right)_\mathrm{crit} \propto \left(1+\frac{T_e}{T_i}\right)
    \left(1+\beta\frac{s}{q}\right)
\end{equation}
were \(\alpha\) and \(\beta\) are fitting parameters. Additionally, more precise 
formulas, including geometric parameters like the aspect ratio and the elongation, 
are available \cite{ETG_critical_gradient}. Due to a lowering of the critical 
gradient, one would therefore expect an increased heat transport (for ions) 
for increasing the temperature ratio \(T_e/T_i\) and the safety factor \(q\), or 
for lowering the magnetic shear \(s\). For electrons, a lower temperature ratio
would reduce the critical gradient, on the other hand. The influences of the different parameters 
can, in a simplified way, be explained by the following:
\begin{itemize}
    \item A larger value of \(q\) means that the particles make more toroidal revolutions 
        with each poloidal revolution. This means that with a larger q value, the consecutive 
        time intervals that one particle spends on the low or high field side get larger. 
        In the previous section, we have seen that instabilities like the interchange 
        instability occur on the low
        field side. A longer time interval spent on the low field side means more time for 
        the instability to grow and can therefore lead to increased heat transport.
    \item Larger values of the magnetic shear have a suppressing effect, due to its
        ability to shear apart radially extended fluctuations/instabilities 
        or even potential turbulence eddies could be sheared apart. A more precise argument 
        is given in \cite{AlexPHD}.
    \item A simplified explanation of the temperature ratios influence on  
        heat transport is that a higher ion temperature allows the ions to follow 
        electrons better, as electrons due to their small mass, are assumed to behave adiabatically. 
        This decreases the perturbations of the potential mentioned in the previous section.
\end{itemize}

\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{img/critical_gradient.png}
    \caption[Heat transport with critical gradient]
    {Heat diffusion coefficient \(\chi\) dependency from the temperature gradient 
    scale. Beyond the critical gradient, transport increases rapidly.}
    \label{chap2_img_critical_gradient}
\end{figure}

\section{Motivation for Machine Learning}

In order to compute the anomalous transport within the plasma, one has to use 
simulation codes since there is no analytical expression for it. 
The most precise models are gyro-kinetic codes like GENE \cite{GENEpaper}. 
While they are the most accurate, they are also the computationally most expensive 
models, and evaluating the transport for single radial points can require more 
than 10.000 CPU hours. For common operational regimes of tokamaks, simplified 
linearized models, like TGLF or 
QualiKiz exist, that show good agreement with experimental data.
Those can reduce the computational cost for single data points 
to CPU seconds. For applications where even faster computations are necessary, 
surrogate models based on neural networks for those models were developed 
\cite{Meneghini2017,Ho2021,Plassche2020}. This thesis takes a different 
approach by not relying on computational models at all and computing transport 
coefficients directly from experimental data via a power balance as described 
in section \ref{chap4_database}, with the goal of training a neural network 
for predicting transport coefficients only from experimental data. 