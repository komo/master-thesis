\chapter{Introduction}

\section{Nuclear Fusion}

The demand for energy in our industrialized and globalized world is rising. 
In 2021 it was estimated that roughly 25000 TWh of electrical energy were 
consumed worldwide and from 1990 to 2013 the worldwide consumption doubled 
from 10000 to 20000 TWh \cite{enerdata}. Renewable energy sources constitute around 30\% of worldwide electricity production in 2022. At the moment, about half of all renewable energy comes from hydropower, which is limited by geographic constraints and cannot be expanded easily. Despite the rise over the last two decades, solar and wind power still only make up around 13\% of worldwide energy production. Another 10\% can be attributed to conventional nuclear power. This still leaves 60\% to fossil fuels \cite{ourworldindata} and we only considered worldwide electricity production, neglecting energy needs for heating or transportation, which in large parts do not use electricity. The pressing issue of climate change, limited availability of fossil power sources, and geopolitical tensions in regions with high occurrences of those lead to the search for new, clean, and abundant energy sources. One such energy source can be nuclear fusion. Compared to conventional chemical reactions occurring when burning fossil fuels, nuclear reactions produce roughly a million times more energy per reaction than their chemical counterparts. Eq. \ref{kap1_eq_nuclear_chemical} compares the chemical reaction of a hydrogen-oxygen mixture to nuclear reactions. While for the oxygen-hydrogen reaction only a couple of electron volts are released, nuclear reactions like the nuclear fission of Uranium-235 and the fusion of the two hydrogen isotopes deuterium and tritium release several MeV of energy per reaction. 
\begin{subequations}
    \label{kap1_eq_nuclear_chemical} 
    \begin{align} 
        \mathrm{2H_2 + O_2} &\rightarrow \mathrm{2H_2O + 5.9\ eV} \\ 
        \mathrm{n + {}_{92}^{235}U} &\rightarrow \mathrm{{}_{56}^{141}Ba + {}_{36}^{92}Kr + 3n + 202.5\ MeV} \\ 
        \mathrm{D+T} &\rightarrow \mathrm{{}_2^4He + n + 17.6\ MeV} 
    \end{align} 
\end{subequations}
The second reaction is already utilized in nuclear fission power plants. While energy production with nuclear fission is less harmful to the environment than fossil fuels, and the total amount of energy available through Uranium or other nuclear fuels far exceeds those of conventional fuels due to the much higher energy density, the disposal of nuclear waste is still an unsolved problem, and disasters like Chernobyl or Fukushima illustrate the hazard potential of such power plants. The precarious situation around nuclear power plants in war-ridden Ukraine raised further doubts regarding the security of nuclear fission in recent months. 
On the other hand, nuclear fusion does not produce nuclear waste radiating for millions of years, and the possibility of severe accidents is not given, as nuclear fusion has to be actively maintained and is not a chain reaction like fission. There are other possible fusion reactions, like the fusion of two deuterium nuclei or one deuterium and one helium-3 nuclei. However, the reaction of deuterium and tritium has the highest cross-section at lower temperatures and is, therefore, the most promising (see Fig. \ref{kap1_img_crosssections}). While deuterium is abundant and occurs naturally as 0.015\% of all hydrogen isotopes, tritium being radioactive with a half-time of roughly 12 years, does not occur naturally and would have to be bred from lithium.
\\
\\
Two different approaches are possible for achieving nuclear fusion: 
\begin{itemize} 
    \item The first is inertial confinement fusion. Here a small solid target containing the deuterium tritium mixture is compressed and heated up by lasers or particle beams on a short time scale (\(\approx 10^{-10}\) s). 
    \item The second approach is magnetic confinement. Here the plasma is confined by a magnetic field through the Lorentz force. One of the most promising configurations for magnetic confinement is the tokamak described in the next section. The target parameters for having a net energy gain of such a device are approximately given by a particle density of \(n\approx 10^{20}\ \mathrm{m}^{-3}\), a temperature of \(T \approx 15\ \mathrm{keV}\), which is equal to 150-200 million Kelvin and a confinement time of 5 seconds or more \cite{scriptPlasma}. 
\end{itemize}
In recent years numerous start-ups researching fusion with the goal of commercializing fusion power plants have been founded in addition to sizeable state-funded research programs. While some of them follow conventional approaches \cite{commonwealthfusion,tokamakenergy}, others explore new means of achieving fusion or even the use of other fusion reactions \cite{generalfusion,helionenergy,trialphaenergy,firstlightfusion,marvelfusion}.

\begin{figure} 
    \centering 
    \includegraphics[width=0.65\textwidth]{img/fusion_cross_section.png} 
    \caption[Fusion reaction cross-sections] {Cross section of different fusion reactions. The deuterium-tritium reaction has a higher cross-section at lower energies. While fusion with \(\mathrm{{}_2^3He}\) would have advantages because no neutrons are produced, it would require higher temperatures. The data was taken from the ENDF-Database \cite{ENDF}.} 
    \label{kap1_img_crosssections} 
\end{figure}

\section{Tokamak}

The term tokamak is a Russian acronym for \textit{"toroidalnaja kamera w magnitnych katuschkach"}, which translates into "toroidal chamber in magnetic coils."To achieve magnetic confinement, the tokamak concept consists of three different magnetic fields that are superposed.
\begin{itemize} 
    \item First toroidal field coils are arranged around the toroidal chamber. These produce a field inside the tokamak that confines the plasma particles to trajectories within the chamber as the Lorentz force makes them spiral with small radii around the field lines compared to the radius of the cross-section of the chamber. Because the coils are arranged circularly around the chamber, they produce a bent magnetic field where the magnetic flux density is larger inside than outside. Therefore, the magnetic field is not homogeneous, and the resulting magnetic field gradient causes a drift of the plasma (see section \ref{chap2_drifts}). 
    \item To prevent this drift, a current is induced into the plasma. This is achieved by coils in the center of the torus that act as primary transformer coils. The magnetic field caused by the current, in addition to the field of the toroidal field coils, results in field lines that screw around the torus. 
    \item Additional vertical coils can be used to modify the plasma's shape or placement in the toroidal chamber. 
\end{itemize}
Fig. \ref{kap1_img_tokamak_fields} shows the arrangement of the field coils for a tokamak.
\begin{figure} 
    \centering 
    \includegraphics[width=0.5\textwidth]{img/tokamak_prinzip.png} 
    \caption[Tokamak schematic] {Schematic drawing of a tokamak. The magnetic field of the toroidal field coils, combined with the field of the plasma current induced by the transformer coils, leads to a field that screws around the torus. \tiny{ (Source: \url{https://www.ipp.mpg.de/14869/tokamak})} } \label{kap1_img_tokamak_fields} 
\end{figure}
To sustain the fusion reaction, the fusion power has to be larger than the power loss \begin{equation} P_{fusion} > P_{conduction\ loss} + P_{radiation\ loss} \end{equation}
The conduction loss is given by heat transfer from the plasma core to the plasma edge. The radiation loss is given through the photons emitted by the plasma. It consists of different components like bremsstrahlung, line radiation, or radiation through recombination. One factor that influences the loss through radiation is impurities in the plasma. These can, for example, be given by atoms from the plasma vessel walls. 
\\
A self-sustaining fusion plasma has yet to be achieved by any experiment based on magnetic confinement as the particle densities and the confinement times are too low on those devices.
Currently, the largest tokamak is the Japanese JT-60SA. With a volume of 130 m³, it has approximately a sixth of the volume of ITER (International Thermonuclear Experimental Reactor) \cite{neilsonBook} currently under construction in France. ITER is supposed to be the first tokamak with a net energy gain in the plasma itself, producing 500 MW while only using 50 MW for heating the plasma \cite{ippJT60SA}. For actual electricity production, the successor of ITER, DEMO (DEMOnstration Power Plant) is planned.
The previously largest tokamak experiment JET (Joint European Torus) in Great Britain already showed that it could produce 65\% of its heating power with fusion. Due to copper coils, the plasma discharges at JET are below one minute. Therefore, both JT-60SA and ITER are equipped with supra-conductive coils enabling much longer discharge times \cite{ippJT60SA}.
Several smaller tokamak experiments worldwide provide the database for constructing ITER and testing individual parts and components to be used in ITER. However, they need to be larger to reach the required parameters for a net energy gain, and most experiments primarily work with model plasmas consisting only of deuterium or helium. 

\section{ASDEX Upgrade}

ASDEX Upgrade (AUG) is a medium-sized tokamak located in Garching, north of Munich. It began operation in 1991 as the successor of ASDEX (Axially Symmetric Divertor Experiment), known for discovering the H-mode \cite{neilsonBook}. 
AUG has a major radius of 1.65 m and a minor radius of 0.5 m giving it a total plasma volume of around 13 m². One specialty of AUG is the reactor wall made of tungsten instead of carbon, like in similar experiments. The advantage of tungsten is its high melting point and the fact that tungsten experiences less corrosion than carbon-based reactor walls, which are often used in other experiments. In total, AUG has around 30 MW of total installed heating power. A brief overview of key diagnostics and installed heating systems at AUG is given in section \ref{chap4_diagnostics} and \ref{chap4_heating_systems}.

\section{The Scope of this Thesis}

For future fusion power plants good plasma confinement and isolation is essential. This requires a solid understanding of the heat and particle transport of the plasma. Gyro-kinetic simulations like GENE \cite{GENEpaper} and GKW \cite{GKWpaper} show good agreement with experiments but require more than 10.000 CPU hours to evaluate a single radial point. Reduced linearized models based on approximations like TGLF (Traped Gyro Landau Fluids) model and QualiKiz for tokamak configurations significantly reduce the computational effort to several CPU seconds. For Applications requiring even faster computation, surrogate models for TGLF and QualiKiz based on neural networks have been developed in recent years \cite{Meneghini2017,Ho2021,Plassche2020}. Advances in the diagnostics of ASDEX Upgrade have made it possible to train a neural network for predicting transport coefficients entirely from observed data instead of relying on simulations with reduced physics models for the generation of the training data. This thesis aims to train a neural network for predicting the diffusion coefficients for electrons and ions \(\chi_e, \chi_i\) with experimental data from ASDEX Upgrade alone. The input parameters for the model are based on \cite{Meneghini2017}. To make comparisons and the evaluation of the model to existing ones easier.

The second chapter of this thesis covers the physical background for understanding heat transport within magnetically confined plasmas, while the third chapter gives a brief introduction to feed-forward neural networks which were used for the machine learning model. The fourth chapter then describes in detail what data is used for training the model and how the database with the training data is created, and how the data is obtained and processed. It also covers the structure and training of the transport neural network. The 5th chapter then discusses the results from the neural network and compares it with physical expectations and parameter dependencies. Lastly, the thesis is summarized, and an outlook for future applications and improvements of this work is given.