#!/usr/bin/env python3
import sys
from pathlib import Path
import shutil
import logging
import subprocess
import aug_sfutils as sf
import numpy as np

path = Path(__file__).parent.absolute()

def get_logger(number: int) -> logging.Logger:
    logger = logging.getLogger("IDErunner{}".format(number))
    logger.setLevel(logging.INFO)
    f_handler = logging.FileHandler(path / "runner{}.log".format(number))
    c_handler = logging.StreamHandler()
    f_handler.setLevel(logging.INFO)
    c_handler.setLevel(logging.DEBUG)

    c_format = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
    f_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    c_handler.setFormatter(c_format)
    f_handler.setFormatter(f_format)

    logger.addHandler(c_handler)
    logger.addHandler(f_handler)
    return logger


class IDE_runner():
    
    def __init__(self, number: int, experiment: str = "KOMO"):
        self.number = number
        self.shots = np.array([])
        self.experiment = experiment
        self.logger = get_logger(number)
        
        
    def set_shots(self, shots: np.ndarray):
        self.shots = shots
        
        
    def start(self):
        if not len(self.shots):
            self.logger.error("no shots list specified")
        else:
            for shot in self.shots:
                self.exec_ide_for_shot(shot)
        self.logger.info("finished with shotlist")
            
    
    def exec_ide_for_shot(self, shotnumber: int, move_shotfile: bool = True):
        try:
            self.update_input_files(shotnumber)
            self.logger.info("Wrote input file for {}".format(shotnumber))
            ide_process = subprocess.Popen(["./exec_IDErunner.sh", 
                                            str(self.number)],
                                            stdout=subprocess.PIPE)
            while ide_process.poll() == None:
                output = str(ide_process.stdout.readline()) # type: ignore
                #print(output)
                if "done!" in output:
                    current = output.split("(", 1)[1].split(")")[0].strip()
                    total = output.split("(of", 1)[1].split(")")[0].strip()
                    print("IDErunner{}: {} of {} timepoints"
                        .format(self.number, current, total[0:4]), end="\r")
                
            if ide_process.poll() != 0:
                self.logger.error("Error while executing IDE for {}" \
                                    .format(shotnumber))
            else:
                try:
                    if move_shotfile:
                        move_to_scratch(shotnumber)
                    self.logger.info("Finished {}".format(shotnumber))
                except:
                    self.logger.error("Could not move {} to Scratch!".format(shotnumber))
        except:
            self.logger.error("Could not write input file for {}; Skipping ...!".format(shotnumber))
            
            
        
    def update_input_files(self, shotnumber: int):
        idf = sf.SFREAD(shotnumber, "IDF") # type: ignore
        old_file_lines = idf.getobject("inp_file")
        old_file_lines = b''.join(old_file_lines).decode("UTF-8").split("\n")
        default_file_path = path / "ide_default.inp"
        default_file = open(default_file_path, "r")
        merged_file_lines = merge_input_files(old_file_lines, default_file, 
                                              shotnumber, self.experiment)
        runner_input_path = path / "IDE_runner{}/ide.inp".format(self.number)
        #runner_input_path = "NoNextcloud/input{}.txt".format(shotnumber)
        input_for_runner = open(runner_input_path, "w")
        input_for_runner.writelines(merged_file_lines)
        input_for_runner.close()
    

def move_to_scratch(shotnumber: int|str):
    base_destination = Path("/toks/scratch/komo/idf_shotfiles/")
    base_source = Path("/afs/ipp/home/k/komo/shotfiles/")
    shotnumber = str(shotnumber)
    folder_number = shotnumber[:2]
    file_number = shotnumber[2:]
    folder_list = ["IDE", "IDF", "IDG", "IDR"]
    
    for folder in folder_list:
        source = base_source / folder / folder_number / (file_number + ".1")
        destination = base_destination / folder / \
                        folder_number / (file_number + ".1")
        shutil.move(source, destination)
        
        
def merge_input_files(old_file_lines, default_file, 
                      shotnumber, experiment="KOMO"):
    
    old_variables = []
    old_indices = []
    default_variables = []
    default_lines = []
    
    new_file_lines = []
    
    fields_to_overwrite = { "flux_loops_unc_scal": 2.0, "bootstrap_mode": 4,
                            "ide_qrz_constraint_ufiles": 
                               'rhopol; 0; "q.36663_rp.u"',
                            "fast_ion_current_source": "RABBIT",
                            "fast_ion_pressure_source": "RABBIT"}
    
    ignore = "varying in time"
    
    def is_line_variable(line):
        if not line.strip() == "" and not ignore in line:
            return not line[0] == "&"
        else:
            return False
    
    for index, line in enumerate(old_file_lines):
        if is_line_variable(line):
            variable = line.split(" = ")
            old_variables.append(variable[0])
            old_indices.append(index)
        
        
    for line in default_file:
        default_lines.append(line)
        if is_line_variable(line):
            variable = line.split(" = ")
            default_variables.append(variable)    
            
            
    for default_line in default_lines:
        
        if is_line_variable(default_line):
            variable = default_line.split(" = ")[0]
            
            if variable == "shotnumbers":
                line = "shotnumbers = {}\n".format(shotnumber)
            elif variable == "experiment_write":
                line = "experiment_write = {}\n".format(experiment)
            elif variable in fields_to_overwrite:
                line = "{} = {}\n".format(variable, 
                    fields_to_overwrite[variable])
            elif variable in old_variables:
                variable_idx = old_variables.index(variable)
                line_idx = old_indices[variable_idx]
                line = old_file_lines[line_idx]
            else:
                line = default_line
            
            new_file_lines.append(line + "\n")
            
        else: 
            new_file_lines.append(default_line)
    
    return new_file_lines


def main(args):
    logging.getLogger('aug_sfutils').setLevel(logging.CRITICAL)
    logging.getLogger('aug_sfutils.sfread').setLevel(logging.CRITICAL)
    shotlist = np.loadtxt(args[1]).astype(int)
    number = int(args[0])
    runner = IDE_runner(number)
    runner.set_shots(shotlist)
    print("Starting IDErunner for Shot {} - {}".format(shotlist[0], shotlist[-1]))
    runner.start()
    
    
if __name__ == "__main__":
    main(sys.argv[1:])