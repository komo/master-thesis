&&&Enabled for IDA_GUI&&&
& The file libe.inp specifies the data and parameter inputs for the Li-beam analysis.
&
& Ampersands & and blank lines are ignored


& shot number
shotnumbers = 37724

& first (tbeg) and last (tend) time [s] (separated by comma) to be read from data base
&!!t_bounds: time_beg_[s], end_[s]
t_bounds = 0.15, 11.0
&t_bounds = 0.15, 1.5
&t_bounds = 3.5, 3.51
 
& time indices to be analyzed and index_step (actual time depends on tbeg, tend, and timstep)
& (0, ?) meeans time points from tbeg
& (?, 0) means time points until tend
time_indices = 0, 0, 1
&time_indices = 1, 1, 1

& parallization mode: 0 .. no 
&                     1 .. initialization 
&                     2 .. run parallel code
&                     3 .. combine results of parallelization
&                     4 .. initialization and run parallel code listen to file parallel_mode.inp
&                     , N_jobs to be run separately  -> qsub -t 1-"N_jobs" ide.sge
parallelization_mode = 0

& set write in shot file (T/F) (file depends on "ida_diagnostics")
write_shot_file_flag = T

& Experiment to be written: RRF, AUGD, AUGE
experiment_write = KOMO

& delta time between two equilibria [s] (minimum 0.0001s?)
timstep = 0.005

& 1/>=2 .. use timstep only (equidistant) / # intervals; (start time, timstep)
time_step_intervals = 1;0., 0.005;2.8, 0.001;6.8, 0.005
&time_step_intervals = 3; 0., 0.005; 2.8, 0.001; 6.8, 0.005 

&?? timsmth_magn: time for smoothing magnetic data [s] (minimum 1d-4 s; or <=1.d-5 for one time point only)
timsmth_magn = 0.0004

& plasma current lower boundaries relative to current maximum: beg, end
plasma_current_lower_bounds_rel = 0.01, 0.01

& verbose
verbose = F

&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   Main 2                        &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& Grid: Rmin, Rmax, Zmin, Zmax (default: 0.80000,  2.3500,  -1.5000, 1.5000)
&!!grid_coord: Rmin, Rmax, Zmin, Zmax
grid_coord = 0.75, 2.67, -1.5040000d+00, 1.504

& grid discretization (Nr_base2,Nz_base2) (default: 6, 7)
&!!grid_discret: Nr_base2, Nz_base2
grid_discret = 6, 7

& stopping criterion for equilibrium iteration (default: N_gs = 60, error =0.0001)
&!!iteration_params: N_gs, error
iteration_params = 150, 0.0005
&iteration_params = 1, 0.0001

& type of (p', FF') basis functions 
& (1:"bernstein", 2:"spline_NumRecip", 3:"fourier_bessel0", 4:"triangles")
&!!type_of_basis_functions: pp, ffp
type_of_basis_functions = 2, 2

& internal spline knot positions: 
& 1st value: psi / rhp .. normalized flux / rhopol (transferred into: psi_norm = rhopol**2)
& 2nd value: N_basis_coeffs == # knot positions as specified (skip knot at psi=rhopol=0)
&ps_spline_knot_pos = rhp, 14, 0.10, 0.20, 0.30, 0.40, 0.50, 0.60, 0.70, 0.80, 0.90, 0.95, 0.98, 1.00, 1.01, 1.02
&fs_spline_knot_pos = rhp, 14, 0.10, 0.20, 0.30, 0.40, 0.50, 0.60, 0.70, 0.80, 0.90, 0.95, 0.98, 1.00, 1.01, 1.02
ps_spline_knot_pos = rhp, 14, 0.12, 0.24, 0.36, 0.48, 0.6, 0.72, 0.81, 0.88, 0.93, 0.96, 0.98, 1.0, 1.01, 1.02
fs_spline_knot_pos = rhp, 14, 0.12, 0.24, 0.36, 0.48, 0.6, 0.72, 0.81, 0.88, 0.93, 0.96, 0.98, 1.0, 1.01, 1.02

& curvature constraint on P' and FF'
& T/F .. use / do not use , 1/2 .. without/with knot distance
& # constraints (should be N_basis_coeffs-1 for P' and FF' each neglecting curvature at spline end points)
& scale and relative uncertainty of curvature constraint at each internal spline knot
curvature_constraint_PsFFs_flag = T, 1
&curvature_constraint_Ps_unc     = 13, 10., 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.5, 0.5, 0.2, 0.1
&curvature_constraint_Fs_unc     = 13, 20., 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.5, 0.5, 0.2, 0.1
curvature_constraint_Ps_unc     = 13, 20.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.5, 0.1
curvature_constraint_Fs_unc     = 13, 20.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.5, 0.1

& amplitude constraint on P' and FF'
& T/F .. use / do not use , default (= 10.0 weak constraint) / selected
& # constraints (should be N_basis_coeffs for P' and FF' each neglecting amplitude at outermost spline points where amplitude=0)
& scale and relative uncertainties of amplitude constraint
amplitude_constraint_PsFFs_flag = T, selected
amplitude_constraint_Ps_unc     = 14, 200.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0
amplitude_constraint_Fs_unc     = 14, 200.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0

& linear regression regularization constant: basis functions, z_shift, ext_curr (default: 0.2)
lin_reg_lambda = 0.0000000d+00, 0.0000000d+00, 0.0000000d+00

& magnetic data: 1/2/3/4/5 = "MAD/MAG" / "MAX" / "MAY"  / "MAP" / "MAY/MAE"  (default=3)
magnetics_data_mode = 3

& coil currents: 1/2/3/4/5/6 = "MBI/MAI"/"MAX"/"MAY"/"MBI/MAP"/"EQUILIBRIUM_CLE"/"EQUILIBRIUM_CLD" (default=3)
coil_current_mode = 3;AUGD, EQH, 0

& method to define Btf: "MBI_BTFABB" (recommended with btf_vac_corr_fact_add = 1.005), MBI_BTF, MAP, MAY, MAX
& correction factor of vacuum toroidal magnetic field
btf_mode = MBI_BTFABB, 1.005
&btf_mode = MBI_BTF, 1.000


& flux loop data to be used
& Dpsi??D      01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 dpsiso1 dpsisu1 dpsia01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17
&Dpsi_used = 35, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T,   T,      T,         F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F
&Dpsi_used = 18, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T,   T,      T
Dpsi_used = 18, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, F, F

&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   Main 3                        &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& Bpol data to be used
& Bthe??       01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 Bpsl02o1 o2 u3 u4 Bdor Bdur Bpslor Bpslur Br1 Br2 Br3 Br4 Br5 Br6 Br7 Br8 Br9 Bhschild Breten-i Broof Breten-a  Bp2i01 02 03 04 05 06 07 08 09 10 11 12 14 15 16 17 18 20 21 22 23 24 25 26 27 28 29 30 31 32
Bpol_used = 61, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, F, T, T, T, T, T, F, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, F, F, F, F, F, F, F, F, F, F, F, F, F, F, T, F, T, T, T, T, F
&Bpol_used = 91, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, F, T, T, T, T, T, F, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T,       F, F, F, F,  F,   F,     F,     F,   F,  F,  F,  F,  F,  F,  T,  F,  F,    T,       T,     T,      F,         T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, F, T, T, T, T, T, T, T, T, T, T, T, T
&Bpol_used = 91, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F,       F, F, F, F,  F,   F,     F,     F,   F,  F,  F,  F,  F,  F,  T,  F,  F,    T,       T,     T,      F,         T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, F, T, T, T, T, T, T, T, T, T, T, T, T

& Bpol array to be used
Bpol_arrays_used = 5; T, F, F, F, F

& Bp standard ring: 157.5°
Bpol_arr1_used = 55;      T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,     T,       F,       F,       F,       F,      F,      F,   F,   F,     F,     F,  T,  F,  T,       T,       T,    T,       F   
Bpol_arr1_name =     Bthe01,Bthe02,Bthe03,Bthe04,Bthe05,Bthe06,Bthe07,Bthe08,Bthe09,Bthe10,Bthe11,Bthe12,Bthe13,Bthe14,Bthe15,Bthe16,Bthe18,Bthe19,Bthe20,Bthe21,Bthe22,Bthe24,Bthe25,Bthe26,Bthe27,Bthe28,Bthe29,Bthe30,Bthe31,Bthe32,Bthe33,Bthe34,Bthe35,Bthe36,Bthe37,Bthe38,Bthe39,Bthe40,Bpsl02o1,Bpsl02o2,Bpsl02u3,Bpsl02u4,Bpslzo1,Bpslzo2,Bdor,Bdur,Bpslor,Bpslur,Br7,Br8,Br9,Bhschild,Breten-i,Broof,Breten-a

& Bp05i: 112.5°
Bpol_arr2_used = 30;       T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T
Bpol_arr2_name =     Bp05i01,Bp05i02,Bp05i03,Bp05i04,Bp05i05,Bp05i06,Bp05i07,Bp05i08,Bp05i09,Bp05i10,Bp05i11,Bp05i12,Bp05i14,Bp05i15,Bp05i16,Bp05i17,Bp05i18,Bp05i20,Bp05i21,Bp05i22,Bp05i23,Bp05i24,Bp05i25,Bp05i26,Bp05i27,Bp05i28,Bp05i29,Bp05i30,Bp05i31,Bp05i32

& Bp09i: -157.5°
Bpol_arr3_used = 30;       T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T
Bpol_arr3_name =     Bp09i01,Bp09i02,Bp09i03,Bp09i04,Bp09i05,Bp09i06,Bp09i07,Bp09i08,Bp09i09,Bp09i10,Bp09i11,Bp09i12,Bp09i14,Bp09i15,Bp09i16,Bp09i17,Bp09i18,Bp09i20,Bp09i21,Bp09i22,Bp09i23,Bp09i24,Bp09i25,Bp09i26,Bp09i27,Bp09i28,Bp09i29,Bp09i30,Bp09i31,Bp09i32

& Bp13i: -67.5°
Bpol_arr4_used = 30;       T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T
Bpol_arr4_name =     Bp13i01,Bp13i02,Bp13i03,Bp13i04,Bp13i05,Bp13i06,Bp13i07,Bp13i08,Bp13i09,Bp13i10,Bp13i11,Bp13i12,Bp13i14,Bp13i15,Bp13i16,Bp13i17,Bp13i18,Bp13i20,Bp13i21,Bp13i22,Bp13i23,Bp13i24,Bp13i25,Bp13i26,Bp13i27,Bp13i28,Bp13i29,Bp13i30,Bp13i31,Bp13i32

& Bp15i: -22.5°
Bpol_arr5_used = 36;       T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T,      T
Bpol_arr5_name =     Bp15i01,Bp15i02,Bp15i03,Bp15i04,Bp15i05,Bp15i06,Bp15i07,Bp15i08,Bp15i09,Bp15i10,Bp15i11,Bp15i12,Bp15i13,Bp15i14,Bp15i15,Bp15i16,Bp15i18,Bp15i19,Bp15i20,Bp15i21,Bp15i22,Bp15i24,Bp15i25,Bp15i26,Bp15i27,Bp15i29,Bp15i31,Bp15i32,Bp15i33,Bp15i34,Bp15i35,Bp15i36,Bp15i37,Bp15i38,Bp15i39,Bp15i40

&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   Main 4                        &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& bpol and flux loop background subtraction: mode (1=const, mean of data(t<=t1), t1, t2
bkg_subtr_mode_bpol  = 1, -1.0000000d+01, -9.0000000d+00, 68.5, 69.0
bkg_subtr_mode_floop = 1, -1.0000000d+01, -9.0000000d+00, 68.5, 69.0

& external conductor groups fitted to magnetic data
&NOTE: OH2o=OH+dOH2s; OH2u=OH+dOH2s+dOH2u; OH=OH3o=OH3u
&number, 1=V1o   2=V1u   3=V2o   4=V2u     5=V3o    6=V3u  7=OH  8=OH2o  9=OH2u  10=OH3o 
&       11=OH3u 12=CoIo 13=CoIu 14=Ipslon 15=Ipslun
ext_cond_groups_fitted = 10, 1, 2, 3, 4, 5, 6, 12, 13, 14, 15
&ext_cond_groups_fitted = 6, 1, 2, 3, 4, 5, 6
&ext_cond_groups_fitted = 4, 12, 13, 14, 15
&ext_cond_groups_fitted = 2, 12, 13


& uncertainty of currents in external poloidal field coils [A]
&                               V1o   V1u   V2o   V2u   V3o   V3u    OH  OH2o  OH2u  OH3o  OH3u  CoIo  CoIu Ipslon Ipslun
ext_cond_groups_curr_unc = 15, 500.0, 500.0, 500.0, 500.0, 500.0, 500.0, 1000.0, 1000.0, 1000.0, 1000.0, 1000.0, 500.0, 500.0, 1000.0, 500.0
&ext_cond_groups_curr_unc = 15, 2.e3, 2.e3, 2.e3, 2.e3, 2.e3, 2.e3, 1.e3, 1.e3, 1.e3, 1.e3, 1.e3, 2.e3, 2.e3, 2.e3, 2.e3
&ext_cond_groups_curr_unc = 15, 5.e1, 5.e1, 5.e1, 5.e1, 5.e1, 5.e1, 1.e3, 1.e3, 1.e3, 1.e3, 1.e3, 5.e2, 5.e2, 1.e3, 5.e2

& scale factor of currents in external poloidal field coils: curr -> curr * scale (default=1.0)
ext_cond_groups_curr_scale = 15, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0
&ext_cond_groups_curr_scale = 15, 0.9942, 1.0026, 0.9920, 0.9897, 1.0025, 0.9906, 1., 1., 1., 1., 1., 0.9922, 1.0128, 1., 1.

& currents in external poloidal field coils outlier threshold for residuum and uncertainty scale: (residuum threshold, scal) (uncertainty = scal * uncertainty
&ext_cond_groups_curr_outlier_thr_unc_scal = 4.0, 2.0
ext_cond_groups_curr_outlier_thr_unc_scal = 10.0, 2.0
&ext_cond_groups_curr_outlier_thr_unc_scal = 2.0, 2.0

& tile current data to be used
&tile_curr_used = 2, 14DUAu, 12DUIu
tile_curr_used = 2, Ipolsola, Ipolsoli

& tile current data relative uncertainty
tile_curr_rel_unc = 0.5
&tile_curr_rel_unc = 1.5d0

& analyse probe calibration shot (Sondeneichschuss)
probe_calibration_shot = F

& discretization of flux derivative in (r, z) direction (to determine Br, Bz) [m] (default: 0.002 each)
flux_deriv_discr = 0.001, 0.001

& 2dim interpolation mode for flux: 1:bicubic / 2:spline_nag / 3:spline_nr / 4:spline_bicubic
flux_2d_interpolation_mode = 1

& mode of correction of magn. probes wrt toroidal magn. field (0 no correction; 1 standard)
tor_field_corr_mode = 1

& time interval for toroidal magnetic field correction (default: 9s - 11s or -10.0, -4.0)
tor_magn_field_corr_time_interval = -6.0000000d+00, -4.0000000d+00

& mode of toroidal magnetic field correction for bpol and floop: "lin_wo_offset", "quadratic"
tor_magn_field_corr_mode = lin_wo_offset, lin_wo_offset

& over-relaxation: (beta-1)*psi_old + beta*psi_new -> psi_new (default = 0.7)
over_relax = 0.3

& mode: (fast_linear_optimizer, MCMC, ferrotiles, equi_given_coefs)
ide_run_mode = fast_linear_optimizer

& uncertainty output using covariance sampling: T/F; #TimeIntervals; time_beg, time_end; ...
ide_covariance_sampling = F; 2; 1.0, 1.1;  1.2, 1.3;

& consider ferrotiles in equilibrium (T/F)
use_ferrotiles = T

& program version
program_version = 1.0


&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   quantities for bpol                        &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& bpol likelihood (1/2 .. Gaussian / Cauchy (outlier);  Cauchy a0)
bpol_probes_likelihood = 1, 0.5

& bpol uncertainty scale: uncertainty = scal * 1 mT (default: 2.0)
&bpol_probes_unc_scal = 1.5
bpol_probes_unc_scal = 1.0

& bpol outlier threshold for residuum and uncertainty scale: (residuum threshold, scal) (uncertainty = scal * uncertainty
bpol_probes_outlier_thr_unc_scal = 4.0, 2.0
&bpol_probes_outlier_thr_unc_scal = 6.0, 2.0

& bpol data: 1=shotfile; 2=read from filename
bpol_probes_data_mode = 1, bpol_probes_mcmc.dat


&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   quantities for flux loops                  &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& flux loops likelihood (1/2 .. Gaussian / Cauchy (outlier);  Cauchy a0)
flux_loops_likelihood = 1, 0.5

& flux loops uncertainty scale: uncertainty = scal * area * 1 mT (default: 2.0)
&flux_loops_unc_scal = 1.5
flux_loops_unc_scal = 1.0

& flux loops threshold for residuum and uncertainty scale: (residuum threshold, scal) (uncertainty = scal * uncertainty
flux_loops_outlier_thr_unc_scal = 4.0, 2.0
&flux_loops_outlier_thr_unc_scal = 6.0, 2.0

& flux loops data: 1=shotfile; 2=read from filename
flux_loops_data_mode = 1, flux_loop_mcmc.dat


&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   quantities for pressure constraint         &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& monotonic pressure (default: T, unc=1.) (-> p'-coefficient positive or negative depending on Ip/Bt direction)
pres_monotonic = T, 0.001

& # pressure constraints  (0 means no constraint)
&N_pres_constr = 0
&N_pres_constr = 15
N_pres_constr = 30

& IDA profiles: (AUGD, AUGE, RRF) (IDA, IDB, YPR)
& exp, diag, edition
&!!ida_use_exp_diag_ed: IDA, exper, diag, ed
ida_use_exp_diag_ed = T, AUGD, IDA, 0

& modify IDA profiles with ASCII file: use, filename
ida_prof_modify = F, te_profs.inp
&ida_prof_modify = T, fakeIDA31163.dat

& TRA data: use, exp (AUGD, TRANSP, RRF, LIVCA, TODSTRCI, ABOCK), diag (TRA), edition (0)
&!!ide_tra_use_exp_diag_ed: use, exper, diag, ed
ide_tra_use_exp_diag_ed = F, RMAXI, TRA, 0

& pressure likelihood (1/2 .. Gaussian / Cauchy (outlier);  Cauchy a0)
pres_likelihood = 1, 0.5

& pressure constraint psi_norm begin and end (0.01/0.4 with/wo TRA, 1.00)
&pres_constr_psi_interval = 0.2, 0.98
pres_constr_psi_interval = 0.01, 0.98

& shift [m] of pressure profile: R -> R + shift (shift > 0 .. outward shift)
pres_prof_shift = -0.0000000d+00

& pressure uncertainty: mode (1=rel+abs(Pa)), values (default: 0.20, 1.d3)
&!!pres_uncertainty: mode, rel, abs [Pa]
pres_uncertainty = 1, 0.5, 1000.0

& pressure data: 1=shotfile; 2=read from filename
pres_data_mode = 1, pres_mcmc.dat

& pressure constraint mode: "flux" (default) at given psi / "r_outer_mid" at given outer midplane major radius 
&pres_constr_mode = r_outer_mid
pres_constr_mode = flux


&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   quantities for ion profiles                &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& pressure fast ion redistribution: 
& 1. flag: (T, F)
& 2. mode: "profile_full", "profile_within_q1"
& 3. slowing_down_time: default=0.05 [s]
& 4. rhotor_max:  default=0.6  (only for "profile_within_q1")
& 5. loss_factor: default=0.5  (only for "profile_full")
pres_fast_ion_redistrib = F, profile_within_q1, 0.05, 0.6, 0.5

& ion temperature profiles: (1 default)
& 1 : (a) ohmic, (b) IDI shotfile if time within time_dist_max, (c) Ti=Te
& 2 : (a) ohmic, (b) CXRS (old version, obsolete, replaced by IDI), (c) Ti=Te
& 3 : (a) ohmic, (b) Ti=Te
& 4 : read from filename
& 5 : ohmic (from collisionality)
& 6 : Ti = Te
ion_temperature_mode = 1, "/afs/ipp/u/rrf/I33311.NPA"

& IDI profiles: (AUGD, AUGE, RRF) (IDI) (used if ion_temperature_mode=1 and idi_use_exp_diag_ed=T)
& exp, diag, edition
&!!idi_use_exp_diag_ed: IDI, exper, diag, ed, time_dist_max[s]
idi_use_exp_diag_ed = T, AUGD, IDI, 0, 0.2


&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   quantities for MSE                         &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& use MSE data
mse_use_data = F

& MSE data source: ("AUG_shotfile", "filename", "EQH_forward_calc"), filename (filename is used only if "filename" is set)
mse_data_source = AUG_shotfile, mse_mcmc.dat

& MSE data AUG shotfile: (AUGD, ABOCK) (MSA)
&!!mse_exp_diag_ed: exper, diag, ed
mse_exp_diag_ed = REM, MSA, 0

& MSE angle factor (channels indicated will be SCALED BEFORE offset subtraction) default 1.0)
&      number of MSE channels to be scaled; tuple(ch,factor); ...
mse_angle_factor = 0;1, 1.15;2, 1.15;3, 1.15;4, 1.15;5, 1.15;6, 1.15;7, 1.15;8, 1.15;9, 1.15;10, 1.15;11, 1.15

& MSE angle offset (channels indicated will be SUBTRACTED from measured data)
&      number of MSE channels to be offsetted; tuple(ch,offset); ...
mse_angle_offset = 0;1, 0.18;2, 0.50;3, 179.60;4, -0.06;5, -0.15;6, -0.20;7, 179.44;8, -0.49;9, -0.84;

&      offset from file (none or filename)
mse_angle_file_offset = none
&mse_angle_file_offset = IDF_mse_off.33391_new

& MSE channels not used: number of MSE channels not used; channels (separated by commas)
mse_channels_not_used = 0;1, 4, 11

& MSE channels not used: R_min, R_max, z_min, z_max
mse_channels_Rz_minmax = 1.0, 2.5, -0.9, 0.9

& MSE model: "all" 10 (or 9) coefficients / "wo_Erz" / "wo_Erz_v_tor" / "wo_Erz_diamag"
mse_model = all

& MSE likelihood (1/2 .. Gaussian / Cauchy (outlier);  Cauchy a0)
mse_likelihood = 1, 0.5

& MSE channels with uncertainty scale: uncertainty = scal * uncertainty
&mse_ch_unc_scal = 0;  1, 100.;  
mse_ch_unc_scal = 9;1, 1.0;2, 1.0;3, 1.0;4, 1.0;5, 1.0;6, 1.0;7, 1000.0;8, 1.0;9, 1000.0;

& MSE all channels uncertainty scale: uncertainty = scal * uncertainty
&mse_unc_scal = 2.0
mse_unc_scal = 2.0000000d+07

& MSE time average [t-dt/2,t+dt/2] [s]
mse_time_average = 0.01

& MSE v_beam scale to consider the beam mix [0.5,1.0] (default=1.d0)
mse_vbeam_scal = 1.0

& MSE data unit: tangens, radians, degree
mse_data_unit = degree


&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   quantities for iMSE                        &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& use iMSE data
ims_use_data = F

& iMSE data source: ("AUG_shotfile", "filename", "EQH_forward_calc"), filename (filename is used only if "filename" is set)
ims_data_source = AUG_shotfile, ims_mcmc.dat

& iMSE data AUG shotfile: (AUGD, ABOCK) (IMA)
&!!imse_exp_diag_ed: exper, diag, ed
ims_exp_diag_ed = IMSD, IMA, 0

& iMSE angle factor (channels indicated will be SCALED BEFORE offset subtraction) default 1.0)
&      number of iMSE channels to be scaled; tuple(ch,factor); ...
ims_angle_factor = 0;1, 1.15;2, 1.15;3, 1.15;4, 1.15;5, 1.15;6, 1.15;7, 1.15;8, 1.15;9, 1.15;10, 1.15;11, 1.15

& iMSE angle offset (channels indicated will be SUBTRACTED from measured data)
&      number of iMSE channels to be offsetted; tuple(ch,offset); ...
ims_angle_offset = 0; 1, 0.18;

&      offset from file (none or filename)
ims_angle_file_offset = none
&ims_angle_file_offset = /afs/ipp/u/rrf/F90/AUG_equil/Data/IMSE/IDF_imse_off.38418_3

&      offset from shotfile (T or F)
ims_angle_shotfile_offset = F

& iMSE channels not used: number of iMSE channels not used; channels (separated by commas)
ims_channels_not_used = 0; 2, 3

& iMSE channels not used: R_min, R_max, z_min, z_max
ims_channels_Rz_minmax = 1.0, 2.95, -1.0, 0.9
&ims_channels_Rz_minmax = 1.66, 1.95, 0.14, 0.9

& iMSE model: "all" 10 (or 9) coefficients / "wo_Erz" / "wo_Erz_v_tor" / "wo_Erz_diamag"
ims_model = all

& iMSE likelihood (1/2 .. Gaussian / Cauchy (outlier);  Cauchy a0)
ims_likelihood = 1, 0.5

& iMSE channels with uncertainty scale: uncertainty = scal * uncertainty
ims_ch_unc_scal = 0;7, 20000000.;9, 20000000.;

& iMSE all channels uncertainty scale: uncertainty = scal * uncertainty
&ims_unc_scal = 4.0
&ims_unc_scal = 10.0
ims_unc_scal = 2.0000000d+07

& iMSE data not fitted for beams: number of beams not fitted; beam number(s)
ims_beam_not_fitted = 0; 7

& iMSE time average [t-dt/2,t+dt/2] [s]
ims_time_average = 0.02

& iMSE v_beam scale to consider the beam mix [0.5,1.0] (default=1.d0)
ims_vbeam_scal = 1.0

& iMSE data unit: tangens, radians, degree
ims_data_unit = degree


&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   quantities for BEP                         &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& use BEP data
bep_use_data = F

& BEP data source: ("AUG_shotfile", "filename", "EQH_forward_calc"), filename (filename is used only if "filename" is set)
bep_data_source = AUG_shotfile, bep_mcmc.dat

& BEP data AUG shotfile: (AUGD, RLD) (BEP)
&!!bep_exp_diag_ed: exper, diag, ed
bep_exp_diag_ed = RLD, BPZ, 0

& BEP angle offset (channels indicated will be SUBTRACTED from measured data) [rad]
&      number of BEP channels to be offsetted; tuple(ch,offset); ...
bep_angle_offset = 0;1, -0.20;

& BEP channels not used: number of BEP channels not used; channels (separated by commas)
bep_channels_not_used = 0;2, 3

& BEP likelihood (1/2 .. Gaussian / Cauchy (outlier);  Cauchy a0)
bep_likelihood = 1, 0.5

& BEP channels with uncertainty scale: uncertainty = scal * uncertainty
bep_ch_unc_scal = 0;7, 20000000.;

& BEP all channels uncertainty scale: uncertainty = scal * uncertainty
&bep_unc_scal = 1.0
bep_unc_scal = 1000.0

& BEP data unit: tangens, radians, degree
bep_data_unit = radians


&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   quantities for iso-flux constraints        &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& # iso-flux constraints  (0 means no constraint) : pair of coordinates with the same flux value psi(R1,z1)=psi(R2,z2)
& relative Te step, iso-flux uncertainty
N_iso_flux_constraints = 0, 0.02, 0.0000000d+00

& evaluate iso-flux data (automatically set to true if N_iso_flux_constraints > 0)
eval_iso_flux_data = F


&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   q constraints                              &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& q(0) constraint: flag; mode: 0/1/2/3 .. constraint / only lower / only upper / both boundaries; 
& time_beg, time_end;
& constraint value (sign!), uncertainty (mode = 0);
& lower boundary value (sign!) and uncertainty; upper boundary (sign!) value and uncertainty
ide_q0_constraint_flag = F; 2; 0.0, 12.0; -1.0, 0.2; -0.6, 0.3; -9.0, 1.0

& q(R,z) constraints: number; value (sign!), uncertainty, R[m], z[m] (for ide_qrz_constraint_data_source="ide_inp")
& z not used so far. Evaluated at the moment at (r,Zmag)
& number of q constraints followed by data blocks separated by ";" 
ide_qrz_constraints = 0;-1.5, 0.02, 1.9, 0.1
&ide_qrz_constraints = 2; -1.5, 0.02, 1.9, 0.1; -1.5, 0.02, 1.9, 0.1;

& q(R,z) constraint ufiles: coordinate ("rhopol" or "cylindrical"); number of files; filenames in "" separated by commas
ide_qrz_constraint_ufiles = rhopol; 0; "q.36663_rp.u"



&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   quantities for faraday rotation            &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& use faraday data, # LOS, N_pos
&!! faraday_use_data: POL, los, npos
faraday_use_data = T, 5, 200

& fit faraday data: # LOS to be fitted; LOS indices to be fitted separated by , (comma)
faraday_fit_data = 2; 1, 2

& faraday data source: ("AUG_shotfile", "filename", "ufile"), filename [degree!] (filename is used only if "filename" is set)
faraday_data_source = AUG_shotfile, 0, "faraday_angle.degree"
&faraday_data_source = ufile, 2, "/afs/ipp/u/rrf/P33313.H1", "/afs/ipp/u/rrf/P33313.H2"

& faraday data AUG shotfile: (AUGD, AMLYNEK, COOD) (POL)
&!!faraday_exp_diag_ed: exper, diag, ed
faraday_exp_diag_ed = AUGD, POL, 0

& faraday data offset (will be SUBTRACTED from measured data of shotfile only!)
&            number of channels with offset; index of faraday channel, offset [degree]
&faraday_angle_offset = 1; 2, 2.3
faraday_angle_offset = 0;2, 0.0000000d+00

& faraday likelihood (1/2 .. Gaussian / Cauchy (outlier);  Cauchy a0)
faraday_likelihood = 1, 0.5

& faraday uncertainty scale: uncertainty = scal * uncertainty (default = 0.2 degree)
&            number of channels scaled; index of faraday channel, scale factor
&faraday_unc_scal = 2; 1, 2.0; 2, 2.0
faraday_unc_scal = 2; 1, 4.0; 2, 8.0
&faraday_unc_scal = 2; 1, 20000.0; 2, 20000.0

& faraday toroidal field correction
&            number of channels with correction; index of faraday channel, scale factor of Btor
&faraday_btor_corr = 2; 1, -0.0055; 2, -0.0075
&faraday_btor_corr = 2; 1, -0.0075; 2, -0.0100
faraday_btor_corr = 2;1, -0.007;2, -7.0000000d-03

& faraday z shift correction to be added (!) to z-coord from DCNgeo
&            number of channels with correction; index of faraday channel, z shift [m]
faraday_z_corr = 2;1, -0.010;2, -1.5000000d-02


&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   quantities for Uloop                       &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& Uloop data, # Uloop measurements; signal names (ULi22p, ULi25p, ULid12f ist nicht angeschlossen, ULi27)
uloop_data_fitted = 3;ULi22p, ULi25p, ULid12f, ULi27

& Uloop prior (0 means no constraint): number of constraints, coordinates (R, z), uncertainty
& N; R[m], z[m], Ulopp uncertainty [V]
uloop_prior = 0;1.70, 0.05, 0.5;1.65, 0.05, 0.5;1.75, 0.05, 0.5;1.70, 0.0000000d+00, 0.5;1.70, 0.1, 0.5


&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   quantities for  IDE results                &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& normalization of beta_pol and li
ide_bpol_li_norm_mode = 6

& # flux labels in IDE shotfile (default=150; increased e.g. 1000)
ide_N_flux_labels = 150


&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   quantities for  MCMC                       &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& sample P'  coefficients
mcmc_use_coef_ps = 10, T, T, T, T, T, T, T, T, T, T

& sample FF' coefficients
mcmc_use_coef_fs = 10, T, T, T, T, T, T, T, T, T, T


&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   quantities for  ferromagnetic elements     &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& mu_eff (Eurofer: 1.4 - 4.0)
ide_ferro_mu_eff = 1.8


&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   quantities for magnetic perturbation coils &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& use magnetic perturbation coils currents
ide_mpcoil_use = F


&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   additional poloidal field coils           &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& number of additional poloidal field coils followed by data blocks separated by ";" 
& with radial and vertical position [m], N_windings, and minor radius of winding [m], current [A]
ide_N_add_pol_field_coils_5tuple = 0;


&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   quantities for ECE                         &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& ECE data: use, exp (AUGD,ECED), diag (CEC), edition (0)
&!!ece_use_exp_diag_ed: use, exper, diag, ed
ece_use_exp_diag_ed = F, ECED, CEC, 0

& equilibrium used for ECE data: exp (AUGD), diag (EQH), edition (0)
&!!ece_eq_exp_diag_ed: exper, diag, ed
ece_eq_exp_diag_ed = AUGD, EQH, 0
&ece_eq_exp_diag_ed = RRF, IDE, 0

& ECE mask: # masked channels, channel number(s) (60 channels -> dat_unc = 1.d+30)
ece_mask = 0, 48

& correction factor of toroidal magnetic field used only for ECE data (addl. to standard ECE corr. factor)
btf_corr_fact_ece = 1.0
&btf_corr_fact_ece = 0.995


&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   quantities for toroidal flow               &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& use toroidal flow in Grad-Shafranov equilibrium
tor_flow_use = F


&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   quantities for poloidal flux diffusion     &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& use poloidal flux diffusion constraint in Grad-Shafranov equilibrium
pol_flux_diffusion_use = T

& # poloidal flux diffusion constraints in Grad-Shafranov equilibrium
pol_flux_diffusion_N_constr = 100

& lower and upper rhopol value for poloidal flux diffusion constraints in Grad-Shafranov equilibrium
pol_flux_diffusion_rho_pol_lo_up = 0.0000000d+00, 1.0

& uncertainties of current density constraint in Grad-Shafranov equilibrium: 
& relative and absolute (default: 0.10, 2.d+5 [A/m^2])
pol_flux_diffusion_unc = 0.1, 2.0000000d+05
&pol_flux_diffusion_unc = 0.05, 1.0000000d+05

& ECCD mode: (NONE, TRANSP, TORBEAM, read_from_file)
eccd_mode = TORBEAM

& equilibrium used for TORBEAM: exp (CONSISTT, AUGD), diag (EQH), edition (0)
eccd_eq_exp_diag_ed = CONSISTT, EQH, 0
&eccd_eq_exp_diag_ed = AUGD, EQH, 0

& modify ECRH settings with ASCII file: use, filename
ecrh_set_modify = F, ECRH_aux_in.dat
&ecrh_set_modify = T, ECRH_aux_in31163.dat

& ECCD smoothing via diffusion: diffusion time [s], diffusion coefficient [m^2/s] (default: 1.d-3, 0.5)
eccd_smooth_diffusion = 0.001, 0.2

& mode of bootstrap current calculation: 0 none / 1 Sauter / 2 Sauter-Hager / 3 TRANSP / 4 Sauter-Redl
bootstrap_mode = 1

& solver of poloidal flux diffusion equation (crank_nicholson or finite_element_method)
pol_flux_diffusion_solver = crank_nicholson

& parameters for the current diffusion equation
&                            dt, radius_minor_eff_sep_scale, # rho values
pol_flux_diffusion_params = 0.0005, 0.995, 100

& parameters for the current diffusion toroidal current initialization:
&   1 / 2 .. previous / flat (default)
pol_flux_diffusion_jtor_init = 2

&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   quantities for fast ion contributions      &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& source of fast ion current: (NONE, TRA, QFI_Weiland, RABBIT)
fast_ion_current_source = RABBIT

& source of fast ion current: (NONE, TRA, QFI_Weiland, RABBIT)
fast_ion_pressure_source = RABBIT

& initialize time traces for QFI_Weiland evaluation (external code)
qfi_weiland_init = F

&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   quantities for sawtooth correction         &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& sawtooth reconnection mode (Kadomtsev_full, within_q1_full)
sawtooth_reconnection_mode = within_q1_full

& sawtooth time points from shotfile: use, experiment, STS shotfile, edition, mode (1 identified: ST_TYPE=1 (default) / 2 all time points found)
&!!sawtooth_corr_use_sts_sf: STS, exper, diag, ed, mode
sawtooth_corr_use_sts_sf = T, AUGD, STS, 0, 1

& sawtooth correction: use, filename
sawtooth_corr_use_file = F, /afs/ipp/u/rrf/F90/AUG_equil/Data/Sawtooth/STS.38418.corr_full

& sawtooth correction using shear on q=1 surface: use, threshold (0.2-0.4)
sawtooth_corr_use_shear = F, 0.5

&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   quantities for CXRS                        &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& CEZ data: use, exp (AUGD), diag (CEZ), edition (0)
&!!cez_use_exp_diag_ed: CEZ, exper, diag, ed
cez_use_exp_diag_ed = T, AUGD, CEZ, 0

& CEZ mask: # masked channels, channel number(s) (channels -> dat_unc = 1.d+30)
cez_mask = 0, 48

& CEZ time averages [t-dt, t+dt]: Ti, v_angular [s]
cez_time_averages = 0.02, 0.02


&&&&&!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
&   quantities for COZ                         &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& COZ data: use, exp (AUGD), diag (COZ), edition (0)
&!!coz_use_exp_diag_ed: COZ, exper, diag, ed
coz_use_exp_diag_ed = T, AUGD, COZ, 0

& COZ mask: # masked channels, channel number(s) (channels -> dat_unc = 1.d+30)
coz_mask = 0, 48

& COZ time averages: Ti, v_angular [s]
coz_time_averages = 0.01, 0.01


&&&&&!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
&   quantities for CUZ                         &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& CUZ data: use, exp (AUGD), diag (CUZ), edition (0)
&!!cuz_use_exp_diag_ed: CUZ, exper, diag, ed
cuz_use_exp_diag_ed = T, AUGD, CUZ, 0

& CUZ mask: # masked channels, channel number(s) (channels -> dat_unc = 1.d+30)
cuz_mask = 0, 48

& CUZ time averages: Ti, v_angular [s]
cuz_time_averages = 0.01, 0.01


&&&&&!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
&   quantities for CMZ                         &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& CMZ data: use, exp (AUGD), diag (CMZ), edition (0)
&!!cmz_use_exp_diag_ed: CMZ, exper, diag, ed
cmz_use_exp_diag_ed = T, AUGD, CMZ, 0

& CMZ mask: # masked channels, channel number(s) (channels -> dat_unc = 1.d+30)
cmz_mask = 0, 48

& CMZ time averages: Ti, v_angular [s]
cmz_time_averages = 0.005, 0.005


&&&&&!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
&   quantities for CPZ                         &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& CPZ data: use, exp (AUGD), diag (CPZ), edition (0)
&!!cpz_use_exp_diag_ed: CPZ, exper, diag, ed
cpz_use_exp_diag_ed = T, AUGD, CPZ, 0

& CPZ mask: # masked channels, channel number(s) (channels -> dat_unc = 1.d+30)
cpz_mask = 0, 48

& CPZ time averages: Ti, v_angular [s]
cpz_time_averages = 0.005, 0.005


&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   quantities for diamagnetic loop            &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& fit diamagnetic loop data: 
diamagn_loop_fit_data = T

& MDI data: use, exp (AUGD), diag (MDI), edition (0)
&!!mdi_use_exp_diag_ed: use, exper, diag, ed
mdi_use_exp_diag_ed = F, AUGD, MDI, 0

& uncertainties of diamagnetic loop constraint in Grad-Shafranov equilibrium: 
& relative and absolute (default: 0.02, 4.d-4 [Wb=Vs])
&diamagn_loop_diffusion_unc = 0.02, 4.d-4
diamagn_loop_diffusion_unc = 1000.02, 0.04


&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   quantities for Zeff                        &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& mass [u] of main ion species (1/2/3 for H/D/T, 4 for Helium)
mass_main_ions = 2

& charge of main ion species (1 for H/D/T, 2 for Helium)
Z_main_ions = 1

& Zeff dimension 0 (constant in time and space)
                 1 (varying in time; constant in space)
                 2 (varying in time and space)
zeff_dim = 1

& Zeff shotfile: use, exp, diag (IDZ), edition (0)
&!!zeff_sf_use_exp_diag_ed: ZEF, exper, diag, ed
zeff_sf_use_exp_diag_ed = T, AUGD, IDZ, 0

& Zeff ufilename
zeff_ufilename = F, Z33134.ZEFF1

& Zeff and Zimp to be used for ni density: ni = ne * (1.d0 - (Zeff - 1.d0)/Zimp)
Zeff_const = 1.5
Zimp       = 5

& mass [u] of main impurity (10.8/12/16/14 .. B/C/O/N)
mass_main_impurity = 10.81


&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&   miscellaneous setting                     &
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

& write poloidal field on arbitrary grid: r_grid_arb.dat, z_grid_arb.dat, psi_grid_arb.dat
&                       flag,  Nr,  ra,  rb,  Nz,   za,  zb
ide_pol_flux_on_arb_grid = F, 350, 0.1, 3.6, 500, -2.5000000d+00, 2.5

