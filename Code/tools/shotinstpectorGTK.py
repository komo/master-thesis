#!/usr/bin/env python3

from matplotlib.backends.backend_gtk3 import (
    NavigationToolbar2GTK3 as NavigationToolbar)
from matplotlib.backends.backend_gtk3agg import (
    FigureCanvasGTK3Agg as FigureCanvas)

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from matplotlib.backend_bases import MouseButton
import matplotlib.gridspec as gridspec
from scipy.interpolate import interp1d

import sys
from pathlib import Path
sys.path.append(str(Path(__file__).parents[1].absolute()))
import gi
gi.require_version('Gtk', '3.0')
#gi.require_version('Adw', '1')
from gi.repository import Gtk, Gio

from src.db_handler import DB_handler
from src.data_handlers.bolometer_handler import get_filtered_bolometer_data
from constants import *

class MainWindow(Gtk.Window):
    def __init__(self):
        super().__init__()
        
        self.db = None
        self.field1 = None
        self.field2 = None
        self.log_scale = False
        
        self.vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        self.hbox.set_spacing(20)
        self.hbox.set_margin_start(10)
        self.hbox.set_margin_end(10)
        self.hbox.set_margin_bottom(10)
        self.vbox.add(self.hbox)
        self.hbox.set_halign(Gtk.Align.CENTER)
        
        self.shot_entry = Gtk.Entry()
        self.shot_entry.set_placeholder_text("Enter Shotnumber")
        self.shot_entry.connect("activate", self.on_shot_entry)
        self.hbox.add(self.shot_entry)
        
        self.render_button = Gtk.Button(label="Render")
        self.hbox.add(self.render_button)
        
        self.set_border_width(10)
        self.set_default_size(400, 200)

        self.header = Gtk.HeaderBar()
        self.header.set_show_close_button(True)
        self.header.props.title = "ShotInstpector"
        self.set_titlebar(self.header)
        self.set_titlebar(self.header)
        

        self.fig = Figure(figsize=(5, 4), dpi=100)
        grid = gridspec.GridSpec(2,3)
        self.ax_profile = self.fig.add_subplot(grid[0,1:])
        self.ax_emission = self.fig.add_subplot(grid[:,0])
        self.ax_meas = self.fig.add_subplot(grid[1,1:])

        self.canvas = FigureCanvas(self.fig)  # a Gtk.DrawingArea
        self.canvas.set_hexpand(True)
        self.canvas.set_vexpand(True)
        self.vbox.add(self.canvas)
        
        toolbar = NavigationToolbar(self.canvas, self)
        self.vbox.add(toolbar)
        
        self.status_label = Gtk.Label()
        self.update_status("")
        self.vbox.add(self.status_label)
        
        self.add(self.vbox)
        self.canvas.mpl_connect("button_press_event", self.on_axis_click)
        
        
    def on_shot_entry(self, entry):
        # text = entry.get_text()
        # shotnumber = int(text)
        # self.update_status("Loading Bolo data for {}".format(shotnumber))
        # self.load_data(shotnumber)
        # self.update_status("Bolo data for {} loaded".format(shotnumber))
        try:
            text = entry.get_text()
            shotnumber = int(text)
            self.update_status("Loading Bolo data for {}".format(shotnumber))
            self.load_data(shotnumber)
            self.update_status("Bolo data for {} loaded".format(shotnumber))
        except:
            self.show_invalid_input_dialog()
            
        
    def load_data(self, shotnumber, fps=100):
        timebase, bolodata = get_filtered_bolometer_data(shotnumber)
        self.timebase = np.arange(timebase.min(), timebase.max(), 1/fps)
        self.bolodata = np.zeros((self.timebase.size, bolodata.shape[1]))
        print(timebase.min(), self.timebase.min())
        for channel in range(bolodata.shape[1]):
            f = interp1d(timebase, bolodata[:, channel])
            self.bolodata[:, channel] = f(self.timebase)
        self.ax_meas.plot(self.timebase, self.bolodata)
        
        
    def update_status(self, status_str):
        self.status_label.set_text("Status: {}".format(status_str))
        
        
    def update_plot(self):
        #self.ax.clear()
        scale = "log" if self.log_scale else "linear"
        if not self.db == None:
            
            if self.field1 == self.field2:
                self.db[self.field1].plot.hist(ax=self.ax, bins=30, logy=self.log_scale)
                #self.ax.set_xlabel(self.field1)
                #self.ax.scatter(x, y)
            else:
                self.db.data.plot.scatter(x=self.field1, y=self.field2, 
                                          ax=self.ax, logy=self.log_scale,
                                          logx=self.log_scale)
            
            self.canvas.draw()
                
    def show_invalid_input_dialog(self):
        self.invalid_file_dialog = Gtk.MessageDialog(
            transient_for=self,
            message_type=Gtk.MessageType.ERROR,
            buttons=Gtk.ButtonsType.OK,
            text="Please enter a valid shotnumber"
        )
        self.invalid_file_dialog.set_modal(True)
        self.invalid_file_dialog.show()
        self.invalid_file_dialog.connect(
            "response", 
            lambda dialog, _: dialog.destroy()
        )


    def on_axis_click(self, event):
        if event.button is MouseButton.LEFT:
            print("bla")
            #if event.inaxes == self.ax_meas:
            #    print(event.x, event.y)


win = MainWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()