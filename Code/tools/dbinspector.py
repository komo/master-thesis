from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas,\
                NavigationToolbar2WxAgg as NavigationToolbar
from matplotlib.figure import Figure
from matplotlib.patches import Rectangle
from matplotlib.widgets import LassoSelector
from matplotlib.path import Path as MPLpath
from matplotlib.colors import SymLogNorm
import numpy as np
import pandas as pd
import wx

import os
import sys
from pathlib import Path
parent_dir = Path(__file__).parents[1].absolute()
sys.path.append(str(parent_dir))
from src.constants import *


INFO_STRING = "Database explorer, no database opened yet"
BOLD = "\033[1m"
END = "\033[0;0m"
WARNING = '\033[93m'

def clear_print():
    os.system("clear")
    print(INFO_STRING)

def print_selected_data(selected_data: pd.DataFrame,
                        x_field: str,
                        y_field: str,
                        plot_type: str,
                        precursor_str: str):
    
    outlier_data = selected_data[selected_data[IS_OUTLIER] == True]
    has_outliers = not outlier_data.empty
    
    clear_print()
    print()
    print(precursor_str)
    
    if has_outliers:
        print("{}Selected data has outliers!".format(BOLD))
        print(pd.unique(outlier_data[OUTLIER_FIELDS]), END)
    print()
    
    if plot_type == "hist":
        print(selected_data[[SHOT, TIME, RHO, x_field, OUTLIER_FIELDS]])
    elif plot_type == "hist2d":
        print(selected_data[[SHOT, TIME, RHO, x_field, y_field, 
                             OUTLIER_FIELDS]])
    elif plot_type == "scatter":
        print(selected_data[[SHOT, TIME, RHO, x_field, y_field, 
                             OUTLIER_FIELDS]])
        

class SelectFromCollection:

    def __init__(self, ax, collection, db):
        self.canvas = ax.figure.canvas
        self.collection = collection
        self.ax = ax

        self.xys = collection.get_offsets()
        self.Npts = len(self.xys)
        self.db = db

        # Ensure that we have separate colors for each object
        self.fc = collection.get_facecolors()
        if len(self.fc) == 0:
            raise ValueError('Collection must have a facecolor')
        elif len(self.fc) == 1:
            self.fc = np.tile(self.fc, (self.Npts, 1))
        
        # for older python/matplotlib version rename props to lineprops
        self.lasso = LassoSelector(ax, onselect=self.onselect, 
                                   props = {"color": "tab:green"})
        self.ind = []
        

    def onselect(self, verts):
        path = MPLpath(verts)
        self.ind = np.nonzero(path.contains_points(self.xys))[0]
        self.canvas.draw()
        self.get_selected_datapoints()
        
        
    def get_selected_datapoints(self):
        x_field = self.ax.get_xlabel()
        y_field = self.ax.get_ylabel()

        selected_data = pd.DataFrame()
        size = self.xys[self.ind].shape[0]
        if size < 1000:
            for point in self.xys[self.ind]:
                selected_point = self.db.loc[
                    (self.db[x_field] == point[0]) & 
                    (self.db[y_field] == point[1])
                ]
                selected_data = pd.concat(
                    [selected_data, selected_point.drop_duplicates()], 
                    ignore_index=True
                )
            
            selected_data = selected_data.sort_values([SHOT, TIME, RHO])
            print_selected_data(selected_data, x_field, y_field, "scatter",
                                "Selected datapoints:")
        else:
            print("Too many possible datapoints ({}) max 1000".format(size))
        

    def disconnect(self):
        self.lasso.disconnect_events()
        self.canvas.draw_idle()


class CanvasFrame(wx.Frame):
    def __init__(self, parent, title):
        super().__init__(parent, -1, title, size=(1000, 600))

        self.db = pd.DataFrame()
        self.open_database(parent_dir / "data/db6june.feather")

        self.figure = Figure()
        
        self.ax = self.figure.add_subplot()
        self.pts = None
        self.selector = None
        self.previous_rect = None
        self.previous_patch = None
        self.hist2d_data = None

        self.canvas = FigureCanvas(self, -1, self.figure)
        self.canvas.mpl_connect("pick_event", self.on_pick)
        self.canvas.mpl_connect("key_press_event", self.end_selection)
        self.canvas.mpl_connect("button_press_event", self.on_click)

        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.add_toolbar_top_options()
        self.add_toolbar_top_selector()
        self.sizer.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        self.add_toolbar_bottom()
        self.add_toolbar_matplotlib()
        
        self.setup_axis_choosers()

        self.SetSizer(self.sizer)
        self.Fit()
        self.update_plot()
        
    
    def add_toolbar_top_selector(self):
        self.selector_bar = wx.Panel(self)
        self.selector_bar_sizer = wx.BoxSizer(wx.HORIZONTAL)
        
        self.xlabel = wx.StaticText(self.selector_bar, label = "x-axis:", 
                                    style = wx.ALIGN_CENTRE)
        self.xaxis_chooser = wx.Choice(self.selector_bar)
        self.ylabel = wx.StaticText(self.selector_bar, label = "y-axis:", 
                                    style = wx.ALIGN_CENTRE)
        self.yaxis_chooser = wx.Choice(self.selector_bar)
        
        self.selector_bar_sizer.AddStretchSpacer()
        self.selector_bar_sizer.Add(self.xlabel, 0, wx.ALL|wx.ALIGN_CENTER, 5)
        self.selector_bar_sizer.Add(self.xaxis_chooser, 0, 
                                  wx.ALL|wx.ALIGN_CENTER, 5)
        self.selector_bar_sizer.AddSpacer(16)
        self.selector_bar_sizer.Add(self.ylabel, 0, wx.ALL|wx.ALIGN_CENTER, 5)
        self.selector_bar_sizer.Add(self.yaxis_chooser, 0, 
                                  wx.ALL|wx.ALIGN_CENTER, 5)
        self.selector_bar_sizer.AddStretchSpacer()
        
        self.selector_bar.SetSizer(self.selector_bar_sizer)
        self.sizer.Add(self.selector_bar, 0, wx.ALL|wx.EXPAND)
    
    
    def add_toolbar_top_options(self):
        self.button_bar = wx.Panel(self)
        self.button_bar_sizer = wx.BoxSizer(wx.HORIZONTAL)

        open_button = wx.Button(self.button_bar, label="Open")
        self.Bind(wx.EVT_BUTTON, self.open_file_dialog, open_button)
        
        label = wx.StaticText(self.button_bar, label = "Plotstyle:", 
                                    style = wx.ALIGN_CENTRE)
        self.plot_chooser = wx.Choice(self.button_bar)
        self.plot_chooser.Append("Histogram")
        self.plot_chooser.Append("ScatterPlot")
        self.plot_chooser.Append("2D-Histogram")
        self.plot_chooser.SetSelection(0)
        self.Bind(wx.EVT_CHOICE, self.on_plot_style_toggle, self.plot_chooser)
        
        self.log_toggle_y = wx.CheckBox(self.button_bar, label="Log Y")
        self.Bind(wx.EVT_CHECKBOX, self.on_log_toggle, self.log_toggle_y)
        
        self.log_toggle_x = wx.CheckBox(self.button_bar, label="Log X")
        self.Bind(wx.EVT_CHECKBOX, self.on_log_toggle, self.log_toggle_x)
        
        self.outlier_toggle = wx.CheckBox(self.button_bar, 
                                          label="Outliers")
        self.Bind(wx.EVT_CHECKBOX, self.on_outlier_toggle, self.outlier_toggle)
        self.outlier_toggle.SetValue(True)
        
        self.select_button = wx.Button(self.button_bar, label="Select")
        self.Bind(wx.EVT_BUTTON, self.on_select_points, self.select_button)
        
        self.button_bar_sizer.Add(open_button, 0, wx.ALL, 5)
        self.button_bar_sizer.AddStretchSpacer()
        self.button_bar_sizer.Add(label, 0, wx.ALL|wx.ALIGN_CENTER, 5)
        self.button_bar_sizer.Add(self.plot_chooser, 0, 
                                  wx.ALL|wx.ALIGN_CENTER, 0)
        self.button_bar_sizer.AddSpacer(16)
        self.button_bar_sizer.Add(self.log_toggle_y, 0, 
                                  wx.ALL|wx.ALIGN_CENTER, 5)
        self.button_bar_sizer.AddSpacer(8)
        self.button_bar_sizer.Add(self.log_toggle_x, 0, 
                                  wx.ALL|wx.ALIGN_CENTER, 5)
        self.button_bar_sizer.AddSpacer(8)
        self.button_bar_sizer.Add(self.outlier_toggle, 0, 
                                  wx.ALL|wx.ALIGN_CENTER, 5)
        self.button_bar_sizer.AddStretchSpacer()
        self.button_bar_sizer.Add(self.select_button, 0, 
                                  wx.ALL|wx.ALIGN_CENTER, 5)
        self.select_button.Disable()
        
        self.button_bar.SetSizer(self.button_bar_sizer)
        self.sizer.Add(self.button_bar, 0, wx.ALL|wx.EXPAND)


    def add_toolbar_bottom(self):
        
        self.slider_bar = wx.Panel(self)
        self.slider_bar_sizer = wx.BoxSizer(wx.HORIZONTAL)

        self.hist_slider = wx.Slider(self.slider_bar, 0, 30, 10, 100)
        self.Bind(wx.EVT_SCROLL, self.on_slider_scroll, self.hist_slider)
        
        self.slider_label = wx.StaticText(self.slider_bar, label = "{} Bins"
                              .format(self.hist_slider.GetValue()), 
                                style = wx.ALIGN_CENTRE)
        
        self.slider_bar_sizer.AddSpacer(8)
        self.slider_bar_sizer.Add(self.slider_label, 0, wx.ALL|wx.ALIGN_CENTER)
        self.slider_bar_sizer.Add(self.hist_slider, wx.ALL|wx.EXPAND)
        
        self.slider_bar.SetSizer(self.slider_bar_sizer)
        self.sizer.Add(self.slider_bar, 0, wx.ALL|wx.EXPAND)
        

    def add_toolbar_matplotlib(self):
        """Copied verbatim from embedding_wx2.py"""
        self.toolbar = NavigationToolbar(self.canvas)
        self.toolbar.Realize()
        # By adding toolbar in sizer, we are able to put it at the bottom
        # of the frame - so appearance is closer to GTK version.
        self.sizer.Add(self.toolbar, 0, wx.LEFT | wx.EXPAND)
        # update the axes menu on the toolbar
        self.toolbar.update()
        
        
    def setup_axis_choosers(self):
        for field in DB_COLUMNS:
            self.xaxis_chooser.Append(field)
            self.yaxis_chooser.Append(field)
            
        self.Bind(wx.EVT_CHOICE, self.on_field_changed, self.xaxis_chooser)
        self.Bind(wx.EVT_CHOICE, self.on_field_changed, self.yaxis_chooser)
        self.xaxis_chooser.SetSelection(0)
        self.yaxis_chooser.SetSelection(0)
        self.yaxis_chooser.Disable()
        
    
    def on_slider_scroll(self, event):
        self.slider_label.SetLabel("{} Bins"
                                   .format(self.hist_slider.GetValue()))
        self.update_plot()
    
    
    def on_select_points(self, event):
        self.selector = SelectFromCollection(self.ax, self.pts, self.db)
    
    
    def end_selection(self, event):
        if event.key == "enter":
            if self.selector != None:
                self.selector.disconnect()
                self.selector = None
                self.canvas.draw()
    
    
    def on_plot_style_toggle(self, event):
        if self.plot_chooser.GetSelection() != 1:
            if self.plot_chooser.GetSelection() == 0:
                self.yaxis_chooser.Disable()
                self.log_toggle_y.SetLabel("Log Y")
                self.log_toggle_x.Enable()
            else:
                self.log_toggle_y.SetLabel("Log")
                self.yaxis_chooser.Enable()
                self.log_toggle_x.SetValue(False)
                self.log_toggle_x.Disable()
            self.select_button.Disable()
            self.slider_bar.Show()
            self.sizer.Layout()
            if self.selector != None:
                self.selector.disconnect()
                self.selector = None
                self.canvas.draw()
        else:
            self.log_toggle_x.Enable()
            self.log_toggle_y.SetLabel("Log Y")
            self.yaxis_chooser.Enable()
            self.select_button.Enable()
            self.slider_bar.Hide()
            self.sizer.Layout()
        self.update_plot()
        
        
    def on_log_toggle(self, event):
        self.update_plot()
        
    def on_outlier_toggle(self, event):
        self.update_plot()
        
    def on_field_changed(self, event):
        self.update_plot()
        
        
    def on_pick(self, event):
        if isinstance(event.artist, Rectangle):
            rectangle = event.artist
            x, y = rectangle.xy
            w = rectangle.get_width()
            if self.previous_rect != None:
                self.previous_rect.set_color("tab:blue")
            rectangle.set_color("tab:red")
            self.previous_rect = rectangle
            x_field, y_field = self.get_selected_fields()
            
            precursor_str = "Datapoints with {}{}{} between {}{}{} - {}{}{}:" \
                                .format(BOLD, x_field, END, 
                                        BOLD, round(x,3), END,
                                        BOLD, round(x+w,3), END)
            
            show_outliers = self.outlier_toggle.GetValue()
            is_outlier = self.db[IS_OUTLIER]==True
            df = self.db if show_outliers else self.db[~is_outlier]
            
            selected_df = df[
                (self.db[x_field] >= x) & (self.db[x_field] <= x + w)
            ]
            print(selected_df[[SHOT, TIME, RHO, x_field]])
            print_selected_data(selected_df, x_field, y_field, "hist", 
                                precursor_str)
            self.canvas.draw()

    
    def on_click(self, event):
        is_hist2d = self.plot_chooser.GetSelection() == 2
        is_main_axis = event.inaxes == self.ax
        if is_hist2d and is_main_axis:
            x_field, y_field = self.get_selected_fields()
            x, y = event.xdata, event.ydata
            x_edges = self.hist2d_data[1]
            y_edges = self.hist2d_data[2]
            
            x_min = x_edges[x_edges < x][-1]
            x_max = x_edges[x_edges > x][0]
            y_min = y_edges[y_edges < y][-1]
            y_max = y_edges[y_edges > y][0]
            
            selection = (
                (self.db[x_field] >= x_min) & (self.db[x_field] <= x_max) &
                (self.db[y_field] >= y_min) & (self.db[y_field] <= y_max)
            )
            
            str1 = "Datapoints with {}{}{} between {}{}{} - {}{}{} and \n" \
                    .format(BOLD, x_field, END, 
                                    BOLD, round(x_min, 3), END, 
                                    BOLD, round(x_max, 3), END)
            str2 = "Datapoints with {}{}{} between {}{}{} - {}{}{}:" \
                            .format(BOLD, y_field, END, 
                                    BOLD, round(y_min, 3), END, 
                                    BOLD, round(y_max, 3), END)
            
            
            print_selected_data(self.db[selection], x_field, y_field, "hist2d",
                                str1 + str2)
            
            if self.previous_patch != None:
                self.previous_patch.remove()
            
            selection_marker = Rectangle((x_min, y_min), x_max - x_min, 
                                         y_max - y_min, color="tab:red")
            self.previous_patch = self.ax.add_patch(selection_marker)
            self.canvas.draw()
        
        
    def update_plot(self):
        clear_print()
        self.figure.clear()
        self.ax = self.figure.add_subplot()
        
        if self.plot_chooser.GetSelection() == 0:
            self.histogram()
        elif self.plot_chooser.GetSelection() == 1:
            self.scatter_plot()
        else:
            self.histogram2d()
            
        self.canvas.draw()
        
        
    def histogram(self):
        log_scale_y = self.log_toggle_y.GetValue()
        log_scale_x = self.log_toggle_x.GetValue()
        show_outliers = self.outlier_toggle.GetValue()
        x_field, _ = self.get_selected_fields()
        n_bins = self.hist_slider.GetValue()
        
        df = self.check_for_inf_and_nan(x_field)
        
        if log_scale_x:
            self.ax.set_xscale("log")
            n_bins = 10**np.linspace(np.log10(df[x_field].min()), 
                                    np.log10(df[x_field].max()), 
                                    n_bins)

        if show_outliers:
            (_, _, patches) = self.ax.hist(df[x_field], 
                                            bins=n_bins, log=log_scale_y)
        else:
            is_outlier = self.db[IS_OUTLIER]==True
            (_, _, patches) = self.ax.hist(df[~is_outlier][x_field], 
                                            bins=n_bins, log=log_scale_y)
        self.ax.set_xlabel(x_field)
        for patch in patches:
            patch.set_picker(True)
            
            
    def histogram2d(self):
        log_scale_y = self.log_toggle_y.GetValue()
        log_scale_x = self.log_toggle_x.GetValue()
        show_outliers = self.outlier_toggle.GetValue()
        x_field, y_field = self.get_selected_fields()
        n_bins = self.hist_slider.GetValue()
        
        df = self.check_for_inf_and_nan(x_field, y_field)

        is_outlier = df[IS_OUTLIER]==True
        if show_outliers:
            x_bins = np.linspace(df[x_field].min(), df[x_field].max(), n_bins)
            y_bins = np.linspace(df[y_field].min(), df[y_field].max(), n_bins)
            self.hist2d_data = np.histogram2d(df[x_field], df[y_field], 
                                              bins=(x_bins, y_bins))
        else:
            x_bins = np.linspace(df[~is_outlier][x_field].min(), 
                                df[~is_outlier][x_field].max(), n_bins)
            y_bins = np.linspace(df[~is_outlier][y_field].min(), 
                                df[~is_outlier][y_field].max(), n_bins)
            self.hist2d_data = np.histogram2d(df[x_field][~is_outlier], 
                                              df[y_field][~is_outlier], 
                                              bins=(x_bins, y_bins))
        
        vmax = self.hist2d_data[0].max()
        
        lognorm = SymLogNorm(linthresh=1, linscale=1,
                             vmin=0, vmax=vmax, base=10)
        norm = lognorm if log_scale_x or log_scale_y else "linear"
        if show_outliers:
            _,_,_, im = self.ax.hist2d(df[x_field], df[y_field], 
                                        bins=(x_bins, y_bins), norm=norm)
        else:
            _,_,_, im = self.ax.hist2d(df[~is_outlier][x_field],
                                        df[~is_outlier][y_field],
                                        bins=(x_bins, y_bins), norm=norm)
        
        self.colorbar = self.figure.colorbar(im, ax=self.ax)
        self.ax.set_xlabel(x_field)      
        self.ax.set_ylabel(y_field)

    
    def scatter_plot(self):
        log_scale_y = self.log_toggle_y.GetValue()
        log_scale_x = self.log_toggle_x.GetValue()
        show_outliers = self.outlier_toggle.GetValue()
        x_field, y_field = self.get_selected_fields()
        if log_scale_y:
                self.ax.set_yscale("log")
        if log_scale_x:
            self.ax.set_xscale("log")
            
        is_outlier = self.db[IS_OUTLIER]==True
        is_direct_outlier = (
            self.db[OUTLIER_FIELDS].str.contains(x_field) | 
            self.db[OUTLIER_FIELDS].str.contains(y_field)
        )
        if show_outliers:
            self.pts = self.ax.scatter(self.db[x_field], 
                                        self.db[y_field], 
                                        c="tab:blue")
            self.ax.scatter(self.db[is_outlier][x_field], 
                            self.db[is_outlier][y_field], 
                            c="tab:orange")
            self.ax.scatter(self.db[is_direct_outlier][x_field], 
                            self.db[is_direct_outlier][y_field], 
                            c="tab:red")
        else:
            self.pts = self.ax.scatter(self.db[~is_outlier][x_field], 
                                        self.db[~is_outlier][y_field], 
                                        c="tab:blue")
        self.ax.set_xlabel(x_field)
        self.ax.set_ylabel(y_field)
        
            
    
    def open_file_dialog(self, event):
        openFileDialog = wx.FileDialog(self, "Open", "", "", "", 
                                        wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        openFileDialog.ShowModal()
        filepath = openFileDialog.GetPath()
        self.open_database(filepath)
        openFileDialog.Destroy()
        
        
    def get_selected_fields(self):
        x_field = DB_COLUMNS[self.xaxis_chooser.GetSelection()]
        y_field = DB_COLUMNS[self.yaxis_chooser.GetSelection()]
        return x_field, y_field
        
        
    def open_database(self, filepath):
        try:
            self.db = pd.read_feather(filepath)
            filepath = str(filepath)
            db_name = filepath.split("/")[-1]
            self.SetTitle("DatabaseInspector - {}".format(db_name))
            global INFO_STRING
            INFO_STRING = "Current database: {} with size {}" \
                            .format(db_name, self.db.shape)
            print(INFO_STRING)
        except:
            wx.MessageBox('Error loading database', 'Error', 
                          wx.OK | wx.ICON_ERROR)
            
    
    def check_for_inf_and_nan(self, x_field, y_field=None):
        
        if y_field == None:
            x_inf = self.db[x_field].isin([np.inf, -np.inf])
            x_nan = self.db[x_field].isna()
            if not self.db[x_inf].empty:
                print(WARNING + 
                  f"There are inf values in selected fields {x_field}")
                print(self.db[x_inf][[SHOT, TIME, RHO, x_field]], END)
            if not self.db[x_nan].empty:
                print(WARNING + 
                  f"There are nan values in selected fields {x_field}")
                print(self.db[x_nan][[SHOT, TIME, RHO, x_field]], END)
        else:
            x_inf = (self.db[x_field].isin([np.inf, -np.inf]) |
                     self.db[y_field].isin([np.inf, -np.inf]))
            x_nan = (self.db[x_field].isna() | self.db[y_field].isna())
            if not self.db[x_inf].empty:
                print(WARNING + 
                      "There are inf values in selected fields {}, {}"
                        .format(x_field, y_field))
                print(self.db[x_inf][[SHOT, TIME, RHO, x_field, y_field]], END)
            if not self.db[x_nan].empty:
                print(WARNING + 
                      "There are inf values in selected fields {}, {}"
                        .format(x_field, y_field))
                print(self.db[x_nan][[SHOT, TIME, RHO, x_field, y_field]], END)
        return self.db[~(x_inf | x_nan)]
        

class MyApp(wx.App):
    def OnInit(self):
        frame = CanvasFrame(None, "DatabaseInspector")
        self.SetTopWindow(frame)
        frame.Show(True)
        return True


if __name__ == "__main__":
    app = MyApp()
    app.MainLoop()