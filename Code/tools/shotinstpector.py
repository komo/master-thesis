#!/usr/bin/env python3

import numpy as np

from matplotlib.backends.qt_compat import QtWidgets
from matplotlib.backends.backend_qt5agg import (
    FigureCanvas, NavigationToolbar2QT as NavigationToolbar)
from matplotlib.figure import Figure
import matplotlib.gridspec as gridspec
from matplotlib.backend_bases import MouseButton
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.interpolate import interp1d


import sys
from pathlib import Path
sys.path.append(str(Path(__file__).parents[1].absolute()))

from src.data_handlers.bolometer_handler import get_filtered_bolometer_data, get_x_point_pos
from constants import *
from GPT.gp_inference import calculate_timeseries, calculate_timeseries_fast
from GPT.plot_helper import plot_emission
from GPT.profiles import get_profile_time_series
from src.GPT.static_coords import r_coord, z_coord

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

small_mask = np.loadtxt("src/GPT/data_for_GPT/mask.txt")

class ApplicationWindow(QtWidgets.QMainWindow):
    def __init__(self, parent = None):
        super().__init__()
        
        self.timebase = np.array([])
        self.bolodata = np.array([])
        self.emiss_frames = np.array([])
        self.status = "Status:"
        
        self._main = QtWidgets.QWidget()
        self.setCentralWidget(self._main)
        self.setWindowTitle("ShotInstpector")
        layout = QVBoxLayout(self._main)
        
        self.fig = Figure(figsize=(5, 4), dpi=100)
        grid = gridspec.GridSpec(2,3)
        self.ax_profile = self.fig.add_subplot(grid[0,1:])
        self.ax_emission = self.fig.add_subplot(grid[:,0])
        self.ax_meas = self.fig.add_subplot(grid[1,1:])
        
        self.canvas = FigureCanvas(self.fig)
        self.nav_bar = NavigationToolbar(self.canvas, self)
        layout.addWidget(self.canvas, stretch=10)
        layout.addWidget(self.nav_bar)
        
        self.fig.tight_layout()
        
        self.shot_entry = QLineEdit()
        self.shot_entry.setValidator(QIntValidator())
        self.shot_entry.setMaxLength(5)
        self.shot_entry.editingFinished.connect(self.on_shot_entry)
        self.shot_entry.setPlaceholderText("Enter Shotnumber...")
        self.nav_bar.addWidget(self.shot_entry)
        
        self.render_button = QPushButton("Render")
        self.render_button.clicked.connect(self.render)
        self.nav_bar.addWidget(self.render_button)
        
        self.fast_mode_switch = QCheckBox("Fast Mode")
        self.nav_bar.addWidget(self.fast_mode_switch)
        
        self.status_label = QLabel(self.status)
        layout.addWidget(self.status_label)
        
        self.timeindicator = self.ax_meas.axvline(0, color="black")
        
        self.canvas.mpl_connect("button_press_event", self.on_axis_click)
        
        
    def on_shot_entry(self):
        #try:
        text = self.shot_entry.text()
        self.setWindowTitle("ShotInstpector - {}".format(text))
        shotnumber = int(text)
    #self.update_status("Loading Bolo data for {}".format(shotnumber))
        self.load_data(shotnumber)
        #self.update_status("Bolo data for {} loaded".format(shotnumber))
        #except:
        #    QMessageBox.about(self, "Invalid Shotnumber", 
        #
    
    def load_data(self, shotnumber, fps=50):
        self.ax_meas.clear()
        self.shotnumber = shotnumber
        #self.x_point_up = get_x_point_pos(shotnumber)
        timebase, bolodata = get_filtered_bolometer_data(shotnumber)
        timebase = timebase[:-5000]
        bolodata = bolodata[:-5000, ...]
        self.timebase = np.arange(timebase.min(), 
                                  timebase.max(), 1/fps)
        self.bolodata = np.zeros((self.timebase.size, bolodata.shape[1]))
        for channel in range(bolodata.shape[1]):
            f = interp1d(timebase, bolodata[:, channel])
            self.bolodata[:, channel] = f(self.timebase)
        self.ax_profile.clear()
        self.ax_meas.plot(self.timebase, self.bolodata/1000)
        
        self.canvas.draw()
        
        
    def render(self):
        if self.bolodata.any() and self.timebase.any():
            self.render_thread = Render_Thread(self.bolodata, 
                                               self.fast_mode_switch.isChecked())
            self.render_thread.update_signal.connect(self.set_status)
            self.render_thread.finished.connect(self.render_done)
            self.render_thread.start()
               
        else:
            self.on_shot_entry()
            
    def render_done(self):
        self.emiss_frames = self.render_thread.emiss_frames
        self.set_status("Rendering of shot {} done".format(self.shotnumber))
        self.im = self.ax_emission.imshow(self.emiss_frames[0]/1e6, 
                                          interpolation="spline36",
                                            extent=[r_coord[0], r_coord[-1], 
                                                    z_coord[0], z_coord[-1]])
        div = make_axes_locatable(self.ax_emission)
        cax = div.append_axes('right', '5%', '5%')
        self.fig.colorbar(self.im, cax=cax, label="Emission in MW/m³")
        self.ax_emission.set_title("time = {} s".format(round(self.timebase[0], 2)))
        self.ax_emission.set_xlabel("r in m")
        self.ax_emission.set_ylabel("z in m")
        self.ax_emission.contour(r_coord, z_coord, small_mask[::-1], 
                        levels=[0.5], colors="white")
        self.canvas.draw()
            
    def on_axis_click(self, event):
        if event.button is MouseButton.LEFT:
            if event.inaxes == self.ax_meas:
                x, y = event.xdata, event.ydata
                self.ax_meas.set_title("time = {} s".format(round(x, 2)))
                self.timeindicator.remove()
                self.timeindicator = self.ax_meas.axvline(x, color="black")
                self.canvas.draw()
                if self.timebase.any():
                    nearest_t_idx = np.searchsorted(self.timebase, x, "left")
                    self.update_emiss_plot(nearest_t_idx)
                
    
    def update_emiss_plot(self, time_idx):
        if self.emiss_frames.any():
            emiss = self.emiss_frames[time_idx]/1e6
            self.im.set_data(emiss)
            self.im.set_clim(0, emiss.max())
            self.ax_emission.set_title("time = {} s".format(round(self.timebase[time_idx], 2)))
        self.canvas.draw()
               
                
    def set_status(self, status):
        self.status = "Status: {}".format(status)
        self.status_label.setText(self.status)
        

class Render_Thread(QThread):
    update_signal = pyqtSignal(str)
    
    def __init__(self, boloframes, fastmode_enabled):
        QThread.__init__(self)
        self.boloframes = boloframes
        self.fastmode_enabled = fastmode_enabled
        self.emiss_frames = np.array([])
        #self.x_point_up
        
    
    def run(self):
        if self.fastmode_enabled:
            self.emiss_frames = calculate_timeseries_fast(self.boloframes, 
                                                         PARAMS_STATIC_CART,
                                                         True)
        else:
            self.emiss_frames = calculate_timeseries(self.boloframes,
                                                    PARAMS_STATIC_CART,
                                                    status_fcn=self.update_status)
            
    def update_status(self, status):
        self.update_signal.emit(status)
            

app = QApplication(sys.argv)
ex = ApplicationWindow()
ex.show()
sys.exit(app.exec_())