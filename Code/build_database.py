#!/usr/bin/env python3
import numpy as np
import sys
from pathlib import Path
import logging

from src.db_handler import DB_handler
from src.get_shot_data import get_data_for_shot
from src.data_handlers.shot_validation import check_IDF, check_IDR

from src.constants import *

import warnings
warnings.filterwarnings("ignore")


path = Path(__file__).parent.absolute()

def get_logger() -> logging.Logger:
    logger = logging.getLogger("database_builder")
    logger.setLevel(logging.INFO)
    f_handler = logging.FileHandler(path / "build_database.log")
    c_handler = logging.StreamHandler()
    f_handler.setLevel(logging.INFO)
    c_handler.setLevel(logging.DEBUG)

    c_format = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
    f_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    c_handler.setFormatter(c_format)
    f_handler.setFormatter(f_format)

    logger.addHandler(c_handler)
    logger.addHandler(f_handler)
    return logger

logger = get_logger()


def pre_load_validation(shot: int) -> bool:
    IDF_status = check_IDF(shot)
    IDR_status = check_IDR(shot)
    if not IDF_status:
        logger.error("No valid IDF Shotfile for {}, skipping...".format(shot))
    elif not IDR_status:
        logger.error("No valid IDR Shotfile for {}, skipping...".format(shot))
    return IDF_status and IDR_status


def build_database(db_path: str, shotlist: np.ndarray):
    db = DB_handler(db_path=db_path)
        
    
    for shot in shotlist:
        logger.info("Starting with shot {}".format(shot))
        if pre_load_validation(shot):
            try:
            # fix very strange bug by wrapping shot in int()
                shot_data = get_data_for_shot(int(shot), fast_GPT=True)
                db.update_shot_data(shot_data)
                db.save()
                logger.info("Appended {} to database".format(shot))
            except:
                logger.error("Some error occured while getting data for {}"
                             .format(shot))
            

def main(args):
    logging.getLogger('aug_sfutils').setLevel(logging.ERROR)
    shotlist = np.loadtxt(args[1]).astype(int)
    db_path = path / Path(args[0])
    build_database(db_path, shotlist)
    
    
    
if __name__ == "__main__":
    main(sys.argv[1:])
