"""Implementation of the database class"""
import sys
from pathlib import Path
sys.path.append(str(Path(__file__).parent.absolute()))
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split

from constants import *
from check_outliers import perform_checks

class DB_handler:
    
    def __init__(self, db_path: str = None, 
                 filetype: str = "feather",
                 legacy_mode: bool = False):
        
        columns = DB_COLUMNS_LEGACY if legacy_mode else DB_COLUMNS
        
        self.filetype = filetype
        if db_path is not None:
            self.filepath = Path(db_path)
        else:
            self.filepath = Path("data/database")
        
        if (self.filepath).is_file():
            self.data = self.load_from_file()
        else:
            self.data = pd.DataFrame(columns=columns)
            
            
    def __getitem__(self, key):
        return self.data[key]
    
    
    def __setitem__(self, key, value):
        self.data[key] = value


    def load_from_file(self, filepath: str = None, 
                       filetype:str = None) -> pd.DataFrame:
        if filepath is None:
            assert self.filepath is not None, "No file path specified"
            filepath = self.filepath
        if filetype is None:
            filetype = self.filetype
        filepath = filepath
        if filetype == "feather":
            return pd.read_feather(filepath)
        elif filetype == "csv":
            return pd.read_csv(filepath)
        else:
            raise ValueError("Wrong filetype. Only Feather and CSV supported!")


    def save(self, filepath: str = None, filetype: str = None):
        if filepath is None:
            assert self.filepath is not None, "No file path specified"
            filepath = self.filepath
        if filetype is None:
            filetype = self.filetype
        filepath = filepath
        if filetype == "feather":
            self.data.to_feather(filepath)
        elif filetype == "csv":
            self.data.to_csv(filepath)
        else:
            raise ValueError("Wrong filetype. Only Feather and CSV supported!")
        
            
    def empty(self) -> bool:
        return self.data.empty
    
    
    def valid(self) -> pd.DataFrame:
        is_valid = self[IS_OUTLIER] == False
        return self[is_valid]
    
    
    def invalid(self) -> pd.DataFrame:
        is_invalid = self[IS_OUTLIER] == True
        return self[is_invalid]


    def get_inputs(self) -> pd.DataFrame:
        inputs = self.valid()[MODEL_INPUTS]
        inputs[Q_SAFETY] = inputs[Q_SAFETY].abs()
        inputs[Q_SAFETY_SHEAR] = inputs[Q_SAFETY_SHEAR].abs()
        return inputs


    def get_profiles_for_shot_time(self, shotnumber: int, 
                                   time: float) -> pd.DataFrame:
        return self[(self[SHOT] == shotnumber) & np.isclose(self[TIME], time)]
            
        
    def find_model_input(self, model_input: np.ndarray, 
                         only_meta:bool = True) -> pd.DataFrame:
        # exclude Q_SA and Q_SA_SHEAR because absolute value is used as input
        fields_to_check = MODEL_INPUTS[0:6] + MODEL_INPUTS[8:-1]
        row_data_to_look_for = np.r_[model_input[0:6], model_input[8:-1]]
        mask = np.isclose(self[fields_to_check].values, row_data_to_look_for)
        selected_data = self.data.loc[mask.all(axis=1)]
        return selected_data[[SHOT, TIME, RHO]] if only_meta else selected_data
    

    def update_shot_data(self, updated_shot_data: pd.DataFrame):
        shots_to_update = updated_shot_data[SHOT].unique()
        self.data = self.data[self.data[SHOT].isin(shots_to_update) == False]
        self.data = pd.concat([self.data, updated_shot_data], 
                              ignore_index=  True)
        self.data = self.data.sort_values([SHOT, TIME, RHO]) \
                        .reset_index(drop=True)
    
    
    def self_check(self, save: bool = False, reset: bool = False):
        if reset:
            self[IS_OUTLIER] = False
            self[OUTLIER_FIELDS] = ""    
        perform_checks(self)
        if save: 
            self.save()
             
    
    def get_norm_parameters(self, norm_type: str = NORM_ZSCORE) -> dict:

        if norm_type == NORM_MINMAX:
            inputs = self.get_inputs()
            minimum = inputs.min().to_numpy(np.float32)
            maximum = inputs.max().to_numpy(np.float32)
            norm_dict = {
                NORM_TYPE: NORM_MINMAX, 
                NORM_MINMAX_MIN: minimum,
                NORM_MINMAX_MAX: maximum
            }
        elif norm_type == NORM_ZSCORE:
            inputs = self.get_inputs()
            sigma = inputs.mean().to_numpy(np.float32)
            mu = inputs.std().to_numpy(np.float32)
            norm_dict = {
                NORM_TYPE: NORM_ZSCORE, 
                NORM_ZSCORE_SIGMA: sigma,
                NORM_ZSCORE_MU: mu
            }
        return norm_dict
                                              
                        
    def train_test_split(self, test_size: float = 0.2, 
                         only_electrons: bool = False, random_state: int = 42):
        assert not self.empty(), "Database is empty!"
        
        x = self.get_inputs()
        if only_electrons:
            y = self.valid()[CHI_ELECTRON]
        else:
            y = self.valid()[[CHI_ELECTRON, CHI_ION]]
        
        x_train, x_test, y_train, y_test = train_test_split(x, y, 
                                                            test_size=
                                                            test_size,
                                                            random_state=
                                                            random_state)
        return x_train, x_test, y_train, y_test
    
    
    def augment_data(self):
        augmented_data = self.data.copy()
        augmented_data[TE_SCALE] = 0
        #augmented_data[TI_SCALE] = 0
        augmented_data[CHI_ELECTRON] = 0
        #augmented_data[CHI_ION] = 0
        self.data = pd.concat([self.data, augmented_data], ignore_index=True)
        
        