from os import minor, remove
from random import triangular
import sys
from pathlib import Path
sys.path.append(str(Path(__file__).parents[1].absolute()))
sys.path.append(str(Path(__file__).parent.absolute()))
import aug_sfutils as sf
from GPT.static_coords import *
from idf_handler import IDF_handler
from gp_profile_fit import fit_profile
import numpy as np
from scipy.interpolate import interp1d
import warnings


class MEQ_handler():
    
    def __init__(self, shotnumber: int, idf_handler: IDF_handler):
        self.eq = sf.EQU(shotnumber)
        self.shotnumber = shotnumber
        self.field_at_rho = np.array([])
        
        _, self.R = self.get_radius(idf_handler.timebase, 
                                    idf_handler.rhobase_r)
        self.timebase = idf_handler.timebase
        self.rhobase = idf_handler.rhobase_r
        self.rhobase_gp_fit = idf_handler.rhobase_gp_fit
        self.interpol_profile = idf_handler.interpol_profile
        
        self.r = idf_handler.r
        self.r_gp_fit = idf_handler.rhobase_gp_fit
        self.a = self.get_lcfs_radius(self.timebase)
    

    def get_radius(self, time, rho):
        flux_surf_radii = np.zeros((len(time), rho.shape[1]))
        major_radii = np.zeros((len(time), rho.shape[1]))

        for i,t in enumerate(time):
            rho_at_t = rho[i, :]
            r, z = sf.rho2rz(self.eq, rho_in=rho_at_t, t_in=t, 
                             coord_in='rho_pol')
            for j, _ in enumerate(rho_at_t):
                if r[0][j].size == 0:
                    flux_surf_radii[i, j] = np.nan
                    major_radii[i, j] = np.nan
                else:
                    R_center = (r[0][j].min() + r[0][j].max())/2
                    #R_center = r[0][j].mean()
                    #z_center = (z[0][j].min() + z[0][j].max())/2#z[0][j].mean()
                    #r_mean = np.sqrt((r[0][j] - R_center)**2 +
                    #                (z[0][j] - z_center)**2).mean()
                    flux_surf_radii[i, j] = (r[0][j].max() - r[0][j].min())/2
                    major_radii[i, j] = R_center
                
        major_radii = self.remove_zero_radii(major_radii, rho)
        return flux_surf_radii, major_radii, 


    def remove_zero_radii(self, major_radii: np.ndarray, 
                          rhobasis: np.ndarray) -> np.ndarray:
        new_major_radii = np.zeros(major_radii.shape)
        for i, (radii, basis) in enumerate(zip(major_radii, rhobasis)):
            mask = radii != 0
            new_radii = radii[mask]
            new_basis = basis[mask]
            f = interp1d(new_basis, new_radii)
            new_major_radii[i, :] = f(basis)
        return new_major_radii
            

    def interpol_profile(self, profiles: np.ndarray, rho_basis: np.ndarray, 
                         rho_intervals: np.ndarray) -> np.ndarray:
        assert profiles.shape == rho_basis.shape, \
                "profile and basis have different size"
        interpol_profiles = np.zeros(rho_intervals.shape)
        for i, (p, r, intv) in enumerate(zip(profiles, rho_basis, 
                                             rho_intervals)):
            f = interp1d(r, p)
            interpol_profiles[i, :] = f(intv)
        return interpol_profiles


    def get_lcfs_radius(self, time: np.ndarray) -> np.ndarray:
        time = np.array(time)
        a = np.zeros(len(time))
        for i,t in enumerate(time):
            r, z = sf.rho2rz(self.eq, rho_in=0.999, t_in=t, coord_in='rho_pol')
            R_center = r[0][0].mean()
            z_center = z[0][0].mean()
            r_mean = np.sqrt((r[0][0] - R_center)**2 +
                                (z[0][0] - z_center)**2).mean()
            a[i] = r_mean
        return a


    def get_elongation(self) -> np.ndarray:
                
        time = self.timebase
        rhobase = self.rhobase
        minor_r = self.r
        
        elongation = np.zeros(rhobase.shape)
        for i, t in enumerate(time):
            rho_at_t = rhobase[i, :]
            _, z = sf.rho2rz(self.eq, rho_in=rho_at_t, t_in=t, 
                             coord_in='rho_pol')
            for j, _ in enumerate(rho_at_t):
                rj = minor_r[i, j]
                if z[0][j].size == 0:
                    elongation[i, j] = 1
                else:
                    z_max = z[0][j].max()
                    z_min = z[0][j].min()
                    elongation[i, j] = (z_max - z_min)/(2*rj)
        elongation[np.isnan(elongation)] = 1
        elongation[elongation < 1] = 1
        return elongation

    
    def get_triangularity(self, mode: str = "massaged") -> np.ndarray:
        
        time = self.timebase
        rhobase = self.rhobase
            
        triangularity = np.zeros(rhobase.shape)

        for i, t in enumerate(time):
            rho_at_t = rhobase[i, :]
            r, z = sf.rho2rz(self.eq, rho_in=rho_at_t, t_in=t, 
                             coord_in='rho_pol')
            for j, _ in enumerate(rho_at_t):
                if not (r[0][j].size == 0 or z[0][j].size == 0):
                    r_major = (r[0][j].max() + r[0][j].min())/2
                    r_minor = (r[0][j].max() - r[0][j].min())/2
                    z_max = z[0][j].argmax()
                    z_min = z[0][j].argmin()
                    max_z_R = r[0][j][z_max]
                    min_z_R = r[0][j][z_min]
                    upper_tri = (r_major - max_z_R)/r_minor
                    lower_tri = (r_major - min_z_R)/r_minor            
                    triangularity[i, j] = (upper_tri + lower_tri)/2
                else:
                    triangularity[i, j] = 0
                    
        triangularity[np.isnan(triangularity)] = 0
        triangularity[rhobase < 0.1] = 0
                
        if mode == "massaged":
            triangularity = self.massage_triangularity(triangularity)
            
        
        return triangularity


    # There is probably a much smarter way for doinǵ this!
    def massage_triangularity(self, triangularity: np.ndarray) -> np.ndarray:
        for i, row in enumerate(triangularity):
            # remove negative bits if overall triangularity is positive
            if row.max() > 0: 
                row[row < 0] = 0
            
            # 'massage' triangularity by averaging over falling bits
            falling_values_idx = np.where(np.gradient(row) > 0)[0]
            if falling_values_idx.size % 2 == 0:
                indices = np.zeros(falling_values_idx.size + 2)
                indices[-1] = -1
                indices[1:-1] = falling_values_idx
            else:
                indices = np.zeros(falling_values_idx.size + 1)
                indices[1:] = falling_values_idx
            indices = indices.reshape((-1, 2))
            indices = indices.astype(int)
            for (start, stop) in indices:
                stop = stop+1
                row[start:stop] = row[start:stop].mean()
            triangularity[i, :] = row
        
        # 'massage' further by appling a rolling mean twice   
        window_size = 10   
        kernel = np.ones(window_size) / window_size
        for i in range(2):
            triangularity = np.apply_along_axis(
                lambda row: np.convolve(row, kernel, mode='same'), 
                axis=1, arr=triangularity
            )
        return triangularity


    def get_elongation_shear(self, elongation: np.ndarray,
                             smooth_profile:bool = True) -> np.ndarray:
        
        minor_r = self.r_gp_fit if smooth_profile else self.r
        
        assert elongation.shape == minor_r.shape, \
                "elongation and minor_r must have same size"
        elongation_shear = np.zeros(elongation.shape)
        for i, (e, r) in enumerate(zip(elongation, minor_r)):
            elongation_shear[i, :] = r * np.gradient(e, r)
        return elongation_shear
        
        
    def get_triangularity_shear(self, triangularity: np.ndarray,
                                smooth_profile: bool = True) -> np.ndarray:
        
        minor_r = self.r_gp_fit if smooth_profile else self.r
        
        assert triangularity.shape == minor_r.shape, \
                "elongation and minor_r must have same size"
        triangularity_shear = np.zeros(triangularity.shape)
        for i, (t, r) in enumerate(zip(triangularity, minor_r)):
            triangularity_shear[i, :] = r * np.gradient(t, r)
        return triangularity_shear


    def smooth_profile(self, profile, l=0.3) -> np.ndarray:
        
        profile = self.interpol_profile(profile, self.rhobase, 
                                        self.rhobase_gp_fit)
            
        rho_test = np.linspace(0, 0.9, 100)
        rho_test = np.tile(rho_test, (self.timebase.size, 1))
        
        smooth_profile = np.zeros(profile.shape)
        for i, (p, rho) in enumerate(zip(profile, self.rhobase_gp_fit)):
            profile_error = 0.05 * p.max() * np.ones(p.shape)
            profile_error[0] = 0
            sigma_profile = 10*p.max()
            smooth_profile[i, :], _ = fit_profile(rho, p, rho, profile_error, 
                                                  sigma=sigma_profile, l=l)
        
        #smooth_profile = self.interpol_profile(smooth_profile, rho_fit, rho_test)
        return smooth_profile


    def get_magnetic_field_at_rho(self, time: np.ndarray, 
                                  rho_intervals: np.ndarray) -> np.ndarray:
    
        if not self.field_at_rho.any():
            field_at_rho = np.zeros(rho_intervals.shape)
            for i, t in enumerate(time):
                rho_at_t = rho_intervals[i]
                r, z = sf.rho2rz(self.eq, rho_in=rho_at_t, t_in=t, 
                                coord_in='rho_pol')
                for j, _ in enumerate(rho_at_t):
                    R_in = r[0][j]
                    z_in = z[0][j]
                    br, bz, bt = sf.rz2brzt(self.eq, r_in=R_in, 
                                            z_in=z_in, t_in=t)
                    b_total = np.sqrt(br**2 + bz**2 + bt**2)
                    field_at_rho[i, j] = b_total.mean()
            self.field_at_rho = field_at_rho
        return self.field_at_rho


    # Stuff for GPT below
    def get_magnetic_coords(self, time: float, 
                            fullsize: bool=False) -> np.ndarray:
        '''
        Returns rho and theta coordinates for the given shotnumber and time;
        Fullsize specifies the resolution (42x23 if False, 85x47 if True)
        '''
        with warnings.catch_warnings():
            warnings.simplefilter("ignore") # remove DeprecationWarning 
            rs = rs_full if fullsize else rs_small
            zs = zs_full if fullsize else zs_small
            rho = sf.rz2rho(self.eq, rs.flatten(), zs.flatten(), time)
            r0,z0 = sf.rho2rz(self.eq, 0.1, time)
            
            # if not len(r0[0][0]) or not len(z0[0][0]):
            #     # for shot 36173 for example eq.rho2rz(0.1,time) return empty lists
            #     # resulting in a array full on NaN values for theta.
            #     r0,z0 = eq.rho2rz(0.1,time)
            r0,z0 = r0[0][0].mean(), z0[0][0].mean()
            
            theta = np.arctan2(zs-z0,rs-r0)
            return rho.reshape(rs.shape), theta.reshape(rs.shape)


    def get_magnetic_time_series(self, time_points: np.ndarray) -> np.ndarray:
        '''
        takes shotnumber and array of timepoints in seconds as input returns 
        array with magnetic coords (42x23 pixels) for those timepoints
        '''
        rho = []
        theta = []
        for i, t in enumerate(time_points):
            rhoi, thetai = self.get_magnetic_coords(t)
            rho.append(rhoi)
            theta.append(thetai)
        return np.array(rho), np.array(theta)