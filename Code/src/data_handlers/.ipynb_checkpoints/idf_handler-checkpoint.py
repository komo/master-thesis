import sys
from pathlib import Path
from xmlrpc.client import Boolean
sys.path.append(str(Path(__file__).parents[1].absolute()))
sys.path.append(str(Path(__file__).parent.absolute()))
import aug_sfutils as sf
import numpy as np
from scipy.interpolate import interp1d, UnivariateSpline


class IDF_handler:
    
    def __init__(self, shotnumber: int, exp: str = "AUGD"):
        self.shotnumber = shotnumber
        self.exp = exp
        self.idf = sf.SFREAD(shotnumber, 'idf', exp=self.exp)
        self.date = self.idf.time
        self.getobject = self.idf.getobject
        self.getareabase = self.idf.getareabase
        self.timebase = self.temporal_downsample(self.getobject('time'))
        
        self.rhobase_r = self.temporal_downsample(
                                    self.getareabase('rho').T)
        self.rhobase_cde = self.temporal_downsample(
                                    self.getobject('cde_rp').T)
        self.rhobase_q = self.temporal_downsample(
                                    self.getareabase('q_sa').T)
        
        self.r = self.temporal_downsample(self.getobject('rho'))
        self.r_cde = self.interpol_profile(self.r, self.rhobase_r, self.rhobase_cde)
        self.r_q = self.interpol_profile(self.r, self.rhobase_r, self.rhobase_cde)
        mask = self.rhobase_r < 1
        max_index = np.argmax(self.rhobase_r * mask)
        self.a = self.r[:, max_index]
        
        self.dA = self.temporal_downsample(self.getobject("Vp"))
        self.dA_cde = self.temporal_downsample(self.getobject("cde_Vp"))
        
        self.q_profiles = self.temporal_downsample(self.getobject('q_sa'))
        self.rho_intervals = self.get_rho_intervals(self.q_profiles, 
                                                    self.rhobase_q)
        self.q_shear = self.get_q_sa_shear()
        
        self.te = self.temporal_downsample(self.getobject('cde_te'))
        self.ti = self.temporal_downsample(self.getobject('cde_ti'))
        self.ne = self.temporal_downsample(self.getobject('cde_ne'))
        self.ni = self.temporal_downsample(self.getobject('cde_ni'))
        self.z_eff = self.temporal_downsample(self.getobject('cde_zeff'))
        self.is_z_eff_const = self.get_is_z_eff_const()
                
        a = np.repeat(self.a, self.rhobase_cde.shape[1])
        a = a.reshape(self.timebase.size, -1)
        
        self.collision_frequency = self.get_collision_frequency()
        
        self.temp_ratio = self.te/self.ti
        
        self.nD, self.nB = self.get_ion_densities()
        d_te, d_ti, d_ne, d_nD, d_nB = self.get_cde_gradients()
        self.te_scale = (a/self.te)*d_te
        self.ti_scale = (a/self.ti)*d_ti
        self.ne_scale = (a/self.ne)*d_ne
        self.nD_scale = (a/self.nD)*d_nD
        self.nB_scale = (a/self.nB)*d_nB
        
        self.d_te = d_te
        self.d_ti = d_ti
            
        self.nD_ne_ratio = self.nD/self.ne
        self.nB_ne_ratio = self.nB/self.ne
        self.pressure_gradient = self.get_pressure_gradient()
        

    def get_rho_interval(self, q_profile: np.ndarray, rho_base: np.ndarray, 
                         n_intervals: int, rho_max: float) -> np.ndarray:
        
        if np.any(np.abs(q_profile) < 1):
            rho_min = rho_base[np.abs(q_profile) < 1].max()
            if rho_min < 0.05: rho_min = 0.05
        else:
            # don't start at 0 to prevent division by 0 in some cases
            # also for small rho the magnetic coordinates sometimes return 
            # empty list
            rho_min = 0.05
        return np.round(np.linspace(rho_min, rho_max, n_intervals), 3)


    def get_rho_intervals(self, q_profiles: np.ndarray, 
                          rho_basis: np.ndarray, 
                          n_intervals: int = 10, 
                          rho_max: float = 0.9) -> np.ndarray:
        assert q_profiles.shape == rho_basis.shape, \
                "profile and basis have different size"
        intervals = np.zeros((q_profiles.shape[0], n_intervals))
        for i, (q, rho) in enumerate(zip(q_profiles, rho_basis)):
            intervals[i, :] = self.get_rho_interval(q, rho, n_intervals, 
                                                    rho_max)
        return intervals
        

    def get_pressure_gradient(self) -> np.ndarray:
        # still has to be multiplied with a^2 and q; see Meneghini for 
        # definition rhobase is same as for get_minor_fluxsurf_radii()
        pressure = self.temporal_downsample(self.getobject('pres'))
        minor_r = self.temporal_downsample(self.getobject('rho'))
        flux_tor = self.temporal_downsample(self.getobject('flux_tor'))
        pressure_gradient = np.zeros(pressure.shape)
        for i,(p, r ,flux) in enumerate(zip(pressure, minor_r, flux_tor)):
            B_unit = np.gradient(flux, r) / (2*np.pi * r)
            pressure_gradient[i, :] = np.gradient(p, r) / (r * B_unit**2)
        return pressure_gradient
            
            
    def get_beta(self, MEQ) -> np.ndarray:
        rhobase = self.temporal_downsample(self.getareabase('pres').T)
        pressure = self.temporal_downsample(self.getobject('pres'))     
        pressure_at_intervals = self.interpol_profile(pressure, rhobase)
        B_at_intervals = MEQ.get_magnetic_field_at_rho(self.timebase, 
                                                       self.rho_intervals)
        my_0 = 1.2566e-6
        beta = pressure_at_intervals/(B_at_intervals/(2*my_0))
        return beta
            

    def interpol_profile(self, profiles: np.ndarray, rho_basis: np.ndarray, 
                         rho_intervals: np.ndarray = None) -> np.ndarray:
        if rho_intervals is None: rho_intervals = self.rho_intervals
        assert profiles.shape == rho_basis.shape, \
                "profile and basis have different size"
        interpol_profiles = np.zeros(rho_intervals.shape)
        for i, (p, r, intv) in enumerate(zip(profiles, rho_basis, 
                                             rho_intervals)):
            f = interp1d(r, p)
            interpol_profiles[i, :] = f(intv)
        return interpol_profiles


    def temporal_downsample(self, data: np.ndarray, 
                            n_points: int = 10, 
                            use_median: Boolean = True) -> np.ndarray:
        """
        Downsample profiles etc. along temporal axis, assuming that the 
        temporal axis is the first one
        """
        size = data.shape[0] // n_points
        cutoff = data.shape[0] - data.shape[0] % n_points
        downsampled_data = data[:cutoff, ...].reshape(size, n_points, -1)
        if use_median:
            return np.median(downsampled_data, axis=1)
        else:
            return downsampled_data.mean(axis=1)


    def get_cde_gradients(self) -> tuple[np.ndarray, np.ndarray, 
                                         np.ndarray, np.ndarray, 
                                         np.ndarray]:
        minor_r_cde_basis = self.interpol_profile(
            self.r, self.rhobase_r, self.rhobase_cde
        )
        d_te = np.zeros(self.te.shape)
        d_ti = np.zeros(self.ti.shape)
        d_ne = np.zeros(self.ne.shape)
        d_nD = np.zeros(self.nD.shape)
        d_nB = np.zeros(self.nB.shape)
        
        for i, r_base_cde in enumerate(minor_r_cde_basis):
            d_te[i] = np.gradient(self.te[i], r_base_cde)
            d_ti[i] = np.gradient(self.ti[i], r_base_cde)
            d_ne[i] = np.gradient(self.ne[i], r_base_cde)
            d_nD[i] = np.gradient(self.nD[i], r_base_cde)
            d_nB[i] = np.gradient(self.nB[i], r_base_cde)
        
        return d_te, d_ti, d_ne, d_nD, d_nB
            

    def get_q_sa_shear(self) -> np.ndarray:
        q_sa_shear = np.zeros(self.q_profiles.shape)
        r_at_q_intervals = self.interpol_profile(
            self.r, self.rhobase_r, self.rhobase_q)
        
        for i, (r, q, a) in enumerate(zip(r_at_q_intervals, 
                                          self.q_profiles, self.a)):
            q_sa_shear[i, :] = q**2 * a**2 / r**2 * np.gradient(q, r)

        return q_sa_shear


    def get_shafranov_shift(self, major_R: np.ndarray ) -> np.ndarray:
        shafranov_shift = np.zeros(self.r[:, 4:].shape)
        for i, (rho, r, R) in enumerate(zip(self.rhobase_r[:, 4:], 
                                    self.r[:, 4:], major_R[:, 4:])):
            spl = UnivariateSpline(rho, R)
            R_interpol = spl(rho)
            shafranov_shift[i, :] = np.gradient(R_interpol, r)
        return shafranov_shift
        

    def get_collision_frequency(self) -> np.ndarray:
        # From Emilianos Email
        #coulomb_log * ne * Zeff * R/sqrt(te) * sqrt(mi)/te^(3/2)
        #coulomb_log * ne * R /sqrt(Te) *sqrt(mi)/Te^(3/2)
        #TODO check formula Plasma formulary and script.
        coulomb_log = self.coulomb_log()
        a = np.repeat(self.a, self.rhobase_cde.shape[1])
        a = a.reshape(self.timebase.size, -1)
        # convert ev to Joule / mass of proton.
        cs = np.sqrt(1.602e-19 * self.te / (2 * 1.672e-27))
        # TODO check prefactor with emiliano
        # Check why Z_eff missing 
        collision_frequency = self.te**-1.5 * self.ne * coulomb_log
        
        collision_frequency = collision_frequency * a / cs
        return collision_frequency
    
    
    def coulomb_log(self) -> np.ndarray:
        '''
        return coulomb logarithm according to NRL Plasma formulary 2019 p. 34
        '''
        ne = self.ne
        Te = self.te
        cl1 = 24 - 0.5*np.log(ne/1e6) + np.log(Te)
        #cl2 = 15.2 - 0.5*np.log(ne/1e20) + np.log(Te/1000)
        return cl1#, cl2
    

    def get_ion_densities(self) -> tuple[np.ndarray, np.ndarray]:
        z_boron = 5
        nD = (z_boron - self.z_eff)/(z_boron - 1) * self.ni
        nB = self.ni-nD
        return nD, nB
    
    
    def get_is_z_eff_const(self) -> np.ndarray:
        is_z_eff_const = np.zeros(self.z_eff.shape[0]).astype(bool)
        for i, z_eff_at_t in enumerate(self.z_eff):
            if z_eff_at_t.min() == z_eff_at_t.max():
                is_z_eff_const[i] = True
        return is_z_eff_const
    
    def write_inp_file(self, filepath: str):
        inp_file_lines = self.getobject("inp_file")
        inp_file_lines = b''.join(inp_file_lines).decode("UTF-8")
        inp_file = open(filepath, "w")
        inp_file.writelines(inp_file_lines)
        inp_file.close()