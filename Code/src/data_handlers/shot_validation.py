import sys
from pathlib import Path
sys.path.append(str(Path(__file__).parents[2].absolute()))
import aug_sfutils as sf
import numpy as np


def check_IDR(shotnumber: int, source: str = "scratch") -> bool:
    try:
        valid = True
        idr = sf.SFREAD(shotnumber, "IDR", exp=source)
        fields_to_validate = ["powe", "powi", "time", "rho_out"]
        for field in fields_to_validate:
            if idr.sfh[field].status != 0:
                valid = False
                break
        return valid
    except:
        return False
    
    
    
def check_IDF(shotnumber: int, source: str = "scratch") -> bool:
    try:
        valid = True
        idf = sf.SFREAD(shotnumber, "IDf", exp=source)
        #  does not check all fields only some
        fields_to_validate = ["time", "rho", "cde_rp", "q_sa", "Vp", "cde_te", 
                              "pres", "flux_tor", "ecpowgyr", "sig_snc"] 
        for field in fields_to_validate:
            if idf.sfh[field].status != 0:
                valid = False
                break
        return valid
    except:
        return False
    
    