import sys
from pathlib import Path
from xmlrpc.client import Boolean
sys.path.append(str(Path(__file__).parents[1].absolute()))
sys.path.append(str(Path(__file__).parent.absolute()))
import aug_sfutils as sf
import numpy as np
from scipy.interpolate import interp1d, UnivariateSpline
from src.gp_profile_fit import fit_profile


class IDF_handler:
    
    def __init__(self, shotnumber: int, exp: str = "AUGD"):
        self.shotnumber = shotnumber
        self.exp = exp
        self.idf = sf.SFREAD(shotnumber, 'idf', exp=self.exp)
        self.date = self.idf.time
        self.getobject = self.idf.getobject
        self.getareabase = self.idf.getareabase
        
        # get basic time and rho basis profiles
        self.timebase = self.temporal_downsample(self.getobject('time'))
        self.rhobase_r = self.temporal_downsample(self.getareabase('rho').T)
        self.rhobase_cde = self.temporal_downsample(self.getobject('cde_rp').T)
        self.rhobase_q = self.temporal_downsample(self.getareabase('q_sa').T)
        
        # get the actual r basis and interpolate it to the other rho bases
        self.r = self.temporal_downsample(self.getobject('rho'))
        self.r_cde = self.interpol_profile(self.r, self.rhobase_r, 
                                           self.rhobase_cde)
        self.r_q = self.interpol_profile(self.r, self.rhobase_r, 
                                         self.rhobase_q)
        
        # compute a (max radius with closed flux surface)
        mask = self.rhobase_r < 1
        max_index = (self.rhobase_r * mask).argmax()
        self.a = self.r[:, max_index]
        a = np.repeat(self.a, self.rhobase_cde.shape[1])
        a = a.reshape(self.timebase.size, -1)
        
        # Load profiles from IDF shotfile
        self.te_raw = self.temporal_downsample(self.getobject('cde_te'))
        self.ti_raw = self.temporal_downsample(self.getobject('cde_ti'))
        self.ne_raw = self.temporal_downsample(self.getobject('cde_ne'))
        self.ni_raw = self.temporal_downsample(self.getobject('cde_ni'))
        self.z_eff = self.temporal_downsample(self.getobject('cde_zeff'))
        self.dA = self.temporal_downsample(self.getobject("Vp"))
        self.dA_cde = self.temporal_downsample(self.getobject("cde_Vp"))
        self.q_profiles = self.temporal_downsample(self.getobject('q_sa'))
        self.pressure_raw = self.temporal_downsample(self.getobject('pres'))
        self.q_shear_raw = self.get_q_sa_shear()
        self.q_shear_raw_alternative = self.get_q_sa_shear_alternative()
        # Get the rho intervalls that are used to sample the profiles from
        # and create an according rho and r basis that is used for smoothing 
        # the profiles with an gaussian process
        self.rho_intervals, self.cutoff_reasons = self.get_rho_intervals()
        self.rhobase_gp_fit = np.linspace(self.rho_intervals.min(axis=1), 
                                          self.rho_intervals.max(axis=1),
                                          100).T
        self.r_gp_fit = self.interpol_profile(self.r, self.rhobase_r, 
                                              self.rhobase_gp_fit)
        
        # smooth Q_shear, Te, Ti, Ne, Ni, pressure with a gaussian process 
        # between rho_min and rho_max of the rho_intervalls 
        self.get_smooth_proflies()
        
        # get nD, nB from ni and Z_eff
        self.nD, self.nB = self.get_ion_densities()
        # compute gradients and scale lenghts from smooth profiles
        d_te, d_ti, d_ne, d_nD, d_nB = self.get_cde_gradients()
        self.te_scale = -(a/self.te)*d_te
        self.ti_scale = -(a/self.ti)*d_ti
        self.ne_scale = -(a/self.ne)*d_ne
        self.nD_scale = -(a/self.nD)*d_nD
        self.nB_scale = -(a/self.nB)*d_nB
        
        # compute other stuff needed
        self.collision_frequency = self.get_collision_frequency()
        self.is_z_eff_const = self.get_is_z_eff_const()
        self.pressure_gradient = self.get_pressure_gradient()
                
        self.temp_ratio = self.te/self.ti

        self.d_te = d_te
        self.d_ti = d_ti
        self.d_ne = d_ne
        self.d_nD = d_nD
        self.d_nB = d_nB
            
        self.nD_ne_ratio = self.nD/self.ne
        self.nB_ne_ratio = self.nB/self.ne
        

    def get_rho_interval(self, rho_base: np.ndarray, 
                         q_profile: np.ndarray,
                         te: np.ndarray, 
                         ti: np.ndarray,  
                         n_intervals: int, 
                         rho_min: float, 
                         rho_max: float) -> tuple[np.ndarray, str]:
        
        rho_min_default = rho_min
        rho_min_te = rho_base[te.argmax()]
        rho_min_ti = rho_base[ti.argmax()]
        
        if np.any(np.abs(q_profile) < 1):
            index = rho_base[np.abs(q_profile) < 1].argmax() + 1
            rho_min_q = rho_base[index]
        else:
            rho_min_q = 0
        
        rho_min = max([rho_min_default, rho_min_q, rho_min_te, rho_min_ti])
        
        rho_intervall = np.round(np.linspace(rho_min, rho_max, n_intervals), 3)
        
        if rho_min == rho_min_te:
            cutoff_reason = "Hollow Te profile"
        elif rho_min == rho_min_ti:
            cutoff_reason = "Hollow Ti profile"
        elif rho_min == rho_min_q:
            cutoff_reason = "q below 1"
        else:
            cutoff_reason = "default"
        
        return rho_intervall, np.repeat(cutoff_reason, rho_intervall.size)


    def get_rho_intervals(self, n_intervals: int = 12, 
                          rho_min: float = 0.1,
                          rho_max: float = 0.9
                          ) -> tuple[np.ndarray, np.ndarray]:
        
        t_size = self.timebase.size
        intervals = np.zeros((t_size, n_intervals))
        cutoff_reasons = np.zeros((t_size, n_intervals), dtype="U18")
        
        rho = np.linspace(0, 0.9, 100)
        rho = np.tile(rho, (t_size, 1))
        
        q_at_rho = self.interpol_profile(self.q_profiles, self.rhobase_q, rho)
        te_at_rho = self.interpol_profile(self.te_raw, self.rhobase_cde, rho)
        ti_at_rho = self.interpol_profile(self.ti_raw, self.rhobase_cde, rho)
        
        for i, (q, rho, te, ti) in enumerate(zip(q_at_rho, rho, 
                                                 te_at_rho, ti_at_rho)):
            rho_interval, reasons = self.get_rho_interval(rho, q, te, ti, 
                                                          n_intervals, 
                                                          rho_min, rho_max)
            intervals[i, :] = rho_interval
            cutoff_reasons[i, :] = reasons
            
        return intervals, cutoff_reasons
        

    def get_pressure_gradient(self) -> np.ndarray:
        # still has to be multiplied with a^2 and q; see Meneghini for 
        # definition rhobase is same as for get_minor_fluxsurf_radii()
        flux_tor = self.temporal_downsample(self.getobject('flux_tor'))
        flux_tor = self.interpol_profile(flux_tor, self.rhobase_r, 
                                         self.rhobase_gp_fit)
        pressure_gradient = np.zeros(self.pressure.shape)
        for i,(p, r ,flux) in enumerate(zip(self.pressure, 
                                            self.r_gp_fit, 
                                            flux_tor)):
            B_unit = np.gradient(flux, r) / (2*np.pi * r)
            pressure_gradient[i, :] = np.gradient(p, r) / (r * B_unit**2)
        return pressure_gradient
            
            
    def get_beta(self, MEQ) -> np.ndarray:
        pressure_at_intervals = self.interpol_profile(self.pressure, 
                                                      self.rhobase_gp_fit)
        B_at_intervals = MEQ.get_magnetic_field_at_rho(self.timebase, 
                                                       self.rho_intervals)
        my_0 = 1.2566e-6
        beta = pressure_at_intervals/(B_at_intervals/(2*my_0))
        return beta
            

    def interpol_profile(self, profiles: np.ndarray, rho_basis: np.ndarray, 
                         rho_intervals: np.ndarray = None) -> np.ndarray:
        if rho_intervals is None: rho_intervals = self.rho_intervals
        assert profiles.shape == rho_basis.shape, \
                "profile and basis have different size"
        interpol_profiles = np.zeros(rho_intervals.shape)
        for i, (p, r, intv) in enumerate(zip(profiles, rho_basis, 
                                             rho_intervals)):
            interpol_profiles[i, :] = interp1d(r, p)(intv)
        return interpol_profiles


    def temporal_downsample(self, data: np.ndarray, 
                            n_points: int = 5, 
                            use_median: Boolean = True) -> np.ndarray:
        """
        Downsample profiles etc. along temporal axis, assuming that the 
        temporal axis is the first one
        """
        size = data.shape[0] // n_points
        cutoff = data.shape[0] - data.shape[0] % n_points
        downsampled_data = data[:cutoff, ...].reshape(size, n_points, -1)
        if use_median:
            return np.median(downsampled_data, axis=1)
        else:
            return downsampled_data.mean(axis=1)


    def get_cde_gradients(self) -> tuple[np.ndarray, np.ndarray, 
                                         np.ndarray, np.ndarray, 
                                         np.ndarray]:
        d_te = np.zeros(self.te.shape)
        d_ti = np.zeros(self.ti.shape)
        d_ne = np.zeros(self.ne.shape)
        d_nD = np.zeros(self.nD.shape)
        d_nB = np.zeros(self.nB.shape)
        
        for i, r in enumerate(self.r_gp_fit):
            d_te[i] = np.gradient(self.te[i], r)
            d_ti[i] = np.gradient(self.ti[i], r)
            d_ne[i] = np.gradient(self.ne[i], r)
            d_nD[i] = np.gradient(self.nD[i], r)
            d_nB[i] = np.gradient(self.nB[i], r)
        
        return d_te, d_ti, d_ne, d_nD, d_nB
            

    def get_q_sa_shear(self) -> np.ndarray:
        q_sa_shear = np.zeros(self.q_profiles.shape)
        for i, (r, q, a) in enumerate(zip(self.r_q, self.q_profiles, self.a)):
            q_sa_shear[i, :] = (r / q) * np.gradient(q, r)
        return q_sa_shear
    
    
    def get_q_sa_shear_alternative(self) -> np.ndarray:
        q_sa_shear = np.zeros(self.q_profiles.shape)
        for i, (r, q, a) in enumerate(zip(self.r_q, self.q_profiles, self.a)):
            q_sa_shear[i, :] = q**2 * a**2 / r**2 * np.gradient(q, r)
        return q_sa_shear


    def get_shafranov_shift(self, major_R: np.ndarray ) -> np.ndarray:
        shafranov_shift = np.zeros(self.r[:, 4:].shape)
        for i, (rho, r, R) in enumerate(zip(self.rhobase_r[:, 4:], 
                                    self.r[:, 4:], major_R[:, 4:])):
            spl = UnivariateSpline(rho, R)
            R_interpol = spl(rho)
            shafranov_shift[i, :] = np.gradient(R_interpol, r)
        return shafranov_shift
        

    def get_collision_frequency(self) -> np.ndarray:
        # From Emilianos Email
        #coulomb_log * ne * Zeff * R/sqrt(te) * sqrt(mi)/te^(3/2)
        #coulomb_log * ne * R /sqrt(Te) *sqrt(mi)/Te^(3/2)
        #TODO check formula Plasma formulary and script.
        coulomb_log = self.coulomb_log()
        a = np.repeat(self.a, self.rhobase_gp_fit.shape[1])
        a = a.reshape(self.timebase.size, -1)
        # convert ev to Joule / mass of Deuterium.
        cs = np.sqrt(1.602e-19 * self.te / (2 * 1.672e-27))
        # TODO check prefactor with emiliano
        # Check why Z_eff missing 
        collision_frequency = self.te**-1.5 * self.ne * coulomb_log
        
        collision_frequency = collision_frequency * a / cs
        return collision_frequency
    
    
    def coulomb_log(self) -> np.ndarray:
        '''
        return coulomb logarithm according to NRL Plasma formulary 2019 p. 34
        '''
        ne = self.ne
        Te = self.te
        cl1 = 24 - 0.5*np.log(ne/1e6) + np.log(Te)
        #cl2 = 15.2 - 0.5*np.log(ne/1e20) + np.log(Te/1000)
        return cl1#, cl2
    

    def get_ion_densities(self) -> tuple[np.ndarray, np.ndarray]:
        nB = (self.ne*self.z_eff - self.ni) / 24
        nD = self.ni-nB
        return nD, nB
    
    
    def get_is_z_eff_const(self) -> np.ndarray:
        is_z_eff_const = np.zeros(self.z_eff.shape[0]).astype(bool)
        for i, z_eff_at_t in enumerate(self.z_eff):
            if z_eff_at_t.min() == z_eff_at_t.max():
                is_z_eff_const[i] = True
        return is_z_eff_const
    
    
    def get_smooth_proflies(self, boundary_conditions:bool = True):
        
        self.te = np.zeros(self.rhobase_gp_fit.shape)
        self.ti = np.zeros(self.rhobase_gp_fit.shape)
        self.ne = np.zeros(self.rhobase_gp_fit.shape)
        self.ni = np.zeros(self.rhobase_gp_fit.shape)
        self.q_shear = np.zeros(self.rhobase_gp_fit.shape)
        self.q_shear_alternative = np.zeros(self.rhobase_gp_fit.shape)
        self.pressure = np.zeros(self.rhobase_gp_fit.shape)
        
        te_raw = self.interpol_profile(self.te_raw, self.rhobase_cde, 
                                       self.rhobase_gp_fit)
        ti_raw = self.interpol_profile(self.ti_raw, self.rhobase_cde, 
                                       self.rhobase_gp_fit)
        ne_raw = self.interpol_profile(self.ne_raw, self.rhobase_cde, 
                                       self.rhobase_gp_fit)
        ni_raw = self.interpol_profile(self.ni_raw, self.rhobase_cde, 
                                       self.rhobase_gp_fit)
        q_shear_raw = self.interpol_profile(self.q_shear_raw, self.rhobase_q, 
                                            self.rhobase_gp_fit)
        q_shear_raw_alt = self.interpol_profile(self.q_shear_raw_alternative, 
                                                self.rhobase_q, 
                                                self.rhobase_gp_fit)
        pressure_raw = self.interpol_profile(self.pressure_raw, self.rhobase_r, 
                                             self.rhobase_gp_fit)
        
        def get_error(profile: np.ndarray, rho: np.ndarray, 
                      multiplier: float = 0.05,
                      boundary_conditions: bool = boundary_conditions
                      ) -> np.ndarray:
            error = multiplier * profile.max() * np.ones(rho.shape)
            #error[0] = error[1] = error[0] * (not boundary_conditions)
            return error
        
        for i, rho in enumerate(self.rhobase_gp_fit):
            te_error = get_error(te_raw[i], rho)
            sigma_te = 0.5 * te_raw[i].max()
            self.te[i, :], _ = fit_profile(rho, te_raw[i], rho, te_error, 
                                           sigma=sigma_te, l=0.3)
            
            ti_error = get_error(ti_raw[i], rho)
            sigma_ti = 0.5 * ti_raw[i].max()    
            self.ti[i, :], _ = fit_profile(rho, ti_raw[i], rho, ti_error, 
                                           sigma=sigma_ti, l=0.3)
            
            ne_error = get_error(ne_raw[i], rho) 
            sigma_ne = 0.5 * ne_raw[i].max()     
            self.ne[i, :], _ = fit_profile(rho, ne_raw[i], rho, ne_error, 
                                           sigma=sigma_ne, l=0.3)
            
            ni_error = get_error(ni_raw[i], rho)
            sigma_ni = 0.5 * ni_raw[i].max()     
            self.ni[i, :], _ = fit_profile(rho, ni_raw[i], rho, ni_error, 
                                           sigma=sigma_ni, l=0.3)
            
            q_shear_error = get_error(q_shear_raw[i], rho, 0.002, False)
            #q_error[-1] = 0
            sigma_q_shear = 0.5 * q_shear_raw[i].max()
            self.q_shear[i, :], _ = fit_profile(rho, q_shear_raw[i], rho, 
                                                q_shear_error, 
                                                sigma=sigma_q_shear, l=0.2)
            
            q_shear_error_alt = get_error(q_shear_raw_alt[i], rho, 0.002, False)
            #q_error[-1] = 0
            sigma_q_shear_alt = 0.5 * q_shear_raw_alt[i].max()
            self.q_shear_alternative[i, :], _ = fit_profile(rho, 
                                                    q_shear_raw_alt[i], 
                                                    rho, q_shear_error_alt, 
                                                    sigma=sigma_q_shear_alt, 
                                                    l=0.2)
            
            pressure_error = get_error(pressure_raw[i], rho)
            sigma_pressure = 0.5 * pressure_raw[i].max()
            self.pressure[i, :], _ = fit_profile(rho, pressure_raw[i], 
                                                 rho, pressure_error, 
                                                 sigma=sigma_pressure, l=0.3)


    def get_time_index(self, time:float) -> int:
        return np.abs(time - self.timebase).argmin()

    
    def write_inp_file(self, filepath: str):
        inp_file_lines = self.getobject("inp_file")
        inp_file_lines = b''.join(inp_file_lines).decode("UTF-8")
        inp_file = open(filepath, "w")
        inp_file.writelines(inp_file_lines)
        inp_file.close()
