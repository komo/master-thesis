import sys
from pathlib import Path
sys.path.append(str(Path(__file__).parents[1].absolute()))
import aug_sfutils as sf
import numpy as np
from scipy.stats import norm
from scipy.signal import butter, sosfilt, sosfilt_zi, sosfiltfilt
from pathlib import Path
from constants import STEPS_PER_SECOND_BOLOMETER
import pandas as pd


def butter_highpass(highcut: int, fs: float, order:int) -> np.ndarray:
    nyq=0.5+fs
    high = highcut/nyq
    sos = butter(order, high, analog=False, btype="high", output='sos')
    return sos


def butter_highpass_filter(data: np.ndarray, highcut: int, 
                           fs: float, order: int = 3) -> np.ndarray:
    sos = butter_highpass(highcut, fs, order=order)
    return sosfiltfilt(sos, data)


# def filter_data(data: np.ndarray, cutoff: float = 30, 
#                 samplingrate: int = 10000) -> [np.ndarray, np.ndarray]:
#     """
#     Calculate filtered shot signal
    
#     Parameters
#     ----------
#     data : np.ndarray
#         original signal
#     cutoff : int, default=30 
#         cutoff frequency for filter
#     samplingrate : int, default=10000
#         samplingrate for filter
    
#     Returns
#     -------
#     highpass_data : np.ndarray
#     lowpass_data : np.ndarray
#     """

#     print("Filtering data")
#     highpass_data = np.zeros(data.shape)

#     for i in range(data.shape[1]): 
#         highpass_data[:, i] = butter_highpass_filter(   data[:, i], 
#                                                         cutoff, 
#                                                         samplingrate, 
#                                                         order=3 )
#     lowpass_data = data - highpass_data
#     return lowpass_data


def filter_data(data: np.ndarray) -> np.ndarray:
    """
    Calculate filtered shot signal with moving average
    
    Parameters
    ----------
    data : np.ndarray
        original signal
    
    Returns
    -------
    filtered_data : np.ndarray
    """

    filtered_data = np.zeros(data.shape)

    for i in range(data.shape[1]):
        windows = pd.Series(data[:, i]).rolling(250) 
        filtered_data[:, i] = windows.mean()
        #highpass_data[:, i] = butter_highpass_filter(   data[:, i], 
        #                                                cutoff, 
        #                                                samplingrate, 
        #                                                order=3 )
    #lowpass_data = data - highpass_data
    
    return filtered_data


def get_bolometer_data(shotnumber: int) -> tuple[np.ndarray, np.ndarray]:
    "Loads real bolometer measurement for given shotnumber via aug_sfutils"
    
    blb = sf.SFREAD('blb', shotnumber) # type: ignore
    time_base = blb.getobject('timFHC')[2500:-2500]
    powFHC = blb.getobject("powFHC")
    powFVC = blb.getobject("powFVC")
    powFDC = blb.getobject("powFDC")
    # Only use first three cameras as they don't move much overtime
    powFLX = np.zeros((powFHC.shape[0], 8))#blb.getobject("powFLX")
    # only first 4 channels active in FLH
    #powFLH = blb.getobject("powFLH")[:,:4]
    # FLH is always set to zero as the signal is usually bad and not 
    # available in more recent shots
    powFLH = np.zeros((powFHC.shape[0], 4))
    powFHS = np.zeros((powFHC.shape[0], 8))#blb.getobject("powFHS")

    data = np.concatenate((powFHC,powFVC,powFDC,
                powFLX,powFLH,powFHS), axis=1)[2500:-2500]
    # if np.isnan(data).any():
    #     data[np.isnan(data)] = 0
    #     print(  "Some measurements contain NaN values. " +
    #             "Those values will be set to 0"    )

    return time_base, data


def get_filtered_bolometer_data(shotnumber: int) -> tuple[np.ndarray, np.ndarray]:
    """returns lowpass filtered bolometer data for given shotnumber"""
    time, data = get_bolometer_data(shotnumber)
    filtered_data = filter_data(data)
    filtered_data[np.isnan(filtered_data)] = 0
    return time, filtered_data


def get_x_point_pos(shotnumber: int) -> bool:
    """
    returns whether x-point is up (true) or down (false). 
    Requires IDE which is not available for all shots!
    """
    ide = sf.SFREAD(shotnumber, "IDE") # type: ignore
    # 1 = inner limiter, 2 = outer limiter, 3 = x-point up, 
    # 4 = x-point down 5 = ???
    ikCAT = np.unique(ide.getobject("ikCAT"))
    if 3 in ikCAT and not 4 in ikCAT:
        return True
    elif 4 in ikCAT and not 3 in ikCAT:
        return False
    else:
        #TODO implement logging
        print("X point could not be determined unambiguously. \
              Assuming it is down")
        return False


def get_data_at_t(data: np.ndarray, time: float) -> np.ndarray:
    """
    takes bolometer data for whole shot as input and returns measurements 
    and given timepoint (in seconds)
    """
    return data[int(time*STEPS_PER_SECOND_BOLOMETER)]