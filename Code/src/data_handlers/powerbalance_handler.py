import sys
from pathlib import Path
import numpy as np
from scipy.interpolate import UnivariateSpline, interp1d
sys.path.append(str(Path(__file__).parents[1].absolute()))
sys.path.append(str(Path(__file__).parent.absolute()))
import aug_sfutils as sf
from data_handlers.bolometer_handler import get_filtered_bolometer_data, \
    get_x_point_pos
from GPT.gp_inference import calculate_timeseries, calculate_timeseries_fast
from GPT.profiles import get_profile_time_series
from idf_handler import IDF_handler
from meq_handler import MEQ_handler
from constants import *

class Powerbalance_handler:
    
    def __init__(self, idf_for_shot: IDF_handler, meq_for_shot: MEQ_handler):
        self.IDF_handler = idf_for_shot
        self.rhobase_cde = self.IDF_handler.rhobase_cde
        self.rhobase_r = self.IDF_handler.rhobase_r
        self.r = self.IDF_handler.r
        self.R = meq_for_shot.R
        self.temporal_downsample = self.IDF_handler.temporal_downsample
        self.interpol_profile = self.IDF_handler.interpol_profile
        self.timebase = self.IDF_handler.timebase.reshape(-1)
        self.shotnumber = self.IDF_handler.shotnumber
        self.rho_intervals = self.IDF_handler.rho_intervals
        self.meq = meq_for_shot
        self.exp = self.IDF_handler.exp
        
    
    def get_NBI_power(self) -> tuple[np.ndarray, np.ndarray, np.ndarray]:
        IDR = sf.SFREAD(self.shotnumber, 'IDR', exp=self.exp)
        power_profile_electron = self.temporal_downsample(
            IDR.getobject("powe")
        )
        power_profile_ion = self.temporal_downsample(
            IDR.getobject("powi")
        )
        rhobase_NBI = IDR.getobject("rho_out")
        
        rhobase_NBI = np.repeat(rhobase_NBI.T, 
                                 power_profile_electron.shape[0], axis=0)
        
        dV = self.temporal_downsample(IDR.getobject("dV"))
        
        power_profile_electron = power_profile_electron * dV
        power_profile_ion = power_profile_ion * dV
        
        return rhobase_NBI, np.cumsum(power_profile_electron, axis=1), \
                            np.cumsum(power_profile_ion, axis=1)
        
    
    def get_ECRH_power(self, 
                       cumsum: bool = True) -> tuple[np.ndarray, np.ndarray]:
        powerprofile_ECRH = self.IDF_handler.getobject("ecpowgyr").sum(axis=2)
        powerprofile_ECRH = self.IDF_handler.temporal_downsample(
            powerprofile_ECRH
        )
        rhobase_ECRH = self.IDF_handler.temporal_downsample(
            self.IDF_handler.getareabase("ecpowgyr").T
        )
        
        dA = self.temporal_downsample(self.IDF_handler.getobject("Vp")) \
                .astype(np.float64)
        dA = np.array(dA)
        
        # Interpol rho basis and dA to basis of ecrh power 
        # not the otherway around since normal rhobasis goes beyond 
        # rhobasis of ecrh
        r_ecrh = self.interpol_profile(self.r, self.rhobase_r, rhobase_ECRH)
        dA = self.interpol_profile(dA, self.rhobase_r, rhobase_ECRH)
        
        dr = np.zeros(r_ecrh.shape).astype(np.float64)
        for i, ri in enumerate(r_ecrh):
            dr[i] = np.gradient(ri)
        #print(dA[80, :]*dr[80, :])
        #print(np.sum(dA[80, :]*dr[80, :]))
        #print(dA.dtype, dr.dtype)
        #print("Volume: ", np.sum(dA*dr, axis=1))
        powerprofile_ECRH = powerprofile_ECRH * dA * dr
        if cumsum:
            powerprofile_ECRH = np.cumsum(powerprofile_ECRH, axis=1)
        return rhobase_ECRH, powerprofile_ECRH
        
    
    def get_ohmic_power(self) -> np.ndarray:

        conductivity = self.temporal_downsample(
            self.IDF_handler.getobject("sig_snc")
        )
        current = self.temporal_downsample(
            self.IDF_handler.getobject("cde_ohcu")
        )
               
        dA = self.IDF_handler.dA_cde
        
        dr = np.zeros(self.IDF_handler.r_cde.shape).astype(np.float64)
        for i, ri in enumerate(self.IDF_handler.r_cde):
            dr[i] = np.gradient(ri)
        
        ohmic_power_profile = current**2/conductivity * dA * dr
        return np.cumsum(ohmic_power_profile, axis=1)
        
    
    def get_radiated_power(self, fastmode: bool = False, 
                           save_emissivity: bool = False) -> np.ndarray:
        
        timebase_bolo, bolo = get_filtered_bolometer_data(self.shotnumber)
        rho, _ = self.meq.get_magnetic_time_series(self.timebase)
        bolo_at_timebase = np.zeros((self.timebase.size, bolo.shape[1]))
        for channel in range(bolo.shape[1]):
            f = interp1d(timebase_bolo, bolo[:, channel])
            bolo_at_timebase[:, channel] = f(self.timebase)
        
        x_point_up = get_x_point_pos(self.shotnumber)
        
        if fastmode:
            emissivity_fits = calculate_timeseries_fast(bolo_at_timebase, 
                                                        PARAMS_STATIC_CART,
                                                        x_point_up=x_point_up)
        else:
            emissivity_fits = calculate_timeseries(bolo_at_timebase, 
                                                    PARAMS_STATIC_CART,
                                                    x_point_up=x_point_up)
        radial_emiss_profiles = get_profile_time_series(emissivity_fits, rho)
        
        if save_emissivity:
            np.save("data/emissivity_visualized/shot{}emiss.npy"
                    .format(self.shotnumber), np.array(emissivity_fits))
            np.save("data/emissivity_visualized/shot{}emissprofiles.npy"
                    .format(self.shotnumber), np.array(radial_emiss_profiles))
            np.save("data/emissivity_visualized/shot{}timebase.npy"
                    .format(self.shotnumber), self.timebase)
        
        emiss_profiles_interpol = np.zeros((self.timebase.size, 
                                            self.rho_intervals.shape[1]))
        
        for i, _ in enumerate(emiss_profiles_interpol):
            spl = interp1d(radial_emiss_profiles[i][1], 
                                   np.cumsum(radial_emiss_profiles[i][0]))
            emiss_profiles_interpol[i, :] = spl(self.rho_intervals[i])
        
        return emiss_profiles_interpol
        #return np.cumsum(emiss_profiles_interpol, axis=1)
    
    
    def electron_ion_exchange(self):
        coulomb = self.IDF_handler.coulomb_log()
        te = self.IDF_handler.te
        ti = self.IDF_handler.ti
        ne = self.IDF_handler.ne
        ni = self.IDF_handler.ni
        nD = self.IDF_handler.nD
        nB = self.IDF_handler.nB
        z_eff = self.IDF_handler.z_eff
        
        # using the atomic mass of deuterium and boron
        A = 2*(nD/ni) + 10.8*(nB/ni)
        
        exchange = 0.00246 * coulomb * (ne/1e19) * (ni/1e19) * z_eff**2 * \
                    ((te-ti)/1000) / A / (te/1000)**(3/2) * 1e6
                    
        dA = self.IDF_handler.dA_cde

        r_cde = self.interpol_profile(self.IDF_handler.r, self.IDF_handler.rhobase_r, self.IDF_handler.rhobase_cde)

        dr = np.zeros(r_cde.shape).astype(np.float64)
        for i, ri in enumerate(r_cde):
            dr[i] = np.gradient(ri)
            
        exchange_integrated = exchange * dr * dA
                
        return np.cumsum(exchange_integrated, axis=1)
    
    
    # def get_total_plasma_energy(self):
    #     idg = sf.SFREAD(self.shotnumber, "IDG", exp=self.exp)
    #     timebase = self.temporal_downsample(
    #         idg.gettimebase("Wmhd")
    #     )    
    #     plasma_energy = self.temporal_downsample(
    #         idg.getobject("Wmhd")
    #     )
        
    #     return timebase, plasma_energy

         
    
    def get_data_for_intervals(self, GPT_fastmode: bool = False) -> dict:
        
        rho_nbi, power_nbi_e, power_nbi_i = self.get_NBI_power()
        
        ohmic_power = self.get_ohmic_power()
        rho_ecrh, power_ecrh = self.get_ECRH_power()
        
        radiation_loss = self.get_radiated_power(fastmode=GPT_fastmode)
        
        power_exchange = self.electron_ion_exchange()
        
        power_nbi_e_intervals = self.interpol_profile(power_nbi_e, rho_nbi)
        power_nbi_i_intervals = self.interpol_profile(power_nbi_i, rho_nbi)
        power_ecrh_intervals = self.interpol_profile(power_ecrh, rho_ecrh)
        power_ohmic_intervals = self.interpol_profile(ohmic_power, 
                                                      self.rhobase_cde)
        power_exchange_intervals = self.interpol_profile(power_exchange, 
                                                         self.rhobase_cde)

        d_te, d_ti, _, _, _ = self.IDF_handler.get_cde_gradients()
        d_te = self.interpol_profile(d_te, self.rhobase_cde)
        d_ti = self.interpol_profile(d_ti, self.rhobase_cde)
        ne = self.interpol_profile(self.IDF_handler.ne, self.rhobase_cde)
        ni = self.interpol_profile(self.IDF_handler.ni, self.rhobase_cde)
        
        dA = self.interpol_profile(self.IDF_handler.dA, self.rhobase_r)
                
        ion_power = power_nbi_i_intervals + power_exchange_intervals
        electron_power = power_nbi_e_intervals + power_ecrh_intervals + \
                            power_ohmic_intervals - radiation_loss - \
                            power_exchange_intervals
        
        CHI_e = electron_power / dA / ne / (-d_te *  1.602e-19)
        CHI_i = ion_power / dA / ni / (-d_ti * 1.602e-19)
        
        #print(power_ecrh[50, :], power_ecrh_intervals[50, :])
        
        return {CHI_ELECTRON: CHI_e, CHI_ION: CHI_i, 
                OHMIC_POWER: power_ohmic_intervals,
                RADIATION_LOSS: radiation_loss, 
                ECRH_POWER: power_ecrh_intervals, 
                NBI_ELECTRON_POWER: power_nbi_e_intervals,
                NBI_ION_POWER: power_nbi_i_intervals, 
                EXCHANGE_TERM: power_exchange_intervals,
                ELECTRON_TOTAL_POWER: electron_power,
                ION_TOTAL_POWER: ion_power}