import numpy as np

# DataBase fields
## Metadata
SHOT = "shot_number"
DATE = "shot_date"
TIME = "time"
RHO = "rho"

## Redundant or not used as input/output for network
PLASMA_R = "plasma_r_idf"
RADIATION_LOSS = "radiation_loss"
OHMIC_POWER = "ohmic_power"
EXCHANGE_TERM = "exchange_term"
NBI_ELECTRON_POWER = "nbi_electron_power"
NBI_ION_POWER = "nbi_ion_power"
ECRH_POWER = "ecrh_power"
ELECTRON_TOTAL_POWER = "electron_total_power"
ION_TOTAL_POWER = "ion_total_power"
ELECTRON_TEMP = "electron_temperature"
ION_TEMP = "ion_temperature"
ELECTRON_DENSITY = "electron_density"
ION_DENSITY = "ion_density"
DEUTERIUM_DENSITY = "deuterium_density"
BORON_DENSITY = "boron_density"
ELECTRON_TEMP_GRAD = "electron_temp_grad"
ION_TEMP_GRAD = "ion_temp_grad"
ELECTRON_DENSITY_GRAD = "electron_density_grad"
DEUTERIUM_DENSITY_GRAD = "deuterium_density_grad"
BORON_DENSITY_GRAD = "boron_density_grad"
FLUX_TUBE_SURF_AREA = "dA"
Q_SAFETY_SHEAR_ALT = "q_sa_shear_alternative"


## Inputs to network
ASPECT_RATIO = "aspect_ratio" # calculated with r from IDF and R from sf.EQU
ELONGATION = "elongation"
ELONGATION_SHEAR = "elongation_shear"
TRIANGULARITY = "triangularity"
TRIANGULARITY_SHEAR = "triangularity_shear"
SHAFRANOV_SHIFT = "shafranov_shift"
Q_SAFETY = "q_sa"
Q_SAFETY_SHEAR = "q_sa_shear"
BETA = "beta_e"
COLLISION_FREQUENCY = "collision_frequency"
T_RATIO = "T_ratio"
ND_NE_RATIO = "nD_ne_ratio"
NB_NE_RATIO = "nB_ne_ratio"
Z_EFF = "Z_eff"
TE_SCALE = "te_scale"
TI_SCALE = "ti_scale"
NE_SCALE = "ne_scale"
ND_SCALE = "nD_scale"
NB_SCALE = "nB_scale"
PRESSURE_GRADIENT = "pressure_gradient"

## Outputs of network
CHI_ELECTRON = "CHI_e"
CHI_ION = "CHI_i"

## Flags and additional information
IS_Z_EFF_CONST = "is_z_eff_const"
IS_OUTLIER = "is_potential_outlier"
OUTLIER_FIELDS = "outlier_fields"
CUTOFF_REASON = "cutoff_reason"

DB_COLUMNS = [SHOT, DATE, TIME, RHO, ASPECT_RATIO, 
              PLASMA_R, ELONGATION, 
              ELONGATION_SHEAR, TRIANGULARITY, TRIANGULARITY_SHEAR,
              SHAFRANOV_SHIFT, Q_SAFETY, Q_SAFETY_SHEAR, BETA,
              COLLISION_FREQUENCY, T_RATIO, Z_EFF, IS_Z_EFF_CONST,
              ND_NE_RATIO, NB_NE_RATIO, TE_SCALE, TI_SCALE, NE_SCALE, 
              ND_SCALE, NB_SCALE, PRESSURE_GRADIENT, RADIATION_LOSS, 
              OHMIC_POWER, EXCHANGE_TERM, NBI_ELECTRON_POWER, 
              NBI_ION_POWER, ECRH_POWER, CHI_ELECTRON, CHI_ION,
              ELECTRON_TEMP, ION_TEMP, ELECTRON_DENSITY, 
              ION_DENSITY, DEUTERIUM_DENSITY, BORON_DENSITY,
              ELECTRON_TEMP_GRAD, ION_TEMP_GRAD, ELECTRON_DENSITY_GRAD,
              DEUTERIUM_DENSITY_GRAD, BORON_DENSITY_GRAD, FLUX_TUBE_SURF_AREA,
              IS_OUTLIER, OUTLIER_FIELDS, CUTOFF_REASON]


DB_DATATYPES = {SHOT: int, DATE: np.datetime64, TIME: float, RHO: float, 
                ASPECT_RATIO: float, PLASMA_R: float, 
                ELONGATION:float, ELONGATION_SHEAR: float, 
                TRIANGULARITY: float, TRIANGULARITY_SHEAR: float, 
                Q_SAFETY: float, Q_SAFETY_SHEAR: float, BETA: float, 
                COLLISION_FREQUENCY: float, SHAFRANOV_SHIFT: float, 
                T_RATIO: float, Z_EFF: float, IS_Z_EFF_CONST: bool,
                ND_NE_RATIO: float, NB_NE_RATIO: float, 
                TE_SCALE: float, TI_SCALE: float, NE_SCALE: float, 
                ND_SCALE: float, NB_SCALE: float, PRESSURE_GRADIENT: float,
                RADIATION_LOSS: float, OHMIC_POWER: float, 
                EXCHANGE_TERM: float, NBI_ELECTRON_POWER: float, 
                NBI_ION_POWER: float, ECRH_POWER: float, 
                CHI_ELECTRON: float, CHI_ION: float,
                ELECTRON_TEMP: float, ION_TEMP: float,
                ELECTRON_DENSITY: float, ION_DENSITY: float, 
                DEUTERIUM_DENSITY: float, BORON_DENSITY: float,
                ELECTRON_TEMP_GRAD: float, ION_TEMP_GRAD: float, 
                DEUTERIUM_DENSITY_GRAD: float, BORON_DENSITY_GRAD: float,
                FLUX_TUBE_SURF_AREA: float, Q_SAFETY_SHEAR_ALT: float,
                IS_OUTLIER: bool, OUTLIER_FIELDS: str, CUTOFF_REASON: str}

MODEL_INPUTS = [ASPECT_RATIO, ELONGATION, ELONGATION_SHEAR, TRIANGULARITY, 
                TRIANGULARITY_SHEAR, SHAFRANOV_SHIFT, Q_SAFETY, 
                Q_SAFETY_SHEAR, BETA, COLLISION_FREQUENCY, T_RATIO, Z_EFF, 
                ND_NE_RATIO, NB_NE_RATIO, TE_SCALE, TI_SCALE, NE_SCALE, 
                ND_SCALE, NB_SCALE, PRESSURE_GRADIENT]


# define maximum thresholds for check_outliers.py
OUTLIER_THRESHOLDS = {
    ELONGATION: 10,
    ELONGATION_SHEAR: 10,
    Q_SAFETY: 20,
    Q_SAFETY_SHEAR: 1e3,
    COLLISION_FREQUENCY: 1e13,
    T_RATIO: 10,
    Z_EFF: 10,
    NB_NE_RATIO: 0.5,
    TE_SCALE: 8,
    TI_SCALE: 8,
    NE_SCALE: 20,
    ND_SCALE: 20,
    NB_SCALE: 20,
    PRESSURE_GRADIENT: 1e6,
    RADIATION_LOSS: 1e7,
    OHMIC_POWER: 2e6,
    EXCHANGE_TERM: 1e8,
    NBI_ELECTRON_POWER: 5e6,
    NBI_ION_POWER: 1e7,
    ECRH_POWER: 6e6,
    CHI_ELECTRON: 10,
    CHI_ION: 10
}

# Stuff for transport_model and data normalization
NORM_TYPE = "norm_type"
## for minmax norm
NORM_MINMAX = "minmax"
NORM_MINMAX_MIN = "minmax_min"
NORM_MINMAX_MAX = "minmax_max"
## for zscore norm
NORM_ZSCORE = "zscore"
NORM_ZSCORE_SIGMA = "zscore_sigma"
NORM_ZSCORE_MU = "zscore_mu"

NORMALIZATIONS = [NORM_MINMAX, NORM_ZSCORE]

# STUFF FOR GPT BELOW
STEPS_PER_SECOND_BOLOMETER = 2500
## For static params see paper
PARAMS_STATIC_COMB = [138700, 0.0688, 0.2641, 14960, 0.134, 11.2]
PARAMS_STATIC_CART = [148900, 0.0693, 0.2464]


# Stuff for legacy database

PLASMA_R_IDF = "plasma_r_idf"
PLASMA_R_MEQ = "plasma_r_meq"

DB_COLUMNS_LEGACY = [SHOT, DATE, TIME, RHO, ASPECT_RATIO, 
              PLASMA_R_IDF, PLASMA_R_MEQ, ELONGATION, 
              ELONGATION_SHEAR, TRIANGULARITY, TRIANGULARITY_SHEAR,
              SHAFRANOV_SHIFT, Q_SAFETY, Q_SAFETY_SHEAR, BETA,
              COLLISION_FREQUENCY, T_RATIO, Z_EFF, IS_Z_EFF_CONST,
              ND_NE_RATIO, NB_NE_RATIO, TE_SCALE, TI_SCALE, NE_SCALE, 
              ND_SCALE, NB_SCALE, PRESSURE_GRADIENT, RADIATION_LOSS, 
              OHMIC_POWER, EXCHANGE_TERM, NBI_ELECTRON_POWER, 
              NBI_ION_POWER, ECRH_POWER, CHI_ELECTRON, CHI_ION,
              IS_OUTLIER, OUTLIER_FIELDS]


DB_DATATYPES_LEGACY = {SHOT: int, DATE: np.datetime64, TIME: float, RHO: float, 
                ASPECT_RATIO: float, PLASMA_R_IDF: float, PLASMA_R_MEQ: float, 
                ELONGATION:float, ELONGATION_SHEAR: float, 
                TRIANGULARITY: float, TRIANGULARITY_SHEAR: float, 
                Q_SAFETY: float, Q_SAFETY_SHEAR: float, BETA: float, 
                COLLISION_FREQUENCY: float, SHAFRANOV_SHIFT: float, 
                T_RATIO: float, Z_EFF: float, IS_Z_EFF_CONST: bool,
                ND_NE_RATIO: float, NB_NE_RATIO: float, 
                TE_SCALE: float, TI_SCALE: float, NE_SCALE: float, 
                ND_SCALE: float, NB_SCALE: float, PRESSURE_GRADIENT: float,
                RADIATION_LOSS: float, OHMIC_POWER: float, 
                EXCHANGE_TERM: float, NBI_ELECTRON_POWER: float, 
                NBI_ION_POWER: float, ECRH_POWER: float, 
                CHI_ELECTRON: float, CHI_ION: float,
                IS_OUTLIER: bool, OUTLIER_FIELDS: str}