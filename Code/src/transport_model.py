"""Implements the model itself, model ensembles and the training/validation dataset class"""

import json
import copy
from pathlib import Path
import numpy as np
import pandas as pd
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader
import torch.optim as optimizers
import torch
from normalize_data import *
from constants import *
from db_handler import DB_handler

DEFAULT_TOPOLOGY = [20,256,512,512,256,2]
ELECTRON_ONLY_TOPOLOGY = [20,256,512,512,256,1]

def get_data_loaders(db: DB_handler, batch_size: int = 64, 
                         test_size: float = 0.2,
                         only_electrons: bool = False, 
                         device:str = "cpu") -> tuple[DataLoader, DataLoader]:
        x_train, x_test, y_train, y_test = db.train_test_split(test_size, 
                                                               only_electrons)
        train_dataset = TransportDataset(x_train, y_train, device)
        test_dataset = TransportDataset(x_test, y_test, device)

        train_dataloader = DataLoader(train_dataset, batch_size=batch_size, 
                                    shuffle=True)
        test_dataloader = DataLoader(test_dataset, batch_size=1, shuffle=False)

        return train_dataloader, test_dataloader


def get_model_inputs(db_selection: pd.DataFrame | pd.Series):
    inputs = db_selection[MODEL_INPUTS].copy()
    if isinstance(db_selection, pd.Series):
        inputs[Q_SAFETY] = np.abs(inputs[Q_SAFETY])
        inputs[Q_SAFETY_SHEAR] = np.abs(inputs[Q_SAFETY_SHEAR])
    else:
        inputs[Q_SAFETY] = inputs[Q_SAFETY].abs()
        inputs[Q_SAFETY_SHEAR] = inputs[Q_SAFETY_SHEAR].abs()
    return torch.tensor(inputs.to_numpy().astype(np.float32))


class TransportDataset(Dataset):
    def __init__(self, x: pd.DataFrame, y: pd.DataFrame, device: str = "cpu"):
        self.x = torch.tensor(x.values.astype(np.float32)).to(device)
        self.y = torch.tensor(y.values.astype(np.float32)).to(device)
        
    def __len__(self) -> int:
        return len(self.x)

    def __getitem__(self, index):
        return self.x[index], self.y[index]


class ModelEnsemble():
    def __init__(self, n_models: int, 
                 model_topology: list[int] = DEFAULT_TOPOLOGY, 
                 device: str = "cpu"):
        
        self.models = [TransportModel(model_topology, device) 
                       for i in range(n_models)]
        
    def set_norm(self, normdict: dict):
        for model in self.models:
            model.set_norm(normdict)
        
    def predict(self, input) -> list[float]:
        predictions = [model(input).detach().numpy().squeeze() 
                       for model in self.models]
        return predictions
    
    def set_model_mode(self, training_mode: bool):
        for model in self.models:
            model.train(training_mode)
     
    def validate(self, testloader: DataLoader) -> np.ndarray:
        size = len(self.models)
        validation_losses = []
        self.set_model_mode(training_mode=False)
        for i, model in enumerate(self.models):
            print(f"\rValidation for ensemble in progress [{i+1}/{size}]")
            loss = model.validate(testloader)
            validation_losses.append(loss)
        return  np.array(validation_losses)
        
        
    def train_epoch(self, trainloader: DataLoader) -> np.ndarray:
        size = len(self.models)
        epoch_losses = []
        self.set_model_mode(training_mode=True)
        for i, model in enumerate(self.models):
            print(f"\rEpoch for ensemble in progress [{i+1}/{size}]", end="")
            loss = model.train_epoch(trainloader)
            epoch_losses.append(loss.mean())
        return  np.array(epoch_losses)
        
        
    def train_epochs(self, trainloader: DataLoader, epochs: int) -> np.ndarray:
        train_avg_losses = []
        for epoch in range(epochs):
            losses = self.train_epoch(trainloader)
            print(f"\rEpoch {epoch+1} of {epochs:}, loss: {losses}")
            train_avg_losses.append(losses)
        return np.array(train_avg_losses)
    
    
    def save(self, ensemble_name: str):
        ensemble_path = Path(f"data/model/{ensemble_name}")
        ensemble_path.mkdir(parents=True, exist_ok=True)
        for i, model in enumerate(self.models):
            torch.save(model.state_dict(), 
                 ensemble_path / f"{ensemble_name}_model_{i+1}.pth")
        with open(ensemble_path / f"{ensemble_name}_norm.json", "w") as f:
            normdict = copy.deepcopy(self.models[0].norm)
            for key, item in normdict.items():
                if isinstance(item, torch.Tensor):
                    normdict[key] = item.tolist()
            json.dump(normdict, f)
        
            
    def load(self, ensemble_name: str):
        ensemble_path = Path(f"data/model/{ensemble_name}")
        #norm_params_path = Path(f"data/model/{model_name}_norm.json")
        for i, model in enumerate(self.models):
            model.load_state_dict(
                torch.load(ensemble_path / f"{ensemble_name}_model_{i+1}.pth")
            )
            model.eval()
        with open(ensemble_path / f"{ensemble_name}_norm.json", "r") as f:
            norm = json.load(f)
            for key, item in norm.items():
                if isinstance(item, list):
                    norm[key] = torch.tensor(item)
            self.set_norm(norm)
    

class TransportModel(nn.Module):
    
    def __init__(self, topology: list[int] = DEFAULT_TOPOLOGY,
                 device: str = "cpu"):
        super().__init__()
        self.device = device
        self.norm = {
            NORM_TYPE: None,
        }
        self.topology = topology
        self.loss = nn.SmoothL1Loss()
        self.infereCHI = nn.Sequential()
        for i in range(len(topology)-2):
            self.infereCHI.append(nn.Linear(topology[i], topology[i+1]))
            self.infereCHI.append(nn.LeakyReLU())
        self.infereCHI.append(nn.Linear(topology[-2], topology[-1]))
        #self.apply(self.init_weights)
        self.to(device)
        
        
    def set_norm(self, normdict: dict):
        for key, item in normdict.items():
            if isinstance(item, np.ndarray):
                normdict[key] = torch.tensor(item).to(self.device)
        self.norm = normdict
    
        
    def forward(self, inputs: torch.Tensor) -> torch.Tensor:
        if self.norm[NORM_TYPE] in NORMALIZATIONS:
            inputs = self.normalize_values(inputs)
        out = self.infereCHI(inputs)
        return out


    def normalize_values(self, values: torch.Tensor) -> torch.Tensor:
        norm = self.norm[NORM_TYPE]
        if norm == NORM_MINMAX:
            minimum = self.norm[NORM_MINMAX_MIN]
            maximum = self.norm[NORM_MINMAX_MAX]
            values = minmax(values, minimum, maximum)
        elif norm == NORM_ZSCORE:
            sigma, mu = self.norm[NORM_ZSCORE_SIGMA], self.norm[NORM_ZSCORE_MU]
            values = z_score(values, sigma, mu)
        return values


    def init_weights(self, module: nn.Module):
        if isinstance(module, nn.Linear):
            module.bias.data.fill_(0)
            nn.init.normal_(module.weight, mean=0, std=0.001)
        
        
    def save(self, model_name: str):
        model_path = f"data/model/{model_name}.pth"
        norm_params_path = f"data/model/{model_name}_norm.json"
        torch.save(self.state_dict(), model_path)
        with open(norm_params_path, "w") as f:
            normdict = copy.deepcopy(self.norm)
            for key, item in normdict.items():
                if isinstance(item, torch.Tensor):
                    normdict[key] = item.tolist()
            json.dump(normdict, f)
        
        
    def load(self, model_name: str):
        model_path = f"data/model/{model_name}.pth"
        norm_params_path = Path(f"data/model/{model_name}_norm.json")
        self.load_state_dict(torch.load(model_path))
        self.eval()
        if norm_params_path.is_file():
            with open(norm_params_path, "r") as f:
                self.norm = json.load(f)
                for key, item in self.norm.items():
                    if isinstance(item, list):
                        self.norm[key] = torch.tensor(item)
        else:
            print(f"No saved norm params for model {model_name} found!")
            
    
    def train_epoch(self, trainloader: DataLoader, 
                    progress: bool = False) -> np.ndarray:
        self.train()
        optimizer = optimizers.Adam(self.parameters(), lr=0.001)
        epoch_loss = []
        size = len(trainloader.dataset)
        for batch_idx, (inputs, targets) in enumerate(trainloader):
            outputs = self(inputs)
            if self.topology[-1] == 1:
                targets = targets.unsqueeze(1)
            optimizer.zero_grad()
            loss = self.loss(outputs, targets)
            loss.backward()
            optimizer.step()
            epoch_loss.append(loss.item())
            if batch_idx % 100 == 0 and progress:
                current = (batch_idx + 1) * len(inputs)
                print(f"\rEpoch progress: [{current:>5d}/{size:>5d}]", end="")
        return np.array(epoch_loss)
                
        
    def validate(self, testloader: DataLoader) -> np.ndarray:
        self.eval()
        test_losses = []
        size = len(testloader.dataset)
        for index, (inputs, targets) in enumerate(testloader):
            outputs = self(inputs)
            if self.topology[-1] == 1:
                targets = targets.unsqueeze(1)
            loss = self.loss(outputs, targets)
            test_losses.append(loss.item())
            #print(f"pred: {outputs.item()}, target: {targets.item()}")
            if index % 1000 == 0:
                print(f"\rValidation in progress: [{index:>5d}/{size:>5d}]", 
                      end="")
        return np.array(test_losses)