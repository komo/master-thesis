"""implements the profile smoothing with gaussian process regression"""

import numpy as np

def kernel(rho1: np.ndarray, rho2: np.ndarray, 
           sigma: np.ndarray, l:float) -> np.ndarray:
    d_rho = rho1[:, np.newaxis] - rho2[np.newaxis, :]    
    return sigma**2 * np.exp(-d_rho**2 / (2*l**2))

def fit_profile(x: np.ndarray, 
                y: np.ndarray, 
                x_fit: np.ndarray, 
                error: np.ndarray, 
                sigma: float,
                l: float) -> tuple[np.ndarray, np.ndarray]:
    k_meas = kernel(x, x, sigma, l)  + np.diag(error)**2
    k_meas_inv = np.linalg.inv(k_meas)
    k_fit_meas = kernel(x_fit, x, sigma, l)
    k_fit = kernel(x_fit, x_fit, sigma, l)
    
    fit = k_fit_meas @ k_meas_inv @ y
    cov = k_fit - k_fit_meas @ k_meas_inv @ k_fit_meas.T
    
    return fit, cov
    