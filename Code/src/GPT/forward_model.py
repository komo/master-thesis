"""Getting measurements from emissions"""
import numpy as np
from pathlib import Path

DATA_DIR = Path(__file__).parents[1] / "data"

transfer_small = np.loadtxt(str(DATA_DIR/"transfermatrix_volume_reduced.txt"))
transfer_full = np.loadtxt(str(DATA_DIR/"full_transfer_volume.txt"))


def get_forward_measurement(emission_distribution: np.ndarray) -> np.ndarray:
    """Get measurement data from emission data via transfermatrix"""

    transfer = (transfer_small if emission_distribution.size == 966 
                else transfer_full)
    
    shape = emission_distribution.shape
    emission_distribution = emission_distribution.reshape(shape[0]*shape[1])
    return transfer @ emission_distribution
