"""Optimize hyperparameters for given measurement"""

from scipy.optimize import minimize, minimize_scalar
from gp_inference import neg_log_likelihood, performGP
from profiles import get_profile_from_coords
import numpy as np
from forward_model import get_forward_measurement
from metrics import ssim, residuum_mean, signal_to_noise_mean
from kernels import KERNEL_CARTESIAN, KERNEL_CARTESIAN_VAR_Z

iteration = 0


def optimize_params(measurements,
                    params_initial,
                    active_channels = np.ones(128, dtype=bool),
                    kernel_function = "cartesian_var_z",
                    fullsize=False,
                    shotnumber=0,
                    time=0,
                    x_point_up=False,
                    magnetic_coords=np.array([]),
                    xatol=1):
    """
    Optimizes parameters by minimizing negative log likelihood.

    Parameters
    ----------
    measurements : numpy.ndarray
    params_initial : numpy.ndarray
        initial guess for hyperparameters  
    transfer_mat : string
        type of transfermatrix ("line" or "volume"(default))
    kernel_function : string
        type of covariance function ("simple_rbf"(default) or "rbf_sigmoid_z")
    remove_zero_measurements : boolean
        set transfermatrix for channels with zero/negative signal to zero

    Returns
    -------
    numpy.ndarray
        best hyperparameters for specified kernel
    """
    global iteration
    to_optimize = neg_log_like( measurements, 
                                active_channels, 
                                kernel_function,
                                fullsize, 
                                shotnumber, 
                                time,
                                x_point_up,
                                magnetic_coords)

    #constraints = get_boundary_constraint(params_initial)

    opt = minimize(fun=to_optimize,# constraints=constraints,
           x0=params_initial, method='Nelder-Mead', 
                options={"disp": True, "maxiter":2000,
                            "xatol":xatol, "adaptive":True})
    iteration = 0
    print(opt)
    return np.abs(opt.x)


def optimize_for_emission_ssim( measurements,
                                wanted_emission,
                                params_initial,
                                kernel_function,
                                shotnumber=0,
                                time=0,
                                fullsize = False,
                                x_point_up = False,
                                magnetic_coords=np.array([])):
    """
    Optimize parameters by maximizing structural similarity index for 
    specified emission and fitted emission
    """

    global iteration

    to_optimize = to_optimize_for_emission_ssim(measurements,
                                                wanted_emission,
                                                kernel_function,
                                                shotnumber,
                                                time,
                                                fullsize,
                                                x_point_up,
                                                magnetic_coords)
    
    #constraints = get_boundary_constraint(params_initial)

    opt = minimize(fun=to_optimize,# constraints=constraints,
           x0=params_initial, method='Nelder-Mead',
           options={"fatol":10, "xatol": 0.2,"disp": True, "maxiter":500})

    iteration = 0
    print(opt)
    return np.abs(opt.x)


def optimize_for_emission(  measurements,
                            wanted_emission,
                            params_initial,
                            kernel_function,
                            shotnumber=0,
                            time=0,
                            fullsize = False,
                            x_point_up = False,
                            magnetic_coords=np.array([]) ):
    """
    Optimizing parameters by minimizing RMSE of specified emission and 
    fitted emission
    """

    global iteration

    to_optimize = to_optimize_for_emission( measurements,
                                            wanted_emission,
                                            kernel_function,
                                            shotnumber,
                                            time,
                                            fullsize,
                                            x_point_up,
                                            magnetic_coords)
    
    #constraints = get_boundary_constraint(params_initial)

    opt = minimize(fun=to_optimize,# constraints=constraints,
           x0=params_initial, method='Nelder-Mead',
           options={"fatol":10, "xatol": 0.2 ,"disp": True, "maxiter":1000})

    iteration = 0
    print(opt)
    return np.abs(opt.x)


def optimize_for_residuum(  measurements,
                            wanted_emission,
                            params_initial,
                            kernel_function,
                            shotnumber=0,
                            time=0,
                            fullsize = False,
                            x_point_up = False,
                            magnetic_coords=np.array([]) ):
    """
    Optimizing parameters by minimizing absolute error between sample and fit #
    divided by std from posterior covariance of fit
    """

    global iteration

    to_optimize = to_optimize_for_residuum( measurements,
                                            wanted_emission,
                                            kernel_function,
                                            shotnumber,
                                            time,
                                            fullsize,
                                            x_point_up,
                                            magnetic_coords)
    
    #constraints = get_boundary_constraint(params_initial)

    opt = minimize(fun=to_optimize,# constraints=constraints,
           x0=params_initial, method='Nelder-Mead',
           options={"fatol":10, "xatol": 0.2 ,"disp": True, "maxiter":500})

    iteration = 0
    print(opt)
    return np.abs(opt.x)


def optimize_for_combination(  measurements,
                            wanted_emission,
                            params_initial,
                            kernel_function,
                            shotnumber=0,
                            time=0,
                            fullsize = False,
                            x_point_up = False,
                            magnetic_coords=np.array([]) ):
    """
    Optimizing parameters by minimizing absolute error between sample and fit #
    divided by std from posterior covariance of fit
    """

    global iteration

    to_optimize = to_optimize_for_combination( measurements,
                                                wanted_emission,
                                                kernel_function,
                                                shotnumber,
                                                time,
                                                fullsize,
                                                x_point_up,
                                                magnetic_coords)
    
    #constraints = get_boundary_constraint(params_initial)

    opt = minimize(fun=to_optimize,# constraints=constraints,
           x0=params_initial, method='Nelder-Mead',
           options={"fatol":10, "xatol": 0.2 ,"disp": True, "maxiter":500})

    iteration = 0
    print(opt)
    return np.abs(opt.x)


def neg_log_like(   measurements, 
                    active_channels, 
                    kernel_function,
                    fullsize, 
                    shotnumber=0,
                    time=0,
                    x_point_up=False,
                    magnetic_coords=np.array([])):
    """Returns function with just params as input to optimize it"""
    #magnetic_coords = np.array([])
    if (not kernel_function in {KERNEL_CARTESIAN, KERNEL_CARTESIAN_VAR_Z}
        and not magnetic_coords.size):
        raise(ValueError("please provide magnetic coordinates"))

    def nll(params):
        global iteration
        res = neg_log_likelihood(   measurements, 
                                    np.abs(params), 
                                    active_channels, 
                                    kernel_function,
                                    fullsize=fullsize, 
                                    shotnumber=shotnumber,
                                    time=time,  
                                    x_point_up=x_point_up,
                                    magnetic_coords=magnetic_coords )
        if not iteration % 200: 
            print("Function call {}: {}".format(iteration, round(res,3)))
            print(np.abs(params))
        iteration += 1
        return res
    return nll


def to_optimize_for_combination(   measurements,
                                wanted_emission,
                                kernel_function,
                                shotnumber,
                                time,
                                fullsize,
                                x_point_up,
                                magnetic_coords):

    if (not kernel_function in {KERNEL_CARTESIAN, KERNEL_CARTESIAN_VAR_Z}
        and not magnetic_coords.size):
        raise(ValueError("please provide magnetic coordinates"))

    def function_to_optimize(params):
        global iteration
        params[0] += 0.1# prevent strage bug, where optimizer sets sigma_cart
                        # to 0 resulting in singular kernel
        fitted_emission, post_cov = performGP( measurements, 
                                        params,
                                        kernel_function=kernel_function,
                                        shotnumber=shotnumber,
                                        time=time,
                                        fullsize=fullsize,
                                        x_point_up=x_point_up,
                                        magnetic_coords=magnetic_coords )

        to_minimize1 = 1/(signal_to_noise_mean(fitted_emission, post_cov))
        to_minimize2 = residuum_mean(wanted_emission, fitted_emission, post_cov)
        to_minimize = to_minimize1 + to_minimize2
        #print(params)
        if not iteration % 200: 
            print("Function call {}: {}".format(iteration, to_minimize))
        iteration += 1
        return to_minimize
    return function_to_optimize


def to_optimize_for_residuum(   measurements,
                                wanted_emission,
                                kernel_function,
                                shotnumber,
                                time,
                                fullsize,
                                x_point_up,
                                magnetic_coords):

    if (not kernel_function in {KERNEL_CARTESIAN, KERNEL_CARTESIAN_VAR_Z}
        and not magnetic_coords.size):
        raise(ValueError("please provide magnetic coordinates"))

    def function_to_optimize(params):
        global iteration
        params[0] += 0.1# prevent strage bug, where optimizer sets sigma_cart
                        # to 0 resulting in singular kernel
        fitted_emission, post_cov = performGP( measurements, 
                                        params,
                                        kernel_function=kernel_function,
                                        shotnumber=shotnumber,
                                        time=time,
                                        fullsize=fullsize,
                                        x_point_up=x_point_up,
                                        magnetic_coords=magnetic_coords )

        to_minimize = residuum_mean(wanted_emission, fitted_emission, post_cov)
        #print(params)
        if not iteration % 50: 
            print("Function call {}: {}".format(iteration, to_minimize))
        iteration += 1
        return to_minimize
    return function_to_optimize


def to_optimize_for_emission(   measurements,
                                wanted_emission,
                                kernel_function,
                                shotnumber,
                                time,
                                fullsize,
                                x_point_up,
                                magnetic_coords):

    if (not kernel_function in {KERNEL_CARTESIAN, KERNEL_CARTESIAN_VAR_Z}
        and not magnetic_coords.size):
        raise(ValueError("please provide magnetic coordinates"))

    def function_to_optimize(params):
        global iteration
        params[0] += 0.1# prevent strage bug, where optimizer sets sigma_cart
                        # to 0 resulting in singular kernel
        fitted_emission, _ = performGP( measurements, 
                                        params,
                                        kernel_function=kernel_function,
                                        shotnumber=shotnumber,
                                        time=time,
                                        fullsize=fullsize,
                                        x_point_up=x_point_up,
                                        magnetic_coords=magnetic_coords )

        rmse = (np.linalg.norm(wanted_emission - fitted_emission)/
                np.linalg.norm(wanted_emission))
        #print(params)
        if not iteration % 200: 
            print("Function call {}: {}".format(iteration, rmse))
        iteration += 1
        return rmse
    return function_to_optimize
    

def to_optimize_for_emission_ssim(  measurements,
                                    wanted_emission,
                                    kernel_function,
                                    shotnumber,
                                    time,
                                    fullsize,
                                    x_point_up,
                                    magnetic_coords):

    if (not kernel_function in {KERNEL_CARTESIAN, KERNEL_CARTESIAN_VAR_Z}
        and not magnetic_coords.size):
        raise(ValueError("please provide magnetic coordinates"))
    
    def function_to_optimize(params):
        global iteration
        
        params[0] += 0.1# prevent strage bug, where optimizer sets sigma_cart
                        # to 0 resulting in singular kernel
        fitted_emission, _ = performGP( measurements, 
                                        params,
                                        kernel_function=kernel_function,
                                        shotnumber=shotnumber,
                                        time=time,
                                        fullsize=fullsize,
                                        x_point_up=x_point_up,
                                        magnetic_coords=magnetic_coords )

        to_minimize = -ssim(wanted_emission, fitted_emission, 
                          data_range=(wanted_emission.max()
                                      -wanted_emission.min()))
        #print(params)
        if not iteration % 200: 
            print("Function call {}: {}".format(iteration, to_minimize))
        iteration += 1
        return to_minimize
    return function_to_optimize



# Older stuff not used for paper

def optimize_for_measurement(measurements,
                             params_initial,
                             kernel_function,
                             shotnumber=0,
                             time=0,
                             x_point_up = False  ):
    """
    Optimizes parameters by minimizing error between input measurements 
    and the measurements of the fitted emission distribution (via forwardmodel)
    """

    global iteration

    to_optimize = to_optimize_for_measurement(  measurements,
                                                kernel_function,
                                                shotnumber,
                                                time,
                                                x_point_up)

    opt = minimize(fun=to_optimize, 
           x0=params_initial, method='Nelder-Mead',
           options={"disp": True, "maxiter":500, "fatol":100, "xatol":0.1})
    iteration = 0
    print(opt)

    return np.abs(opt.x)


def optimize_for_profile(   measurements,
                            wanted_profile,
                            params_initial,
                            kernel_function,
                            shotnumber,
                            time,
                            fullsize = False,
                            x_point_up = False  ):
    """
    Optimize parameters by minimizing error of specified profile and profile 
    from fitted emission distribution
    """

    global iteration
    to_optimize = to_optimize_for_profile(  measurements,
                                            wanted_profile,
                                            kernel_function,
                                            shotnumber,
                                            time,
                                            fullsize,
                                            x_point_up    )
    
    constraints = get_boundary_constraint(params_initial)

    opt = minimize(fun=to_optimize, constraints=constraints,
           x0=params_initial, method='COBYLA',
           options={"disp": True, "maxiter":500})
    iteration = 0
    print(opt)
    return np.abs(opt.x)


def to_optimize_for_measurement(measurements,
                                kernel_function,
                                shotnumber,
                                time,
                                x_point_up=False,
                                magnetic_coords=np.array([])):

    if (not kernel_function in {KERNEL_CARTESIAN, KERNEL_CARTESIAN_VAR_Z}
        and not magnetic_coords.size):
        raise(ValueError("please provide magnetic coordinates"))

    def function_to_optimize(params):
        global iteration

        fitted_emission, _ = performGP( measurements, 
                                        np.abs(params),
                                        kernel_function=kernel_function,
                                        shotnumber=shotnumber,
                                        time=time,
                                        x_point_up=x_point_up,
                                        magnetic_coords=magnetic_coords)
        
        fitted_measurements = get_forward_measurement(fitted_emission)

        mask = measurements > 0

        rmse = (np.linalg.norm(measurements[mask] - fitted_measurements[mask])/
                np.linalg.norm(measurements[mask]))
        
        if not iteration % 50: 
            print("Function call {}: {}".format(iteration, rmse))
        iteration += 1
        return rmse
        
    return function_to_optimize


def to_optimize_for_profile(measurements,
                            wanted_profile,
                            kernel_function,
                            shotnumber,
                            time,
                            fullsize,
                            x_point_up=False,
                            magnetic_coords=np.array([])):
    
    if (not kernel_function in {KERNEL_CARTESIAN, KERNEL_CARTESIAN_VAR_Z}
        and not magnetic_coords.size):
        raise(ValueError("please provide magnetic coordinates"))
    rho_coords = magnetic_coords[0]

    def function_to_optimize(params):
        global iteration
        
        fitted_emission, _ = performGP( measurements, 
                                        params,
                                        kernel_function=kernel_function,
                                        shotnumber=shotnumber,
                                        time=time,
                                        fullsize=fullsize,
                                        x_point_up=x_point_up,
                                        magnetic_coords=magnetic_coords )
        
        fitted_profile = get_profile_from_coords(fitted_emission, rho_coords)

        rmse = (np.linalg.norm(wanted_profile[0] - fitted_profile[0])/
                np.linalg.norm(wanted_profile[0]))
        
        if not iteration % 20: 
            print("Function call {}: {}".format(iteration, rmse))
        iteration += 1
        return rmse
    return function_to_optimize


def get_boundary_constraint(params):
    bounds = []
    constraints = []
    for _ in params:
        bounds.append((0.0001,np.inf))

    # convert bounds to inequality constraints for COBYLA
    for factor in range(len(bounds)):
        lower, upper = bounds[factor]
        l = {'type': 'ineq', 'fun': lambda x, lb=lower, i=factor: x[i] - lb}
        u = {'type': 'ineq', 'fun': lambda x, ub=upper, i=factor: ub - x[i]}
        constraints.append(l)
        constraints.append(u)

    return constraints

# Old stuff, did not work as well as expected

# def to_optimize_sigma_combined(measurements, x_point_up, shotnumber, time):

#     magnetic_coords = get_magnetic_coords(shotnumber, time)

#     def function_to_optimize(sigmas):
#         global iteration
#         params = PARAMS_STATIC.copy()
#         params[0] = sigmas[0]
#         params[3] = sigmas[1]

#         res = neg_log_likelihood(measurements, np.abs(params), x_point_up,
#                                  shotnumber=shotnumber, time=time,
#                                  kernel_function="combined_var_vert",
#                                  magnetic_coords=magnetic_coords)

#         if not iteration % 50: 
#             print("Function call {}: {}".format(iteration, res))
#         iteration += 1
#         return res

#     return function_to_optimize


# def optimize_sigma_combined(measurements, shotnumber, time,
#                             sigma_initial=(250000, 25000), 
#                             x_point_up=False):

#     global iteration

#     to_optimize = to_optimize_sigma_combined(measurements, x_point_up, 
#                                              shotnumber, time)

#     opt = minimize(fun=to_optimize, x0=sigma_initial, method='COBYLA', 
#                     options={"disp": True, "maxiter":2000, 
#                                 "bounds":[1000, 10000000]})
#     iteration = 0
#     print(opt)
#     return np.abs(opt.x)


# def to_optimize_sigma_cartesian(measurements, x_point_up):

#     def function_to_optimize(sigma):
#         global iteration
#         params = PARAMS_STATIC[0:3]
#         params[0] = sigma

#         res = neg_log_likelihood(measurements, np.abs(params), x_point_up,
#                                  kernel_function="cartesian_var_scale")
#         if not iteration % 1: 
#             print("Function call {}: {}".format(iteration, res))
#         iteration += 1
#         return res

#     return function_to_optimize


# def optimize_sigma_cartesian(measurements, 
#                              sigma_initial=250000, 
#                              x_point_up=False):

#     global iteration

#     to_optimize = to_optimize_sigma_cartesian(measurements, x_point_up)

#     opt = minimize_scalar(fun=to_optimize, bounds=[10000, 100000000], 
#                             method="Bounded")

#     iteration = 0
#     print(opt)
#     return np.abs(opt.x)
