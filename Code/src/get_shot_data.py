"""function for obtaining the dataframe for the database of one given shotnumber"""

import sys
from pathlib import Path
sys.path.append(str(Path(__file__).parent.absolute()))
from data_handlers.idf_handler import IDF_handler
from data_handlers.meq_handler import MEQ_handler
from data_handlers.powerbalance_handler import Powerbalance_handler
import pandas as pd

from constants import *


def get_data_for_shot(shotnumber: int, fast_GPT: bool = False) -> pd.DataFrame:

    # use data from AUGD for everything except for the power data 
    # since ECRH power and NBI power (IDR) do not exist for all shots
    IDF = IDF_handler(shotnumber, exp="AUGD")
    IDF_POWH = IDF_handler(shotnumber, exp="scratch")
    MEQ = MEQ_handler(shotnumber, IDF)
    POWH = Powerbalance_handler(IDF_POWH, MEQ)
    
    if IDF.timebase.argmax() > IDF_POWH.timebase.argmax():
        timebase = IDF_POWH.timebase
        max_tidx = timebase.argmax()
    else:
        timebase = IDF.timebase
        max_tidx = timebase.argmax()
    
    aspect_ratio = IDF.r/MEQ.R
    
    
    elongation = MEQ.get_elongation()
    elongation = MEQ.smooth_profile(elongation)
    elongation_shear = MEQ.get_elongation_shear(elongation)
    triangularity = MEQ.get_triangularity()
    triangularity = MEQ.smooth_profile(triangularity, l=0.8)
    triangularity_shear = MEQ.get_triangularity_shear(triangularity)
    shafranov_shift = IDF.get_shafranov_shift(MEQ.R)
    
    beta = IDF.get_beta(MEQ)
    
    aspect_ratio = IDF.interpol_profile(aspect_ratio, IDF.rhobase_r)
    elongation = IDF.interpol_profile(elongation, IDF.rhobase_gp_fit)
    elongation_shear = IDF.interpol_profile(elongation_shear, IDF.rhobase_gp_fit)
    triangularity = IDF.interpol_profile(triangularity, IDF.rhobase_gp_fit)
    triangularity_shear = IDF.interpol_profile(triangularity_shear, IDF.rhobase_gp_fit)
    shafranov_shift = IDF.interpol_profile(shafranov_shift, IDF.rhobase_r[:, 4:])
    
    q_sa = IDF.interpol_profile(IDF.q_profiles, IDF.rhobase_q)
    q_shear = IDF.interpol_profile(IDF.q_shear, IDF.rhobase_gp_fit)
    q_shear_alt = IDF.interpol_profile(IDF.q_shear_alternative, IDF.rhobase_gp_fit)
    collsision_frequency = IDF.interpol_profile(IDF.collision_frequency, IDF.rhobase_gp_fit)
    
    temp_ratio = IDF.interpol_profile(IDF.temp_ratio, IDF.rhobase_gp_fit)
    z_eff = IDF.interpol_profile(IDF.z_eff, IDF.rhobase_gp_fit)
    nD_ne_ratio = IDF.interpol_profile(IDF.nD_ne_ratio, IDF.rhobase_gp_fit)
    nB_ne_ratio = IDF.interpol_profile(IDF.nB_ne_ratio, IDF.rhobase_gp_fit)
    te_scale = IDF.interpol_profile(IDF.te_scale, IDF.rhobase_gp_fit)
    ti_scale = IDF.interpol_profile(IDF.ti_scale, IDF.rhobase_gp_fit)
    ne_scale = IDF.interpol_profile(IDF.ne_scale, IDF.rhobase_gp_fit)
    nD_scale = IDF.interpol_profile(IDF.nD_scale, IDF.rhobase_gp_fit)
    nB_scale = IDF.interpol_profile(IDF.nB_scale, IDF.rhobase_gp_fit)
    pressure_grad = IDF.interpol_profile(IDF.pressure_gradient, IDF.rhobase_gp_fit)
    
    te = IDF.interpol_profile(IDF.te, IDF.rhobase_gp_fit)
    ti = IDF.interpol_profile(IDF.ti, IDF.rhobase_gp_fit)
    ne = IDF.interpol_profile(IDF.ne, IDF.rhobase_gp_fit)
    ni = IDF.interpol_profile(IDF.ni, IDF.rhobase_gp_fit)
    nD = IDF.interpol_profile(IDF.nD, IDF.rhobase_gp_fit)
    nB = IDF.interpol_profile(IDF.nB, IDF.rhobase_gp_fit)
    
    d_te = IDF.interpol_profile(IDF.d_te, IDF.rhobase_gp_fit)
    d_ti = IDF.interpol_profile(IDF.d_ti, IDF.rhobase_gp_fit)
    d_ne = IDF.interpol_profile(IDF.d_ne, IDF.rhobase_gp_fit)
    d_nD = IDF.interpol_profile(IDF.d_nD, IDF.rhobase_gp_fit)
    d_nB = IDF.interpol_profile(IDF.d_nB, IDF.rhobase_gp_fit)
    
    dA = IDF.interpol_profile(IDF.dA, IDF.rhobase_r)
    
    n_rho = IDF.rho_intervals[:max_tidx].shape[1]
    n_rows = max_tidx * n_rho
        
    a_plasma = np.repeat(IDF.a[:max_tidx], n_rho)
    pressure_grad = pressure_grad[:max_tidx].reshape(n_rows)
    q_sa = q_sa[:max_tidx].reshape(n_rows)
    pressure_grad = pressure_grad * q_sa * a_plasma**2
    
    
    #Powerbalance stuff
    powerdata = POWH.get_data_for_intervals(GPT_fastmode=fast_GPT)
    
    ### Creating Dataframe and populating its columns
    
    df_shot = pd.DataFrame(columns=DB_COLUMNS, index=np.arange(n_rows))
    
    df_shot[SHOT]                   = shotnumber
    df_shot[DATE]                   = IDF.date
    df_shot[DATE]                   = pd.to_datetime(df_shot[DATE], format='%Y-%m-%d')
    df_shot[TIME]                   = np.repeat(timebase[:max_tidx], n_rho)
    df_shot[RHO]                    = IDF.rho_intervals[:max_tidx].reshape(n_rows)
    df_shot[ASPECT_RATIO]           = aspect_ratio[:max_tidx].reshape(n_rows)
    df_shot[PLASMA_R]               = a_plasma
    df_shot[ELONGATION]             = elongation[:max_tidx].reshape(n_rows)
    df_shot[ELONGATION_SHEAR]       = elongation_shear[:max_tidx].reshape(n_rows)
    df_shot[TRIANGULARITY]          = triangularity[:max_tidx].reshape(n_rows)
    df_shot[TRIANGULARITY_SHEAR]    = triangularity_shear[:max_tidx].reshape(n_rows)
    df_shot[SHAFRANOV_SHIFT]        = shafranov_shift[:max_tidx].reshape(n_rows)
    df_shot[Q_SAFETY]               = q_sa
    df_shot[Q_SAFETY_SHEAR]         = q_shear[:max_tidx].reshape(n_rows)
    df_shot[Q_SAFETY_SHEAR_ALT]     = q_shear_alt[:max_tidx].reshape(n_rows)
    df_shot[BETA]                   = beta[:max_tidx].reshape(n_rows)
    df_shot[COLLISION_FREQUENCY]    = collsision_frequency[:max_tidx].reshape(n_rows)
    df_shot[T_RATIO]                = temp_ratio[:max_tidx].reshape(n_rows)
    df_shot[Z_EFF]                  = z_eff[:max_tidx].reshape(n_rows)
    df_shot[IS_Z_EFF_CONST]         = np.repeat(IDF.is_z_eff_const[:max_tidx], n_rho)
    df_shot[ND_NE_RATIO]            = nD_ne_ratio[:max_tidx].reshape(n_rows)
    df_shot[NB_NE_RATIO]            = nB_ne_ratio[:max_tidx].reshape(n_rows)
    df_shot[TE_SCALE]               = te_scale[:max_tidx].reshape(n_rows)
    df_shot[TI_SCALE]               = ti_scale[:max_tidx].reshape(n_rows)
    df_shot[NE_SCALE]               = ne_scale[:max_tidx].reshape(n_rows)
    df_shot[ND_SCALE]               = nD_scale[:max_tidx].reshape(n_rows)
    df_shot[NB_SCALE]               = nB_scale[:max_tidx].reshape(n_rows)
    df_shot[PRESSURE_GRADIENT]      = pressure_grad.reshape(n_rows)
    df_shot[ELECTRON_TEMP]          = te[:max_tidx].reshape(n_rows)
    df_shot[ION_TEMP]               = ti[:max_tidx].reshape(n_rows)
    df_shot[ELECTRON_DENSITY]       = ne[:max_tidx].reshape(n_rows)
    df_shot[ION_DENSITY]            = ni[:max_tidx].reshape(n_rows)
    df_shot[DEUTERIUM_DENSITY]      = nD[:max_tidx].reshape(n_rows)
    df_shot[BORON_DENSITY]          = nB[:max_tidx].reshape(n_rows)
    df_shot[ELECTRON_TEMP_GRAD]     = d_te[:max_tidx].reshape(n_rows)
    df_shot[ION_TEMP_GRAD]          = d_ti[:max_tidx].reshape(n_rows)
    df_shot[ELECTRON_DENSITY_GRAD]  = d_ne[:max_tidx].reshape(n_rows)
    df_shot[DEUTERIUM_DENSITY_GRAD] = d_nD[:max_tidx].reshape(n_rows)
    df_shot[BORON_DENSITY_GRAD]     = d_nB[:max_tidx].reshape(n_rows)
    df_shot[FLUX_TUBE_SURF_AREA]    = dA[:max_tidx].reshape(n_rows)
    
    df_shot[RADIATION_LOSS]         = powerdata[RADIATION_LOSS][:max_tidx].reshape(n_rows)
    df_shot[OHMIC_POWER]            = powerdata[OHMIC_POWER][:max_tidx].reshape(n_rows)
    df_shot[ECRH_POWER]             = powerdata[ECRH_POWER][:max_tidx].reshape(n_rows)
    df_shot[NBI_ELECTRON_POWER]     = powerdata[NBI_ELECTRON_POWER][:max_tidx].reshape(n_rows)
    df_shot[NBI_ION_POWER]          = powerdata[NBI_ION_POWER][:max_tidx].reshape(n_rows)
    df_shot[EXCHANGE_TERM]          = powerdata[EXCHANGE_TERM][:max_tidx].reshape(n_rows)
    df_shot[ELECTRON_TOTAL_POWER]   = powerdata[ELECTRON_TOTAL_POWER][:max_tidx].reshape(n_rows)
    df_shot[ION_TOTAL_POWER]        = powerdata[ION_TOTAL_POWER][:max_tidx].reshape(n_rows)
    df_shot[CHI_ELECTRON]           = powerdata[CHI_ELECTRON][:max_tidx].reshape(n_rows)
    df_shot[CHI_ION]                = powerdata[CHI_ION][:max_tidx].reshape(n_rows)
    
    df_shot[IS_OUTLIER]             = False
    df_shot[OUTLIER_FIELDS]         = ""
    df_shot[CUTOFF_REASON]          = IDF.cutoff_reasons[:max_tidx].reshape(n_rows)

    df_shot = df_shot.astype(DB_DATATYPES)
    return df_shot