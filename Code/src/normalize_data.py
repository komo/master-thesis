"""Implements the normalizations for the neural network"""

from torch import Tensor, log, exp
from numpy import ndarray

def log_norm(tensor: Tensor) -> Tensor:
    tensor[tensor > 0] = log(tensor[tensor > 0])
    tensor[tensor < 0] = -log(-tensor[tensor < 0])
    return tensor

def reverse_log_norm(tensor: Tensor) -> Tensor:
    tensor[tensor > 0] = exp(tensor[tensor > 0])
    tensor[tensor < 0] = -exp(-tensor[tensor < 0])

def z_score(tensor: Tensor, sigma: Tensor, 
            mu: Tensor) -> Tensor:
    return (tensor - sigma) / mu

def reverse_z_score(tensor, sigma, mu):
    pass

def minmax(tensor: Tensor, min: Tensor, 
           max: Tensor) -> Tensor:
    return (tensor - min) / (max - min)

def reverse_minmax(tensor, min, max):
    pass