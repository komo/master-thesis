import numpy as np
import pandas as pd

from src.constants import *

FIELDS_TO_CHECK = [RHO, ASPECT_RATIO, PLASMA_R, 
                   ELONGATION, ELONGATION_SHEAR, TRIANGULARITY, 
                   TRIANGULARITY_SHEAR, SHAFRANOV_SHIFT, Q_SAFETY, 
                   Q_SAFETY_SHEAR, BETA, COLLISION_FREQUENCY, T_RATIO, 
                   ND_NE_RATIO, NB_NE_RATIO, Z_EFF, TE_SCALE, TI_SCALE, 
                   NE_SCALE, ND_SCALE, NB_SCALE, PRESSURE_GRADIENT,
                   RADIATION_LOSS, OHMIC_POWER, EXCHANGE_TERM, 
                   NBI_ELECTRON_POWER, NBI_ION_POWER, ECRH_POWER, 
                   CHI_ELECTRON, CHI_ION]


FIELDS_TO_CHECK_SIGN = [ND_NE_RATIO, CHI_ELECTRON, CHI_ION, TE_SCALE, TI_SCALE, T_RATIO]


def check_NaN(db: pd.DataFrame, field: str):
    is_nan = db.data[field].isna()
    db[IS_OUTLIER] = db[IS_OUTLIER] | is_nan
    update_outlier_str(db, is_nan, f"{field} is NaN")
    if is_nan.sum() > 0:
        print(f"{field}: found {is_nan.sum()} with NaN")

   
def check_inf(db: pd.DataFrame, field: str):
    is_inf = db.data[field].isin([np.inf, -np.inf])
    db[IS_OUTLIER] = db[IS_OUTLIER] | is_inf
    update_outlier_str(db, is_inf, f"{field} is inf")
    if is_inf.sum() > 0:
        print(f"{field}: found {is_inf.sum()} with inf")


def check_threshold(db: pd.DataFrame, field: str):
    if field in OUTLIER_THRESHOLDS:
        threshold = OUTLIER_THRESHOLDS[field]
        is_outlier = db[field].abs() > threshold
        db[IS_OUTLIER] = db[IS_OUTLIER] | is_outlier
        update_outlier_str(db, is_outlier, f"{field} threshold")
        if is_outlier.sum() > 0:
            print(f"{field}: found {is_outlier.sum()} outside threshold")
            
            
def check_sign(db: pd.DataFrame, field: str):
    if field in FIELDS_TO_CHECK_SIGN:
        is_outlier = db[field] < 0
        db[IS_OUTLIER] = db[IS_OUTLIER] | is_outlier
        update_outlier_str(db, is_outlier, f"{field} negative")
        if is_outlier.sum() > 0:
            print(f"{field}: found {is_outlier.sum()} with wrong sign")
            
            
def update_outlier_str(db: pd.DataFrame, is_outlier: pd.Series, field_str: str):
    outlier_str_contains_field = db.data[OUTLIER_FIELDS].str.contains(field_str)
    update_str_selection = is_outlier & ~(outlier_str_contains_field)
    db.data.loc[update_str_selection, OUTLIER_FIELDS] += field_str + ', '
    

def perform_checks(db: pd.DataFrame):
    for field in FIELDS_TO_CHECK:
        check_inf(db, field)
        check_NaN(db, field)
        check_threshold(db, field)
        check_sign(db, field)
    print(f"Check finished: {len(db.valid())} valid " + 
            f"and {len(db.invalid())} invalid")
            
        